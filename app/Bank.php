<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{



    protected $fillable = [
        'name',
        'user_id'
    ];

    protected $appends =[
        'urlbanktransaction',
        'urlbankedit',
        'urlbankdelete',
    ];
    public function transactions()
    {
        return $this->hasMany(Transaction::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function getUrlbankeditAttribute(){

        return route('bank.bank-edit',$this);
    }

    public function getUrlbankdeleteAttribute(){

        return route('bank.bank-delete',$this);
    }
    public function getUrlbanktransactionAttribute(){

        return route('bank.transactions',$this);
    }


}
