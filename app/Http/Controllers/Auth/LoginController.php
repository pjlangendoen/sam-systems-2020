<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers {
        logout as performlogout;
    }
    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function logout ()
    {
        Auth::logout();
        return redirect ('/');
    }


    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);

    }

    protected function redirectTo()
    {

        return route('site.dashboard');
    }

    protected function authenticated(Request $request , $user)
    {

        \Session::flash('flash_message', trans('flashmessage.welcome_message', ['user' => Auth::user()->name]));
        $request->session()->flash('payday_check', true);
//        succes(' "Welkom <b>" . Auth::User()->name . "</b> in Samsystems"');
        emptycheck($user);
        return redirect()->intended($this->redirectPath());
    }

}
