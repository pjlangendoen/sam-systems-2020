<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class AuthController extends Controller
{
    public function user()
    {
        return view('site.user');
    }

     public function logout()
    {
        Auth::logout();
    }

    public function __construct()
{

    $this->middleware('auth');

}


}
