<?php

namespace App\Http\Controllers\Site;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Response;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Str;
use Intervention\Image;
Use Auth;
use App\Transaction;
use App\User;
use App\Household;
Use Carbon\Carbon;
Use App\Filecat;
Use App\File;
use Validator;




class FileController extends \App\Http\Controllers\Controller
{

    public function media()
    {
        $files = Auth::user()->files;
        return view('admin.media',['files'=>$files]);
    }

    public function mediaid(File $file)
    {
        $files = new Collection();
        $files->push($file);
        return view('admin.media',['files'=>$files,'fileid' => $file->id]);
    }


    public function mediafiles(Transaction $transaction)
    {
        return view('admin.media',['files'=>$transaction->files, 'transactionid' => $transaction->id]);
    }

    public function mediafilescat(Filecat $filecat)
    {
        return view('admin.media',['files'=>$filecat->file, 'fileCatId' => $filecat->id]);
    }



    public function filenew()
    {   $transactions = Auth::user()->transactions;
        $categorys = Auth::user()->filecatparent()->orderBy('cat', 'asc')->get();
        return view('forms.file-new',[
            'transactions'=>$transactions,
            'categorys'=>$categorys,]);
    }

    public function fileedit(File $file)
    {
        $transactions = Auth::user()->transactions;
        $categorys = Auth::user()->filecatparent()->orderBy('cat', 'asc')->get();
        return view('forms.file-edit', [
            'transactions'=>$transactions,
            'categorys'=>$categorys,
            'file'=> $file]);
    }

    public function filenewupload(Request $request)
    {



        $this->validate($request, [

            'file' => 'required|mimes:jpeg,png,jpg,gif,svg,xlsx,docx,pdf',
            'filecat_id' => 'required',
            'description' => 'required',
            'latest_payday'      => 'date|date_format:Y-m-d',
        ],[
            'filecat_id.required' => trans('media.category_required'),
            'file.required' => trans('media.file_required'),
            'description.required' => trans('media.description_required')

            ]);



        $file = $request->file('file');
        $newname = md5(microtime(true)).'.'.$file->getClientOriginalExtension();
        $destinationPath = storage_path('/app/public/uploads/');
        $file->move($destinationPath, $newname);


        File::create([
            'filename' => $file->getClientOriginalName(),
            'path' => $newname,
            'datetime' => Carbon::now()->format('Y-m-d'),
            'user_id' => Auth::user()->id,
            'transaction_id' => ($request->has('transaction_id') ? $request->input('transaction_id'):null),
            'filecat_id' => $request->input('filecat_id'),
            'latest_payday' => ($request->input('latest_payday') ? $request->input('latest_payday'):null),
            'status' => $request->input('status'),
            'description' => $request->input('description'),
        ]);

        if($request->input('next-page', false)) {
            \Session::flash('flash_message', trans('flashmessage.file_uploaded_succes'));
            return redirect()->route('site.media');
        }
        \Session::flash('flash_message', trans('flashmessage.file_uploaded_succes'));
        return redirect()->route('site.media');
    }

    public function get_file($newname)
    {
        $file_path = storage_path('app/public/uploads') . "/" . $newname;
        return Response::download($file_path);
    }

    public function open_file($newname)
    {
        $file_view = storage_path('app/public/uploads') . "/" . $newname;
        return response()->file($file_view);

    }


    public function fileeditsave( Request $request, File $file)
    {

        $validator=Validator::make($request->input(),[
            'description' => 'required',
            'latest_payday'      => 'date|date_format:Y-m-d',
        ],[

            'description.required' => trans('media.description_required')
        ]);

        if(!$validator->fails()){
        $input = $request->input();

        if(empty($request->input('latest_payday'))) {
        $input['latest_payday'] = null;
        }
            else{
                $input['latest_payday'] = $request->input('latest_payday');
            }
        $file->update($input);
        \Session::flash('flash_message', trans('flashmessage.file_changed_succes'));
        return redirect()->route('site.media');
    }else{

            return redirect()->back()
                ->withErrors($validator)
                ->withInput();

        }}


    public function filedelete(File $file)
    {
        unlink(storage_path('app/public/uploads/'). $file->path);
        $file->delete();
        \Session::flash('flash_message', trans('flashmessage.file_deleted_succes'));
        return redirect()->route('site.media');
    }

}
