<?php

namespace App\Http\Controllers\Site;
use Illuminate\Http\Request;
use App\Filecat;
use App\User;
Use Auth;
use Illuminate\Validation\Rule;
use Validator;
Use App\Transaction;
Use App\File;


class CategoryController extends \App\Http\Controllers\Controller
{

        public function categorys()
    {
        $categorys = Auth::user()->filecatparent;
        return view('admin.category',['categorys'=>$categorys]);
    }

		public function categorynew()
    {
        $filecat = Auth::user()->filecatparent;
        return view('forms.category-new',['filecat'=>$filecat]);
    }

		public function categoryedit(Filecat $filecat)
    {
        $categories = [];
        foreach (Auth::user()->filecatparent as $item) {
            $categories[$item->id] = $item->cat;
        }


        return view('forms.category-edit',['filecat'=>$filecat, 'categories'=>$categories]);
    }





    /*  functions Crud  */

    public function categorynewsave(Request $request)
    {

 

    	$validator=Validator::make($request->input(),[
    		'cat' => Rule::unique('filecat')->where('user_id',Auth::user()->id)
    	],[

    		'cat.unique' => trans('flashmessage.category_name_unique')

    	]);

    	if(!$validator->fails()){
    	$input = $request->input();

    	if(!$request->has('parent_id')) {
  		$input['parent_id'] = null;    }
    	$input['user_id'] = Auth::user()->id;

        Filecat::create($input);

        if($request->input('next-page', false)) {
            return redirect($request->input('next-page'));
        }
            \Session::flash('flash_message', trans('flashmessage.category_made_succes'));
            return redirect ($request->get('redirect'));
    }else{

    	return redirect()->back()
			->withErrors($validator)
			->withInput();

    }}

  public function categoryeditsave( Request $request, Filecat $filecat )
    {
        $validator=Validator::make($request->input(),[
            'cat' => Rule::unique('filecat')->where('user_id',Auth::user()->id)
        ],[

            'cat.unique' => trans('flashmessage.category_name_unique')

        ]);

        if(!$validator->fails()){
        $filecat->update($request->input());
        \Session::flash('flash_message', trans('flashmessage.category_changed_succes'));
        return redirect ($request->get('redirect'));
    }else{

            return redirect()->back()
                ->withErrors($validator)
                ->withInput();

        }}


       public function categorydelete(Filecat $filecat)
    {
       foreach ($filecat->File as $file){
            $file->filecat()->dissociate();
            $file->save();
        }

        foreach ($filecat->Transaction as $transactions){
            $transactions->filecat()->dissociate();
            $transactions->save();
        }


        $filecat->delete();
        \Session::flash('flash_message', trans('flashmessage.category_deleted_succes'));
        return redirect()->route('cat.category');
    }



}
