<?php

namespace App\Http\Controllers\Site;
use App\Filecat;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
Use Auth;
use App\Transaction;
use App\Bank;
use App\Household;
use App\File;
use Illuminate\Support\Facades\Redirect;
use Validator;

class TransactionController extends \App\Http\Controllers\Controller
{
        public function transaction()
    {
        $transactions = Auth::user()->transactions()->with(['household', 'bank', 'files', 'filecat'])->get();
        return view('admin.transactions',['transactions'=>$transactions]);
    }

        public function transactionbank(Bank $bank)
    {
        return view('admin.transactions',['transactions'=>$bank->transactions, 'BankId' => $bank->id]);
    }

        public function transactionhousehold(Household $household)
{
    return view('admin.transactions', ['transactions' => $household->transactions]);
}

    public function transactionscat(Filecat $filecat)
    {
        return view('admin.transactions',['transactions'=>$filecat->transaction, 'fileCatId' => $filecat->id]);
    }

    public function transactions(Transaction $transaction)
    {
        $transactions = new Collection();
        $transactions->push($transaction);
        return view('admin.transactions',['transactions'=>$transactions, 'transactionid' => $transaction->id]);
    }





    public function transactionnew( Household $household)
    {

        $banks = Auth::user()->banks;
        $categorys = Auth::user()->filecatparent()->orderBy('cat', 'asc')->get();
        return view('forms.transaction-new',[
            'household' => $household,
            'banks'=>$banks,
            'categorys'=>$categorys]);
    }
    public function transactionsnew()
    {
        $banks = Auth::user()->banks;
        $households = Auth::user()->households;
        $categorys = Auth::user()->filecatparent()->orderBy('cat', 'asc')->get();
        return view('forms.transaction-new',[
            'banks'=>$banks,
            'households'=>$households,
            'categorys'=>$categorys]);
    }
    public function transactionnewUpload( Household $household, Transaction $transaction)
    {
        $categorys = Auth::user()->filecatparent()->orderBy('cat', 'asc')->get();
        return view('forms.file-new',[
            'household' => $household ,
            'transaction' => $transaction,
            'categorys'=>$categorys
        ]);
    }

   		public function transactionedit(Transaction $transaction)
    {
        $banks = Auth::user()->banks;
        $households = Auth::user()->households;
        $categorys = Auth::user()->filecatparent()->orderBy('cat', 'asc')->get();
        return view('forms.transaction-edit', [
            'transaction'=> $transaction,
            'banks'=>$banks,
            'households'=>$households,
            'categorys'=>$categorys]);

    }






    /*  functions Crud  */

 	public function transactionnewsave(Request $request, Household $household)
    {

    	$input = $request->input();
    	$input['household_id'] = $household->id;
    	$input['user_id'] = Auth::user()->id;

        $transaction = Transaction::create($input);
        \Session::flash('flash_message', trans('flashmessage.transaction_with_file'));
        return redirect()->route('transaction.transaction-new.upload', [$household, $transaction->id]);
    }




    public function transactionsnewsave(Request $request)
    {

        $validator=Validator::make($request->input(),[
            'price' => 'required|numeric:transactions',
            'name' => 'required:transactions',
            'bank_id' => 'required:transactions',
            'household_id' => 'required:transactions',
            'datetime' => 'required:transactions'
        ],[

            'price.numeric' => trans('flashmessage.price_numeric'),
            'price.required' => trans('flashmessage.price_required'),
            'name.required' => trans('flashmessage.name_required'),
            'bank_id.required' => trans('flashmessage.bank_required'),
            'household_id.required' => trans('flashmessage.household_required'),
            'datetime.required' => trans('flashmessage.datetime_required')
        ]);

        if(!$validator->fails()){
            $input = $request->input();
            $input['user_id'] = Auth::user()->id;

            if(!$request->input('description')) {
                $input['description'] = null;    }

            Transaction::create($input);
            \Session::flash('flash_message', trans('flashmessage.transaction_made_succes'));
            return redirect()->route('transaction.transactions');
        }else{

            return redirect()->back()
                ->withErrors($validator)
                ->withInput();

        }}



    public function transactioneditsave(Request $request,Transaction $transaction)
    {
        $validator=Validator::make($request->input(),[
            'price' => 'required|numeric:transactions',
            'name' => 'required:transactions',
            'bank_id' => 'required:transactions',
            'household_id' => 'required:transactions',
            'datetime' => 'required:transactions'
        ],[

            'price.numeric' => trans('flashmessage.price_numeric'),
            'price.required' => trans('flashmessage.price_required'),
            'name.required' => trans('flashmessage.name_required'),
        ]);

        if(!$validator->fails()){
        $input = $request->input();

        if(!$request->input('description')) {
            $input['description'] = null;    }

        $transaction->update($input);
        \Session::flash('flash_message', trans('flashmessage.transaction_changed_succes'));
        return redirect()->route('transaction.transactions');
    }else{

            return redirect()->back()
                ->withErrors($validator)
                ->withInput();

        }}



    public function transactiondelete(Transaction $transaction)
    {
        foreach ($transaction->files as $files){
            $files->transaction()->dissociate();
            $files->save();
        }
        $transaction->delete();
        \Session::flash('flash_message', trans('flashmessage.transaction_deleted_succes'));
        return Redirect::back();
    }
}
