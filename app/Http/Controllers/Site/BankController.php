<?php

namespace App\Http\Controllers\Site;
use Illuminate\Http\Request;
use App\User;
use App\Bank;
Use Auth;
use Illuminate\Validation\Rule;
use Validator;


class BankController extends \App\Http\Controllers\Controller
{
        public function bank()
    {
        $banks = Auth::user()->banks;
        return view('admin.bank',['banks'=>$banks]);
    }

		public function banknew()
    {
        return view('forms.bank-new');
    }

		public function bankedit(Bank $bank)
    {
        return view('forms.bank-edit',['bank'=>$bank]);
    }


    /*  functions Crud  */

    public function banknewsave(Request $request)
    {
    	$validator=Validator::make($request->input(),[
    		'name' => Rule::unique('banks')->where('user_id',Auth::user()->id)

    	],[

    		'name.unique' => trans('flashmessage.bank_name_unique')
    	]);

    	if(!$validator->fails()){
    	$input = $request->input();
    	$input['user_id'] = Auth::user()->id;

        Bank::create($input);
            \Session::flash('flash_message', trans('flashmessage.bank_made_succes'));
            return redirect()->route('bank.banks');
    }else{

    	return redirect()->back()
			->withErrors($validator)
			->withInput();

    }}

  public function bankeditsave( Request $request, Bank $bank)
    {
        $validator=Validator::make($request->input(),[
            'name' => Rule::unique('banks')->where('user_id',Auth::user()->id)
        ],[

            'name.unique' => trans('flashmessage.bank_name_unique')
        ]);

        if(!$validator->fails()){
        $bank->update([
            'name' => $request->name,
        ]);

        \Session::flash('flash_message', trans('flashmessage.bank_changed_succes'));
        return redirect()->route('bank.banks');
    }else{

            return redirect()->back()
                ->withErrors($validator)
                ->withInput();

        }}


    public function bankdelete(Bank $bank)
    {
        foreach ($bank->transactions as $transaction){
            $transaction->bank()->dissociate();
            $transaction->save();
        }
        $bank->delete();
        \Session::flash('flash_message', trans('flashmessage.bank_deleted_succes'));
        return redirect()->route('bank.banks');
    }



}
