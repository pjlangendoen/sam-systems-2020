<?php

namespace App\Http\Controllers\Site;
use App\Transaction;
use Carbon\Carbon;
Use Illuminate\support\Facades\Auth;
use App\Household;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use App\Exports\Householdexport;
use App\Exports\Householdexportall;
use Maatwebsite\Excel\Facades\Excel;


class HouseholdController extends \App\Http\Controllers\Controller
{

    public function household()
    {
        $households = Auth::user()->households;
        return view('admin.household',['households'=>$households]);
    }

    public function householdview(Household $household)
    {
        return view('site.household-view', ['household' => $household]);
    }


    public function export()
    {
        return Excel::download(new Householdexport, 'household.csv');
    }

    public function exportall()
    {
        return Excel::download(new Householdexportall, 'household.csv');
    }

    public function householdnew()
    {
        $household_latest = Household::where('user_id', Auth::user()->id)->orderBy('date', 'desc')->first();
        if (!$household_latest) {
            $household = Household::create([
                'user_id' => Auth::user()->id,
                'date' => Carbon::now()->startOfMonth(),
            ]);
            return redirect()->route('household.household-view', [$household->id]);
        }

        $diff_dates = Carbon::now()->diff(Carbon::parse($household_latest->date));
        if (!$diff_dates->invert) {
            error(trans('flashmessage.household_in_use'));
//            \Session::flash('flash_message', "There is already a household for this month");
            return Redirect::back();
        }



        $new_date = Carbon::parse($household_latest->date)->addMonth(1)->firstOfMonth();
        $start_period = Carbon::parse($household_latest->date)->firstOfMonth();
        $end_period = Carbon::parse($household_latest->date)->lastOfMonth();

        $household = Household::create([
            'user_id' => Auth::user()->id,
            'date' => $new_date->format('Y-m-d'),
        ]);

        $transactions = Transaction::where('user_id', Auth::user()->id)
            ->where('datetime', '>=', $start_period)
            ->where('datetime', '<=', $end_period)
            ->where('monthly', 'yes')
            ->get();

        /** @var Transaction $transaction */
        foreach ($transactions as $transaction) {
            $new_transaction = $transaction->getAttributes();
            unset($new_transaction['id']);
            $new_transaction['datetime'] = $transaction->datetime->addMonth(1)->format('Y-m-d H:i:s');
            $new_transaction['household_id'] = $household->id;
            Transaction::create($new_transaction);
        }
        \Session::flash('flash_message', trans('flashmessage.household_new_made') .count($transactions). trans('flashmessage.household_transactions'));
        return redirect()->route('household.household-view', [$household->id]);
    }

    public function householdedit(Household $household)
    {
        return view('forms.household-edit', ['household'=> $household]);

    }


    /*  functions Crud  */

    public function householdnewsave(Request $request)
    {
        $input = $request->input();

        dd($input);
        $input['user_id'] = Auth::user()->id;
        $household = Household::create($input);

        foreach ($input['transaction_id'] as $transaction_id) {

        }
        return redirect()->route('household.household');
    }

    public function householdeditsave(Request $request,Household $household)
    {

        $household->update($request->input());
        return redirect()->route('household.household');
    }

    public function householddelete(Household $household)
    {
        foreach ($household->transactions as $transaction){
            $transaction->household()->dissociate();
            $transaction->save();
        }
        $household->delete();
        \Session::flash('flash_message', trans('flashmessage.household_deleted_succes'));
        return redirect()->route('household.household');
    }

}