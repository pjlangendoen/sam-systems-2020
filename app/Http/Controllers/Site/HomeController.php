<?php

namespace App\Http\Controllers\Site;

use Auth;
use Illuminate\Http\Request;
Use App\User;
use App\Transactions;
class HomeController extends \App\Http\Controllers\Controller
{

    public function index()
    {
        $transactions = Auth::user()->transactions->sortByDesc('created_at')->take(5);
        $households = Auth::user()->households->sortByDesc('created_at')->take(5);
        $banks = Auth::user()->banks->sortByDesc('created_at')->take(5);
        $categorys = Auth::user()->filecatparent->sortByDesc('created_at')->take(5);
        $files = Auth::user()->files->sortByDesc('created_at')->take(4);

        return view('admin.dashboard',[
            'transactions'=>$transactions,
            'households'=>$households,
            'banks'=>$banks,
            'files'=>$files,
            'categorys'=>$categorys]);
    }
    
    public function user()
    {
        return view('forms.user');
    }
    public function info()
    {
        return view('site.info');
    }
    public function feedback()
    {
        return view('forms.feedback');
    }
    public function usersave(Request $request,User $user)
    {

        $user->update($request->input());
        \Session::flash('flash_message', trans('flashmessage.user_changed_succes'));
        return redirect()->route('site.dashboard');
    }

}