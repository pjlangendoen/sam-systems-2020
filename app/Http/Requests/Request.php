<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

abstract class Request extends FormRequest
{
{

    $user = Auth::user();
    
    $user->username = Request::input('name');
    $user->email = Request::input('email');
    $user->lastname = Request::input('lastname');

    if ( ! Request::input('password') == '')
    {
        $user->password = bcrypt(Request::input('password'));
    }

    $user->save();

    Flash::message('Your account has been updated!');
    return Redirect::to('/user');
}
}
