<?php

namespace App\Http\Middleware;

use Closure;

class Paydaycheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!$request->session()->has('payday_check')) {
            return $next($request);
        }

         \View::composer(['layouts.Layoutpages','layouts.main','admin.media'], function($view) use ($request) {
            $files = $request->user()->files()->where('status', 'Open')->whereBetween('latest_payday', [today(), today()->addDays(7)->endOfDay()])->get();
            $view->with('paydayFiles', $files);
        });
        return $next($request);


    }
}
