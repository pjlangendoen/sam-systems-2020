<?php

use Carbon\Carbon;

function error(...$messages)
{
    $old = Session::get('error') ?: [];
    Session::flash('error', array_merge($old, $messages));
}

function errors() {
    return Session::get('error') ?: [];
}

function emptycheck($user){

if($user->banks()->count()== 0){
        error( trans('flashmessage.first_bank'));
    }
    if($user->transactions()->count()== 0){
        error(trans('flashmessage.first_transaction'));
    }
    if($user->filecatparent()->count()== 0){
        error(trans('flashmessage.first_category'));
    }
    if($user->files()->count()== 0){
        error(trans('flashmessage.first_file'));
    }
    if($user->households()->count()== 0){
        $user->households()->create([
            'user_id' => Auth::user()->id,
            'date' => Carbon::now()->startOfMonth(),
        ]);
        return redirect()->route('site.dashboard');
    }
}
