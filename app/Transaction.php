<?php

namespace App;
use Illuminate\Database\Eloquent\Model;


class Transaction extends Model
{




    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'datetime'
    ];

    protected $fillable = [
        'user_id',
        'name',
        'price',
        'monthly',
        'bank_id',
        'household_id',
        'type',
        'datetime',
        'filecat_id',
        'description'
    ];

    protected $appends =[
        'daymonthyear',
        'urlhousehold',
        'urltransactionedit',
        'urltransactiondelete',
        'urltransactionsfile',

    ];


    public function household()
    {
        return $this->belongsTo(Household::class);
    }

    public function filecat()
    {
        return $this->belongsTo(Filecat::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function bank()
    {
        return $this->belongsTo(Bank::class);
    }

     public function files()
    {
        return $this->hasMany(File::class);
    }

    public function getDaymonthyearAttribute(){

        return $this->datetime->isoformat ('D MMMM, Y');
    }

    public function getUrlhouseholdAttribute(){

        return route('household.household-view',$this->household_id);
    }

    public function getUrltransactioneditAttribute(){

        return route('transaction.transaction-edit',$this);
    }

    public function getUrltransactiondeleteAttribute(){

        return route('transaction.transaction-delete',$this);
    }
    public function getUrltransactionsfileAttribute(){

        return route('site.transactions',$this);
    }






}
