<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
class Household extends Model
{

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'date'
    ];

    protected $fillable = [

        'date',
        'user_id'
    ];

    protected $appends = [
        'monthlyincomingsum',
        'monthlyoutgoingsum',
        'extraincomingsum',
        'extraoutgoingsum',
        'totaloutgoingsum',
        'totalincomingsum',
        'totaloversum',
        'urlhouseholddelete',
        'urlhouseholdview',
        'monthyear'

    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
        public function transactions()
    {
        return $this->hasMany(Transaction::class);
    }

    public function monthlyincoming()
    {
        return $this->hasMany(Transaction::class)->where([
            ['transactions.monthly','Yes'],
            ['transactions.type','Incoming']
        ]);
    }

    public function monthlyoutgoing()
    {
        return $this->hasMany(Transaction::class)->where([
            ['transactions.monthly','Yes'],
            ['transactions.type','Outgoing']
        ]);
    }

    public function extraincoming()
    {
        return $this->hasMany(Transaction::class)->where([
            ['transactions.monthly','No'],
            ['transactions.type','Incoming']
        ]);
    }

     public function extraoutgoing()
    {
        return $this->hasMany(Transaction::class)->where([
            ['transactions.monthly','No'],
            ['transactions.type','Outgoing']
        ]);
    }

     public function totalincoming()
    {
        return $this->hasMany(Transaction::class)->where([
            ['transactions.type','Incoming']
        ]);
    }

     public function totaloutgoing()
    {
        return $this->hasMany(Transaction::class)->where([
            ['transactions.type','Outgoing']
        ]);
    }

    public function getMonthlyincomingsumAttribute()
    {
        return $this->monthlyincoming->sum('price');
    }

    public function getMonthlyoutgoingsumAttribute()
    {
        return $this->monthlyoutgoing->sum('price');
    }

    public function getExtraincomingsumAttribute()
    {
        return $this->extraincoming->sum('price');
    }

    public function getExtraoutgoingsumAttribute()
    {
        return $this->extraoutgoing->sum('price');
    }



    
    public function getTotaloutgoingsumAttribute()
    {
        return $this->totaloutgoing->sum('price');
    }

    public function getTotalincomingsumAttribute()
    {
        return $this->totalincoming->sum('price');
    }

    public function getTotaloversumAttribute()
    {
        return $this->totalincomingsum - $this->totaloutgoingsum;
    }
    public function getUrlhouseholdviewAttribute(){

        return route('household.household-view',$this);
    }

    public function getUrlhouseholddeleteAttribute(){

        return route('household.household-delete',$this);
    }
    public function getMonthyearAttribute(){

        return $this->date->isoFormat('MMMM - Y');
    }


}