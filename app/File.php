<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class File extends Model
{




    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */

    protected $dates = [

        'datetime',
        'created_at',
        'updated_at',
        'latest_payday',
    ];

    protected $fillable = [

        'filename',
        'path',
        'datetime',
        'user_id',
        'transaction_id',
        'filecat_id',
        'latest_payday',
        'status',
        'description'
    ];

    protected $appends =[
        'monthyear',
        'paydate',
        'payday',
        'urlfiledelete',
        'urlfileedit',
        'urlfileopen',
        'urlfiledownload',
        'urltransaction',


    ];
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function transaction()
    {
        return $this->belongsTo(Transaction::class);
    }

    public function filecat()
    {
        return $this->belongsTo(Filecat::class);
    }

    public function getMonthyearAttribute(){

        return $this->datetime->isoformat ('D MMMM, Y');
    }
    public function getPaydateAttribute(){

        return optional($this->latest_payday)->isoformat ('D MMMM, Y');
    }
    public function getPaydayAttribute(){

        return optional($this->latest_payday)->format ('Y-m-d');
    }

    public function getUrlfileeditAttribute(){

        return route('file.file-edit',$this);
    }
    public function getUrlfiledeleteAttribute(){

        return route('file.file-delete',$this);
    }
    public function getUrlfileopenAttribute(){

        return route('openFile', $this->path);
    }
    public function getUrlfiledownloadAttribute(){

        return route('getFile', $this->path);
    }
    public function getUrltransactionAttribute(){

        if (!$this->transaction_id){
            return null;}

        return route('transactions.transaction',$this->transaction_id);
    }



}
