<?php

namespace App\Console\Commands;

use App\Household;
use App\Transaction;
use App\User;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Foundation\Inspiring;

class CreateHouseholdCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sam:household';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new household with monthly transactions';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users = User::all();

        foreach ($users as $user) {
            $household_latest = Household::where('user_id', $user->id)->orderBy('date', 'desc')->first();
            if (!$household_latest) {
                $this->error('Gebruiker heeft geen huishoudens');
                continue;
            }

            $diff_dates = Carbon::now()->diff(Carbon::parse($household_latest->date));
            if (!$diff_dates->invert || $diff_dates->days >= 35) {
                $this->error('Huishouden ligt in de toekomst of te ver in het verleden');
                continue;
            }

            $new_date = Carbon::parse($household_latest->date)->addMonth(1)->firstOfMonth();
            $start_period = Carbon::parse($household_latest->date)->firstOfMonth();
            $end_period = Carbon::parse($household_latest->date)->lastOfMonth();

            $household = Household::create([
                'user_id' => $user->id,
                'date' => $new_date->format('Y-m-d'),
            ]);
            $this->info('Huishouden voor gebruiker '.$user->id.' aangemaakt');

            $transactions = Transaction::where('user_id', $user->id)
                                    ->where('datetime', '>=', $start_period)
                                    ->where('datetime', '<=', $end_period)
                                    ->where('monthly', 'yes')
                                    ->get();

            /** @var Transaction $transaction */
            foreach ($transactions as $transaction) {
                $new_transaction = $transaction->getAttributes();
                unset($new_transaction['id']);
                $new_transaction['datetime'] = $transaction->datetime->addMonth(1)->format('Y-m-d H:i:s');
                Transaction::create($new_transaction);
            }



        }

        $this->info('Hii');
        $this->warn('Hii');
        $this->error('Hii');




    }
}
