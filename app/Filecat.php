<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Filecat extends Model
{


    protected $fillable = [

        'cat',
        'user_id',
        'parent_id',

    ];

    protected $appends = [
        'urlcategoryedit',
        'urlcategorydelete',
        'Urltransactionfile',
        'urlfilecategory',
        'urltransactionfile'

    ];

    protected $table = 'filecat';

    public function File()
    {
        return $this->hasMany(File::class);
        
    }

    public function Transaction()
    {
        return $this->hasMany(Transaction::class);
    }

    public function User()
    {
        return $this->belongsTo(File::class);
    } 

    public function Filecatsub()
    {
        return $this->hasMany(Filecat::class, 'parent_id');
    }

    public static function boot()
    {
        parent::boot();

        static::deleting(function (Filecat $cat) {
            $cat->Filecatsub()->each(function (Filecat $cat) {
                $cat->delete();
            });
        });
    }

    public function parent()
    {
        return $this->belongsTo(static::class, 'parent_id');
    }

    public function getUrlcategoryeditAttribute(){

        return route('cat.category-edit',$this);
    }

    public function getUrlcategorydeleteAttribute(){

        return route('cat.category-delete',$this);
    }
    public function getUrlfilecategoryAttribute(){

        return route('cat.category-media',$this);
    }
    public function getUrltransactionfileAttribute(){

        return route('cat.category-transactions',$this);
    }


}
