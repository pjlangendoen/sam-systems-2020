<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Household;
use App\Filecat;
class User extends Authenticatable
{
    use Notifiable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email','lastname', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function households()
    {
        return $this->hasMany(Household::class);
    }
    public function files()
    {
        return $this->hasMany(File::class);
    }

    public function transactions()
    {
        return $this->hasMany(Transaction::class);
    }

    public function banks()
    {
        return $this->hasMany(Bank::class);
    }

    public function filecatparent()
    {

    return $this->hasMany(Filecat::class)->where('filecat.parent_id', null);
    }




    
}
