<?php

namespace App\Exports;

use App\Household;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class Householdexport implements FromCollection,Withheadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {

        return Household::all();

    }


    public function headings(): array
    {
        return [
            'Month',
            'Fixed Incoming',
            'Fixed Outgoing',
            'Monthly Incoming',
            'Monthy Outgoing',
            'Total Incoming',
            'Total Outgoing',
            'Balans',
        ];
    }

}
