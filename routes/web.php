<?php
Route::get('/lang/{locale}', function($locale){
    session()->put ('locale', $locale);
    return redirect()->back();
});

require 'web/site.php';
require 'web/household.php';
require 'web/transaction.php';
require 'web/file.php';
require 'web/category.php';
require 'web/bank.php';



Auth::routes();




