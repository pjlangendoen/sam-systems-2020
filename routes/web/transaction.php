<?php

Route::group(['prefix' => 'admin', 'namespace' => 'Site', 'middleware' => 'auth'], function () {


    /* Route Get */

    Route::get('/transactions/list', function(\Illuminate\Http\Request $request){
        $transactions = $request->user()
            ->transactions()
            ->with(['household', 'bank', 'files', 'filecat'])
            ->when($request->input('id'), function ($query, $id) {
                $query->whereHas('filecat', function ($query) use ($id) {
                    $query->where('id', $id);
                });
            })
            ->when($request->input('bank_id'), function ($query, $id) {
                $query->whereHas('bank', function ($query) use ($id) {
                    $query->where('id', $id);
                });
            })
            ->when($request->input('transaction_id'), function ($query, $id) {
                    $query->where('id', $id);
            })
            ->get();

        return response()->json($transactions);
    });

    Route::get('/transactions', [
        'as' => 'transaction.transactions',
        'uses' => 'TransactionController@transaction'
    ]);
    Route::get('/transactions/{transactionid}', [
        'as' => 'transactions.transaction',
        'uses' => 'TransactionController@transactions'
    ]);
    Route::get('/category/transactions/{categoryid}', [
        'as' => 'cat.category-transactions',
        'uses' => 'TransactionController@transactionscat'
    ]);

    Route::get('/banks/transactions/{bankid}', [
        'as' => 'bank.transactions',
        'uses' => 'TransactionController@transactionbank'
    ]);

    Route::get('/household/transactions/{householdid}', [
        'as' => 'household.transactions',
        'uses' => 'TransactionController@transactionhousehold'
    ]);


    Route::get('/transaction-delete/{transactionid}', [
        'as' => 'transaction.transaction-delete',
        'uses' => 'TransactionController@transactiondelete'
    ]);

    Route::get('/transaction-new/{householdid}', [
        'as' => 'transaction.transaction-new',
        'uses' => 'TransactionController@transactionnew'
    ]);

    Route::get('/household-view/{householdid}/transaction-new/', [
        'as' => 'transaction-household.transaction-new',
        'uses' => 'TransactionController@transactionnew'
    ]);

    Route::get('/transaction-new/', [
        'as' => 'transactions.transaction-new',
        'uses' => 'TransactionController@transactionsnew'
    ]);

    Route::get('/transaction-new/{householdid}/upload/{transactionid}', [
        'as' => 'transaction.transaction-new.upload',
        'uses' => 'TransactionController@transactionnewUpload'
    ]);

    Route::get('/transaction-edit/{transactionid}/upload/', [
        'as' => 'transaction.transaction-edit.upload',
        'uses' => 'TransactionController@transactionnewUpload'
    ]);

    Route::get('/transaction-edit/{transactionid}', [
        'as' => 'transaction.transaction-edit',
        'uses' => 'TransactionController@transactionedit'
    ]);




    /* Route post */

    Route::post('/household-view/{householdid}/transaction-new/', [
        'as' => 'transaction.transaction-new',
        'uses' => 'TransactionController@transactionnewsave'
    ]);

    Route::post('/transaction-new/', [
        'as' => 'transactions.transaction-new',
        'uses' => 'TransactionController@transactionsnewsave'
    ]);

    Route::post('/transaction-new/{householdid}/upload/{transactionid}', [
        'as' => 'transaction.transaction-new.upload',
        'uses' => 'FileController@filenewupload'
    ]);

    Route::post('/transaction-edit/{transactionid}/upload/', [
        'as' => 'transaction.transaction-edit.upload',
        'uses' => 'FileController@filenewupload'
    ]);

    Route::post('/transaction-edit/{transactionid}', [
        'as' => 'transaction.transaction-edit',
        'uses' => 'TransactionController@transactioneditsave'
    ]);


});


Route::bind('transactionid', function ($value){
    return \App\Transaction::where('id', $value)
        ->firstOrFail();
});

Route::bind('bankid', function ($value){
    return \App\Bank::where('id', $value)
        ->firstOrFail();
});

Route::bind('householdid', function ($value){
    return \App\Household::where('id', $value)
        ->firstOrFail();
});