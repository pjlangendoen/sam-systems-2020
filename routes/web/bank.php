<?php

Route::group(['prefix' => 'admin', 'namespace' => 'Site', 'middleware' => 'auth'], function () {

/* Route Get */

    Route::get('/banks/list', function(){
        return response()->json(Auth::user()->banks()->with(['transactions'])->get());
    });

    Route::get('/banks', [
        'as' => 'bank.banks',
        'uses' => 'BankController@bank'
    ]);


    Route::get('/bank-new', [
        'as' => 'bank.bank-new',
        'uses' => 'BankController@banknew'
    ]);

    Route::get('/bank-edit/{bankid}', [
        'as' => 'bank.bank-edit',
        'uses' => 'BankController@bankedit'
    ]);


    Route::get('/bank-delete/{bankid}', [
        'as' => 'bank.bank-delete',
        'uses' => 'BankController@bankdelete'
    ]);


/* Route post */

    Route::post('/bank-new', [
        'as' => 'bank.bank-new',
        'uses' => 'BankController@banknewsave'
    ]);

    Route::post('/bank-edit/{bankid}', [
        'as' => 'bank.bank-edit',
        'uses' => 'BankController@bankeditsave'
    ]);


});


     Route::bind('bankid', function ($value){
        return \App\Bank::where('id', $value)
        ->firstOrFail();
    });
