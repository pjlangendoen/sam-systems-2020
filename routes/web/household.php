<?php

Route::group(['prefix' => 'admin', 'namespace' => 'Site', 'middleware' => 'auth'], function () {


 /* Transaction get */
    Route::get('/households/list', function(){
        return response()->json(Auth::user()->Households()->get());
    });

    Route::get('/household', [
        'as' => 'household.household',
        'uses' => 'HouseholdController@household'
    ]);

    Route::get('/household-view/{householdid}', [
        'as' => 'household.household-view',
        'uses' => 'HouseholdController@householdview'
    ]);
    Route::get('/household-export/{householdid}', [
        'as' => 'household.household-export',
        'uses' => 'HouseholdController@export'
    ]);

    Route::get('/household-exportall', [
        'as' => 'household.household-exportall',
        'uses' => 'HouseholdController@exportall'
    ]);

    Route::get('/household-new', [
        'as' => 'household.household-new',
        'uses' => 'HouseholdController@householdnew'
    ]);

    Route::get('/household-edit/{householdid}', [
        'as' => 'household.household-edit',
        'uses' => 'HouseholdController@householdedit'
    ]);

    Route::get('/household-delete/{householdid}', [
        'as' => 'household.household-delete',
        'uses' => 'HouseholdController@householddelete'
    ]);

    /* Transaction post */

    Route::post('/household-edit/{householdid}', [
        'as' => 'household.household-edit',
        'uses' => 'HouseholdController@householdeditsave'
    ]);

    Route::post('/household-new', [
        'as' => 'household.household-new',
        'uses' => 'HouseholdController@householdnewsave'
    ]);


});

 /* Bindings */

     Route::bind('householdid', function ($value){
        return \App\Household::where('id', $value)
        ->firstOrFail();
    });