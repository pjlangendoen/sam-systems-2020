<?php





Route::group(['namespace' => 'Site'], function () {

    Route::get('/', function () {

        if (Auth::check()) {
            return redirect()->route('site.dashboard');

        }

        return view('auth.login');
    });

});

/* get */

Route::group(['namespace' => 'Auth'], function () {
    Route::get('/logout', [
        'as' => 'auth.logout',
        'uses' => 'LoginController@logout'
    ]);
});



Route::group(['prefix' => 'admin', 'namespace' => 'Site', 'middleware' => 'auth'], function () {
    Route::get('/', [
        'as' => 'site.dashboard',
        'uses' => 'HomeController@index'

    ]);


    Route::get('/user/{userid}', [
        'as' => 'site.user',
        'uses' => 'HomeController@user'
    ]);

    Route::get('/info', [
        'as' => 'site.info',
        'uses' => 'HomeController@info'
    ]);

    Route::get('/feedback', [
        'as' => 'forms.feedback',
        'uses' => 'HomeController@feedback'
    ]);




/* User post */

    Route::post('/user/{userid}', [
        'as' => 'site.user',
        'uses' => 'HomeController@usersave'
    ]);
});

/* Binding */

 Route::bind('userid', function ($value){
        return \App\User::where('id', $value)
        ->firstOrFail();

    });


 