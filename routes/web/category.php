<?php

Route::group(['prefix' => 'admin', 'namespace' => 'Site', 'middleware' => 'auth'], function () {


/* Route Get*/
    Route::get('/category/list', function(){
        return response()->json(Auth::user()->filecatparent()->with('Filecatsub.transaction','Filecatsub.file', 'Transaction','File')->get());
    });


    Route::get('/category', [
        'as' => 'cat.category',
        'uses' => 'CategoryController@categorys'
    ]);

    Route::get('/category-new', [
        'as' => 'cat.category-new',
        'uses' => 'CategoryController@categorynew'
    ]);


       Route::get('/category-edit/{categoryid}', [
        'as' => 'cat.category-edit',
        'uses' => 'CategoryController@categoryedit'
    ]);

    Route::get('/category-delete/{categoryid}', [
        'as' => 'cat.category-delete',
        'uses' => 'CategoryController@categorydelete'
    ]);




/* Route post */
Route::post('/category-new', [
        'as' => 'cat.category-new',
        'uses' => 'CategoryController@categorynewsave'
    ]);

Route::post('/category-edit/{categoryid}', [
        'as' => 'cat.category-edit',
        'uses' => 'CategoryController@categoryeditsave'
    ]);

});

/* Binding */

     Route::bind('categoryid', function ($value){
        return \App\Filecat::where('id', $value)
        ->firstOrFail();
    });