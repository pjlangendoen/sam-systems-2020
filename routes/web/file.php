<?php

Route::group(['prefix' => 'admin', 'namespace' => 'Site', 'middleware' => 'auth'], function () {



/* Route Get */
    Route::get('/media/list', function(\Illuminate\Http\Request $request){
        $files = $request->user()
            ->files()
            ->with(['transaction','filecat'])
            ->when($request->input('id'), function ($query, $id) {
                $query->whereHas('filecat', function ($query) use ($id) {
                    $query->where('id', $id);
                });
            })
            ->when($request->input('transaction_id'), function ($query, $id) {
                $query->whereHas('transaction', function ($query) use ($id) {
                    $query->where('id', $id);
                });
            })
            ->when($request->input('file_id'), function ($query, $id) {
                    $query->where('id', $id);
            })

            ->get();

        return response()->json($files);
    });


    Route::get('/media', [
        'as' => 'site.media',
        'uses' => 'FileController@media'
    ]);

    Route::get('/media/{fileid}', [
        'as' => 'site.mediaid',
        'uses' => 'FileController@mediaid'
    ]);



    Route::get('/transactions/media/{transactionid}', [
        'as' => 'site.transactions',
        'uses' => 'FileController@mediafiles'
    ]);
    Route::get('/category/media/{categoryid}', [
        'as' => 'cat.category-media',
        'uses' => 'Filecontroller@mediafilescat'
    ]);


    Route::get('/file-new', [
        'as' => 'file.file-new',
        'uses' => 'FileController@filenew'
    ]);

    Route::get('/file-edit/{fileid}', [
        'as' => 'file.file-edit',
        'uses' => 'FileController@fileedit'
    ]);

    Route::get('media/download/{filename}', [
        'as' => 'getFile', 
        'uses' => 'FileController@get_file'
    ]);

    Route::get('media/view/{filename}', [
        'as' => 'openFile', 
        'uses' => 'FileController@open_file'
    ]);

    Route::get('/file-delete/{fileid}', [
        'as' => 'file.file-delete',
        'uses' => 'FileController@filedelete'
    ]);




/* Route post */
Route::post('/file-new', [
        'as' => 'file.file-new',
        'uses' => 'FileController@filenewupload'
    ]);

Route::post('/file-edit/{fileid}', [
        'as' => 'file.file-edit',
        'uses' => 'FileController@fileeditsave'
    ]);



});

/* Binding */
     Route::bind('transactionid', function ($value){
        return \App\Transaction::where('id', $value)
        ->firstOrFail();
    });

    Route::bind('fileid', function ($value){
        return \App\File::where('id', $value)
            ->firstOrFail();
    });