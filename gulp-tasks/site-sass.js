module.exports = function (gulp, plugins, paths) {
    return function () {
        return gulp.src(paths.input.site.sass.dir + paths.input.site.sass.filename)
            .pipe(plugins.sass().on('error', plugins.sass.logError))
            .pipe(plugins.rename(paths.output.site.sass.filename))
            .pipe(gulp.dest(paths.output.manifest.cache_dir + paths.output.site.sass.dir));
    };
};