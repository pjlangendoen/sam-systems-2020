module.exports = function (gulp, plugins, paths) {
    return function () {
        return gulp.src(paths.input.site.script)
            .pipe(plugins.concat(paths.output.site.script.filename))
            .pipe(gulp.dest(paths.output.manifest.cache_dir + paths.output.site.script.dir));
    };
};