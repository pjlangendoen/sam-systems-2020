module.exports = function (gulp, plugins, paths) {
    return function () {
        gulp.src(paths.input.vendor.style)
            .pipe(gulp.dest('public/build/assets/css'));

        // Copy vendor stylesheets
        gulp.src(paths.input.vendor.script)
            .pipe(gulp.dest('public/build/assets/js'));

        var build_files = gulp.src(paths.output.manifest.cache_dir + paths.output.manifest.output_dir + '**');

        // Check productiob flag
        if (plugins.util.env.production) {
            var build_files = build_files
                .pipe(plugins.minify({
                    ext:{
                        min:'.js'
                    }
                }))
                .pipe(plugins.cleanCss({ processRemoteImport: false }));
        }

        return build_files
            .pipe(plugins.rev())
            .pipe(gulp.dest(paths.output.manifest.output_dir))
            .pipe(plugins.rev.manifest())
            .pipe(gulp.dest(paths.output.manifest.output_dir));
    };
};