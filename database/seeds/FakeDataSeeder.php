<?php

use Illuminate\Database\Seeder;

class FakeDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        $limit = 10;
        for ($i=0;$i<$limit;$i++) {
            \App\Bank::create([
                'name' => $faker->sentences(1, true),
                'user_id' => \App\User::inRandomOrder()->first()->id,
            ]);
            \App\Household::create([
                'date' => \Carbon\Carbon::now(),
                'user_id' => \App\User::inRandomOrder()->first()->id,
            ]);
        }

        $limit = 1000;
        for ($i=0;$i<$limit;$i++) {
            \App\Transaction::create([
                'user_id' => \App\User::inRandomOrder()->first()->id,
                'name' => $faker->sentences(1, true),
                'price' => (rand(100,100000) / 100),
                'monthly' => rand(0,1) ? 'yes' : 'no',
                'bank_id'  => \App\Bank::inRandomOrder()->first()->id,
                'household_id' => \App\Household::inRandomOrder()->first()->id,
                'type' => rand(0,1) ? 'incoming' : 'outgoing',
                'datetime' => \Carbon\Carbon::now(),
            ]);
        }
    }
}
