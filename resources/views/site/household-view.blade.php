@extends('layouts.Layoutpages')


@section('title')
    {{ trans('household.household_budgeting') }} {{ $household->date->isoFormat('MMMM - Y')}}
@endsection

@section('content')

                <div class="row">

                      <div class="col-md-12">
                        <div class="card">
                           <div class="header">
                              <div class="add-new">
                                  <h4 class="title"> <b>{{ trans('household.household_view') }} | <span style="text-decoration:underline;" class="capitalize">{{ $household->date->isoFormat('MMMM - Y')}}</span></b></h4>
                                  <a class="btn btn-primary" href="{{route('transaction-household.transaction-new', $household)}}">
                                      {{ trans('household.add_transaction') }} <i class="fa fa-plus" aria-hidden="true"></i>
                                  </a>
                              </div>
                                <p class="category"> {{ trans('household.transactions_view') }}</p>
                            </div>
                        </div>
                    </div>

<!-- Table left 1 Fixed monthly incoming -->
                    <div class="col-md-6 left-household">
                        <div class="card">
                            <div class="header">
                                <h4 class="title"> {{ trans('household.fixed_income') }}</h4>

                            </div>
                            <div class="content table-responsive table-full-width">
                                <table id="table_household" class="table table-hover table-striped">
                                    <thead>
                                        <th>{{ trans('household.name') }}</th>
                                        <th>{{ trans('household.price') }}</th>
                                        <th>{{ trans('household.files') }}</th>
                                        <th>{{ trans('household.edit') }}</th>
                                        <th>{{ trans('household.delete') }}</th>

                                    </thead>
                                    <tbody>
                                        @forelse($household->monthlyincoming as $transaction)
                                     <tr>
                                         <td class="smalltekst-home">{{ $transaction->name }}</td>
                                            <td>&euro; {{ $transaction->price }}</td>
                                         <td><a class="badge" href="{{route('site.transactions',$transaction)}}">
                                                 {{count($transaction->files)}}
                                             </a>
                                         </td>
                                            <td><a class="btn btn-primary" href="{{route('transaction.transaction-edit',$transaction)}}"><i class="fa fa-pencil-alt" aria-hidden="true"></i></a></td>
                                            <td><a onclick="return ConfirmDelete()"  class="btn btn-danger" href="{{route('transaction.transaction-delete',$transaction)}}"><i class="fa fa-trash" aria-hidden="true"></i></a></td>
                                        </tr>
                                        @empty
                                        <tr><td colspan="7">{{ trans('household.transactions_no') }}</td></tr>
                                        @endforelse
   
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>

                    <!-- Table left 2 Fixed Monthly outgoing -->
                    <div class="col-md-6 right-household">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">{{ trans('household.fixed_outgoing') }}</h4>
                            </div>
                            <div class="content table-responsive table-full-width">
                                <table id="table_household" class="table table-hover table-striped">
                                    <thead>
                                    <th>{{ trans('household.name') }}</th>
                                    <th>{{ trans('household.price') }}</th>
                                    <th>{{ trans('household.files') }}</th>
                                    <th>{{ trans('household.edit') }}</th>
                                    <th>{{ trans('household.delete') }}</th>
                                    </thead>
                                    <tbody>
                                    @forelse($household->monthlyoutgoing as $transaction)
                                        <tr>
                                            <td class="smalltekst-home">{{ $transaction->name }}</td>
                                            <td>&euro; {{ $transaction->price }}</td>
                                            <td><a class="badge" href="{{route('site.transactions',$transaction)}}">
                                                    {{count($transaction->files)}}
                                                </a>
                                            </td>
                                            <td><a class="btn btn-primary" href="{{route('transaction.transaction-edit',$transaction)}}"><i class="fa fa-pencil-alt" aria-hidden="true"></i></a></td>
                                            <td><a class="btn btn-danger" href="{{route('transaction.transaction-delete',$transaction)}}"><i class="fa fa-trash" aria-hidden="true"></i></a></td>
                                        </tr>
                                    @empty
                                        <tr><td colspan="7">{{ trans('household.transactions_no') }}</td></tr>
                                    @endforelse

                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>

<!-- Table right 1 Extra Incoming -->
                    <div class="col-md-6 left-household">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">{{ trans('household.incoming_extra') }}</h4>
                            </div>
                            <div class="content table-responsive table-full-width">
                                <table id="table_household" class="table table-hover table-striped">
                                    <thead>
                                    <th>{{ trans('household.name') }}</th>
                                    <th>{{ trans('household.price') }}</th>
                                    <th>{{ trans('household.files') }}</th>
                                    <th>{{ trans('household.edit') }}</th>
                                    <th>{{ trans('household.delete') }}</th>
                                    </thead>
                                    <tbody>
                                    @forelse($household->extraincoming as $transaction)
                                        <tr>
                                            <td class="smalltekst-home">{{ $transaction->name }}</td>
                                            <td>&euro; {{ $transaction->price }}</td>
                                            <td><a class="badge" href="{{route('site.transactions',$transaction)}}">
                                                    {{count($transaction->files)}}
                                                </a>
                                            </td>
                                            <td><a class="btn btn-primary" href="{{route('transaction.transaction-edit',$transaction)}}"><i class="fa fa-pencil-alt" aria-hidden="true"></i></a></td>
                                            <td><a class="btn btn-danger" href="{{route('transaction.transaction-delete',$transaction)}}"><i class="fa fa-trash" aria-hidden="true"></i></a></td>
                                        </tr>
                                    @empty
                                        <tr><td colspan="7">{{ trans('household.transactions_no') }}</td></tr>
                                    @endforelse

                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>


<!-- Table right2  Extra Outgoing-->
                    <div class="col-md-6 right-household">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">{{ trans('household.outgoing_extra') }}</h4>
                            </div>
                            <div class="content table-responsive table-full-width">
                                <table id="table_household" class="table table-hover table-striped">
                                    <thead>
                                    <th>{{ trans('household.name') }}</th>
                                    <th>{{ trans('household.price') }}</th>
                                    <th>{{ trans('household.files') }}</th>
                                    <th>{{ trans('household.edit') }}</th>
                                    <th>{{ trans('household.delete') }}</th>
                                    </thead>
                                    <tbody>
                                    @forelse($household->extraoutgoing as $transaction)
                                        <tr>
                                            <td class="smalltekst-home">{{ $transaction->name }}</td>
                                            <td>&euro; {{ $transaction->price }}</td>
                                            <td><a class="badge" href="{{route('site.transactions',$transaction)}}">
                                                    {{count($transaction->files)}}
                                                </a>
                                            </td>
                                            <td><a class="btn btn-primary" href="{{route('transaction.transaction-edit',$transaction)}}"><i class="fa fa-pencil-alt" aria-hidden="true"></i></a></td>
                                            <td><a class="btn btn-danger" href="{{route('transaction.transaction-delete',$transaction)}}"><i class="fa fa-trash" aria-hidden="true"></i></a></td>
                                        </tr>
                                    @empty
                                        <tr><td colspan="7">{{ trans('household.transactions_no') }}</td></tr>
                                    @endforelse

                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>




<!-- Table under Al Totals -->
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">{{ trans('household.totals') }}</h4>
                            </div>
                            <div class="content table-responsive table-full-width">
                                <table id="table-household" class="table table-hover table-striped">
                                    <thead>
                                    <th>{{ trans('household.name') }}</th>
                                    <th>{{ trans('household.price') }}</th>

                                    </thead>
                                    <tbody>
                                     <tr>
                                            <td><b>{{ trans('household.fixed_income') }}</b></td>
                                            <td class="incoming">&euro; + {{$household->monthlyincomingsum}}</td>
                                        </tr>
                                     <tr>
                                            <td><b>{{ trans('household.fixed_outgoing') }}</b></td>
                                            <td class="outgoing">&euro; - {{$household->monthlyoutgoingsum}}</td>
                                        </tr>

                                     <tr>
                                            <td><b>{{ trans('household.incoming_extra') }}</b></td>
                                            <td class="incoming">&euro; + {{$household->extraincomingsum}}</td>
                                        </tr>

                                     <tr>
                                            <td><b>{{ trans('household.outgoing_extra') }}</b></td>
                                            <td class="outgoing">&euro; - {{$household->extraoutgoingsum}}</td>
                                        </tr>
                                    <tr>
                                        <td><b>{{ trans('household.balance') }}</b></td>
                                        @if(strpos($household->totaloversum, '-') === 0)
                                            <td class="outgoing">&euro; {{$household->totaloversum}}</td>
                                        @else
                                            <td class="incoming">&euro; + {{$household->totaloversum}}</td>
                                        @endif
                                    </tr>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>

                </div>

@endsection
