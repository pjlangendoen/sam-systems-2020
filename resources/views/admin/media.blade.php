@extends('layouts.Layoutpages')
@section('title')
    {{ trans('media.media') }}
@endsection

@section('content')


                <div class="row">
                    <div class="sub-mobile">
                        <div class="mobile-dashboard-buttons-sub">
                            <a class="btn btn-primary" href="{{route('file.file-new')}}">
                                {{ trans('media.add_media') }} <i class="fa fa-plus" aria-hidden="true"></i>
                            </a>
                            @if ($files->whereNotNull('latest_payday')->where('status', 'Open')->isNotEmpty())
                                <div class="openstaand">
                                    <div class="letop"><i class="fas fa-exclamation-triangle"></i> {{ trans('media.letop') }}</div>
                                    {{ trans('media.openstaand')}}
                                </div>
                            @endif
                        </div>
                        <input class="form-group" type="text" placeholder="{{ trans('media.search_name') }}" ng-model="search.description" ng-change="currentPage = 0">
                        <input class="form-group" type="text" placeholder="{{ trans('media.search_date') }}" ng-model="search.datetime" ng-change="currentPage = 0">
                        <input class="form-group" type="text" placeholder="{{ trans('media.search_payday') }}" ng-model="search.latest_payday" ng-change="currentPage = 0">

                        <select ng-options="status for status in ['Open', 'Payed']" class="form-control" ng-model="search.status" ng-change="currentPage = 0">
                            <option value="" disabled selected> {{ trans('media.search_open') }}</option>
                        </select>

                        <button id="clear-filter" class="btn btn-secondary" type="button" ng-click="search = {}">{{ trans('media.clear_filter') }}<i class="fa fa-times"></i></button>
                        <div class="subpage" ng-repeat="files in file | filter:{description: search.description, monthyear: search.datetime, paydate: search.latest_payday, status: search.status } | startFrom:currentPage*pageSize | limitTo:pageSize">

                            <button id="green" class="accordion" ng-if="files.status == 'Payed'"><b class="smalltekst" ng-bind="files.description"></b> | <span ng-bind="files.monthyear +' | {{ trans('media.closed_trans') }}'" class="sub-small-text capitalize"></span></button>
                           <button id="red" class="accordion" ng-if="files.status == 'Open' && !files.warning"><b class="smalltekst" ng-bind="files.description"></b> | <span ng-bind="files.monthyear +' | {{ trans('media.open_trans') }}'" class="sub-small-text capitalize"></span></button>
                         <button id="red" class="accordion" ng-if="files.status == 'Open' && files.warning"><b class="smalltekst" ng-bind="files.description"></b> | <span ng-bind="files.monthyear +' | {{ trans('media.open_trans') }}'" class="sub-small-text capitalize"></span><i class="fas fa-info-circle right-circle"></i></button>
                            <div class="panel-sub">

                                <td class="card-sub">
                                    <table class="table">
                                        <thead>

                                        <th>{{ trans('media.open') }}</th>
                                        <th>{{ trans('media.date') }}</th>
                                        <th>{{ trans('media.payday') }}</th>
                                        <th>{{ trans('media.transaction') }}</th>
                                        <th>{{ trans('media.category') }}</th>
                                        <th>{{ trans('media.status') }}</th>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td><a class="btn btn-primary-submedia" ng-href="[{files.urlfileopen}]" target="_Blank"><i class="fa fa-file" aria-hidden="true"></i></a></td>
                                            <td class="capitalize" ng-bind="files.monthyear"></td>


                                            <td ng-if="files.paydate == null" ><span style="color:red;">{{ trans('media.no_payday_m') }}</span></td>
                                            <td class="capitalize" ng-if="files.paydate != null && files.warning  && files.status == 'Payed'" ng-bind="files.paydate"></td>
                                           <td class="capitalize" ng-if="files.paydate != null && !files.warning  && files.status == 'Payed'" ng-bind="files.paydate"></td>
                                            <td class="capitalize" ng-if="files.paydate != null && !files.warning  && files.status == 'Open'" ng-bind="files.paydate"></td>

                                            <td ng-if="files.paydate != null && files.warning && files.status == 'Open'">
                                               <a tabindex="0"  data-trigger="focus" title="{{ trans('media.letop') }}" data-content="{{ trans('media.popover') }}" id="capitalize-warning" ng-if="files.paydate != null && files.warning && files.status == 'Open'" ng-bind="files.paydate"> </a>
                                                <span id="click" tabindex="0" data-trigger="focus" title="{{ trans('media.letop') }}" data-content="{{ trans('media.popover') }}"  ng-if="files.paydate != null && files.warning && files.status == 'Open'"><span class="fas fa-info-circle right-circle"></span></span></td>

                                            <td>
                                                <a ng-if="files.transaction_id != null" ng-bind="files.transaction.name" ng-href="[{files.urltransaction}]"></a>
                                                <span ng-if="files.transaction_id == null" style="color:red;">{{ trans('media.no_transaction_m') }}</span>

                                            </td>
                                            <td ng-if="files.filecat_id == null" class="empty">{{ trans('media.no_cat') }}</td>

                                            <td ng-if="files.filecat_id != null" ng-bind="files.filecat.cat"></td>


                                            <td ng-if="files.status == 'Open' || files.status == 'open'" class="red" ng-bind="'{{ trans('media.open_trans') }}'"></td>

                                            <td ng-if="files.status == 'Payed' || files.status == 'payed'" class="green" ng-bind="'{{ trans('media.closed_trans') }}'"></td>

                                            <div id="buttons-sub">
                                                <div class="buttons-subpage"><a class="btn btn-info" ng-href="[{files.urlfiledownload}]"><i class="fa fa-download" aria-hidden="true"></i></a></div>
                                                <div class="buttons-subpage"><a  class="btn btn-primary" ng-href="[{files.urlfileedit}]"><i class="fa fa-edit" aria-hidden="true"></i></a></div>
                                                <div class="buttons-subpage"><a onclick="return ConfirmDelete()" class="btn btn-danger" ng-href="[{files.urlfiledelete}]"><i class="fa fa-trash" aria-hidden="true"></i></a></div>

                                            </div>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <div ng-if="files.description != null" class="description">
                                        <b style="color:#9A9A9A;">{{ trans('media.description') }}</b><br/>
                                        <span ng-bind="files.description"></span>
                                    </div>

                                </div>
                            </div>
                        <div class="pagination">

                            <li class="paginate_button previous " id="table_previous">
                                <button ng-disabled="currentPage == 0" ng-click="currentPage=currentPage-1">
                                    {{ trans('media.previous') }}
                                </button> </li>

                            <li class="paginate_buttonsub">
                                [{currentPage+1}] / [{numberOfPages()}]
                            </li>

                            <li class="paginate_button next" id="table_next">
                                <button ng-disabled="currentPage >= getFiles().length/pageSize - 1" ng-click="currentPage=currentPage+1">
                                    {{ trans('media.next') }}
                                </button> </li>

                        </div>
                        </div>


                    <div class="sub-desktop">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">{{ trans('media.media') }}</h4>
                                <div class="add-new">
                                    <a class="btn btn-primary" href="{{route('file.file-new')}}">
                                        {{ trans('media.add_media') }}<i class="fa fa-plus" aria-hidden="true"></i>
                                    </a>
                                </div>
                                <p class="category"> {{ trans('media.subtitle') }}</p>
                            </div>

                            @if ($files->whereNotNull('latest_payday')->where('status', 'Open')->isNotEmpty())
                                   <div class="openstaand">
                                       <div class="letop"><i class="fas fa-exclamation-triangle"></i> {{ trans('media.letop') }}</div>
                                    {{ trans('media.openstaand')}}
                                   </div>
                                @endif

                            <div class="content table-responsive table-full-width">
                                <table id="table" class="table table-hover table-striped" exclude="delete|edit|download|id|verwijderen|wijzigen|open bestand|open file">

                                    <thead>
                                        <th>{{ trans('media.open') }}</th>
                                        <th>{{ trans('media.name') }}</th>
                                        <th>{{ trans('media.date') }}</th>
                                        <th>{{ trans('media.payday') }}</th>
                                        <th>{{ trans('media.transaction') }}</th>
                                        <th>{{ trans('media.category') }}</th>
                                        <th>{{ trans('media.status') }}</th>
                                        <th>{{ trans('media.download') }}</th>
                                        <th>{{ trans('media.edit') }}</th>
                                        <th>{{ trans('media.delete') }}</th>

                                    </thead>

                                    <tbody>
                                        @forelse($files as $file)
                                     <tr>

                                            <?php
                                            $extension = strtolower(explode(".", $file->path)[1]);
                                            $image_types = ['jpg','png','gif','jpeg'];
                                            $image_pdf = ['pdf'];
                                            $image_xlsx = ['xlsx'];
                                            $image_docx = ['docx'];
                                            ?>


                                            @if(in_array($extension, $image_types))
                                            <td><a href="{{route ('openFile', $file->path)}}" target=_Blank>
                                                <img src="{{route ('openFile', $file->path)}}" style="width:50px;">
                                            </td>
                                            @elseif(in_array($extension, $image_pdf))

                                            <td><a href="{{route ('openFile', $file->path)}}" target=_Blank>
                                                <i class="fa fa-file-pdf-o" aria-hidden="true" style="font-size:25px;"></i>
                                            </td>

                                            @elseif(in_array($extension, $image_xlsx))

                                            <td><a href="{{route ('openFile', $file->path)}}" target=_Blank>
                                                <i class="fa fa-file-excel-o" aria-hidden="true" style="font-size:25px;"></i>
                                            </td>

                                            @elseif(in_array($extension, $image_docx))

                                            <td><a href="{{route ('openFile', $file->path)}}" target=_Blank>
                                                <i class="fa fa-file-word-o" aria-hidden="true" style="font-size:25px;"></i>
                                            </td>

                                            @else

                                            <td></td>
                                            @endif



                                            <td>{{ $file->description }}</td>
                                            <td class="capitalize">{{ $file->datetime->isoformat ('D MMMM, Y')}}</td>

                                            @if ($file->latest_payday == null)
                                            <td class="empty">{{ trans('media.no_payday') }}</td>
                                            @elseif($file->latest_payday->isBetween('today', '+7 days') and ($file->status == 'Open'))
                                            <td class="capitalize-red">{{ $file->latest_payday->isoformat ('D MMMM, Y')}}
                                                <a tabindex="0" role="button" class="btn btn-lg btn-danger" data-toggle="popover" data-trigger="focus" title="{{ trans('media.letop') }}" data-content="{{ trans('media.popover') }}"><i class="fas fa-info-circle"></i></a></td>
                                            @else
                                            <td class="capitalize">{{ $file->latest_payday->isoformat ('D MMMM, Y')}}</td>
                                            @endif


                                            @if ($file->transaction_id == null)
                                            <td class="empty">{{ trans('media.no_transaction') }}</td>
                                            @else
                                            <td>{{ $file->transaction->name }}</td>
                                            @endif


                                                    @if ($cat = $file->filecat)
                                                    <td> <b style="font-size:16px;">{{ $cat->cat }}</b><br/>
                                                        @if ($parent = $cat->parent)

                                                            -- {{ $parent->cat }}</td>
                                                        @endif
                                                    @else
                                                    <td class="empty"> {{ trans('media.no_cat') }}</td>
                                                    @endif



                                            @if ($file->status === 'Payed')
                                                <td class="incoming"> <i class="fa fa-circle" aria-hidden="true"></i> {{ trans('media.payed') }} </td>
                                            @elseif ($file->status === 'Open')
                                                <td class="outgoing"> <i class="fa fa-circle" aria-hidden="true"></i> {{ trans('media.open_pay') }} </td>
                                            @else
                                                <td>{{ $file->status }}</td>
                                            @endif
                                            </td>
                                            <td><a class="btn btn-info" href="{{route ('getFile', $file->path)}}"><i class="fa fa-download" aria-hidden="true"></i></a></td>
                                            <td><a class="btn btn-primary" href="{{route('file.file-edit', $file)}}"><i class="fa fa-pencil-alt" aria-hidden="true"></i></a></td>
                                            <td><a onclick="return ConfirmDelete()" class="btn btn-danger" href="{{route('file.file-delete', $file)}}"><i class="fa fa-trash" aria-hidden="true"></i></a></td>
                                        </tr>
                                        @empty
                                        <tr><td colspan="100">{{ trans('media.no_media') }}</td></tr>
                                        @endforelse

                                  <tfoot>
                                  <th>{{ trans('media.open') }}</th>
                                  <th>{{ trans('media.name') }}</th>
                                  <th>{{ trans('media.date') }}</th>
                                  <th>{{ trans('media.payday') }}</th>
                                  <th>{{ trans('media.transaction') }}</th>
                                  <th>{{ trans('media.category') }}</th>
                                  <th>{{ trans('media.status') }}</th>
                                  <th>{{ trans('media.download') }}</th>
                                  <th>{{ trans('media.edit') }}</th>
                                  <th>{{ trans('media.delete') }}</th>
                                    </tfoot>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                    </div>

                </div>
                </div>
@endsection
@section('scripts')
    <script>
        app.controller('main',['$scope', '$http', '$filter', function($scope, $http, $filter){
            var url = "/admin/media/list";
            $scope.file = [];
            $scope.pageSize = 10;
            $scope.currentPage = 0;
            $scope.search = [];


            @if (!empty($fileCatId))
                url = url + "?id={{ $fileCatId }}";
            @elseif (!empty($transactionid))
                url = url + "?transaction_id={{ $transactionid }}";
            @elseif (count($files)==1)
                url = url + "?file_id=" + {{ $fileid }};
            @elseif (!empty($files))
                url = url;
            @endif


            $http.get(url).then(function(res){
                $scope.file = res.data.map(function (file) {
                    var today = moment().format('YYYY-MM-DD');
                    file.warning = moment(file.latest_payday).isBetween(today, moment(today).add(7, 'days'), null, '[]');
                    return file;
                });
            });

            $scope.getFiles = function () {
                return $filter('filter')($scope.file, {description: $scope.search.description, monthyear: $scope.search.datetime, paydate: $scope.search.latest_payday, status: $scope.search.status });
            }

            $scope.numberOfPages=function(){
                return Math.ceil($scope.getFiles().length/$scope.pageSize);
            }

        }]);



    </script>

@endsection