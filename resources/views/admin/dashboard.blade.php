@extends('layouts.Layoutpages')
@section('title')
Dasboard
@endsection
@section('content')

                <div class="row">


<!-- Transaction + File show -->
                    <div class="mobile-dashboard-buttons">
                            <a style="margin-top:0px;" class="btn btn-primary" href="{{route('transactions.transaction-new')}}">
                                {{ trans('dashboard.add_transactions') }} <i class="fa fa-plus" aria-hidden="true"></i>
                            </a>
                     </div>
                    <div class="col-md-6">
                        @if($transactions->count()== 0)
                         <div class="card-home" style="box-shadow: 0px 0px 6px 0px red; background: rgba(0,0,0,0.8);">
                                <div id="text">
                                {{ trans('dashboard.add_first_transactions') }} <a class="btn btn-primary" href="{{route('transactions.transaction-new')}}"><i class="fa fa-plus" aria-hidden="true"></i></a>
                                </div>
                                <div class="header">
                                    <h4 style="color:#000;" class="title-dashboard">{{ trans('dashboard.transactions') }} </h4>

                                </div>
                                <div style="z-index:-1; position:relative;" class="content table-responsive table-full-width">
                                    <table class="table table-hover table-striped">
                                        <thead>
                                        <th>{{ trans('dashboard.name') }}</th>
                                        <th>{{ trans('dashboard.price') }}</th>
                                        <th>{{ trans('dashboard.type') }}</th>
                                        <th>{{ trans('dashboard.date') }}</th>
                                        </thead>
                                        <tbody>
                                            <tr><td colspan="100">{{ trans('dashboard.no_transactions') }}</td></tr>
                                        </tbody>
                                        <tfoot>
                                        <th>{{ trans('dashboard.name') }}</th>
                                        <th>{{ trans('dashboard.price') }}</th>
                                        <th>{{ trans('dashboard.type') }}</th>
                                        <th>{{ trans('dashboard.date') }}</th>
                                        </tfoot>
                                    </table>

                                </div>
                            </div>
                        @else
                         <div class="card-home">
                            <div class="header">
                                <h4 class="title-dashboard">{{ trans('dashboard.transactions') }} </h4>
                                <div class="add-new-dashboard">
                                    <a class="btn btn-primary" href="{{route('transactions.transaction-new')}}"><i class="fa fa-plus" aria-hidden="true"></i></a>
                                </div>
                            </div>
                            <div class="content table-responsive table-full-width">
                                <table class="table table-hover table-striped">
                                    <thead>
                                    <th>{{ trans('dashboard.name') }}</th>
                                    <th>{{ trans('dashboard.price') }}</th>
                                    <th>{{ trans('dashboard.type') }}</th>
                                    <th>{{ trans('dashboard.date') }}</th>
                                    </thead>
                                    <tbody>
                                    @forelse($transactions as $transaction)
                                        <tr>
                                            <td class="smalltekst-home"><a href="{{route('transactions.transaction',$transaction)}}">{{ $transaction->name }}</td>
                                            <td>&euro; {{ $transaction->price }}</td>

                                            @if ($transaction->type == 'Incoming')
                                                <td class="incoming">+ {{ trans('dashboard.incoming') }}</td>
                                            @elseif ($transaction->type == 'Outgoing')
                                                <td class="outgoing">- {{ trans('dashboard.outgoing') }}</td>
                                            @else
                                                <td>{{ $transaction->type }}</td>
                                            @endif


                                            <td>{{ $transaction->datetime->format('d-m-Y')}}</td>
                                        </tr>
                                    @empty
                                        <tr><td colspan="100">{{ trans('dashboard.no_transactions') }}</td></tr>
                                    @endforelse

                                    </tbody>
                                    <tfoot>
                                    <th>{{ trans('dashboard.name') }}</th>
                                    <th>{{ trans('dashboard.price') }}</th>
                                    <th>{{ trans('dashboard.type') }}</th>
                                    <th>{{ trans('dashboard.date') }}</th>
                                    </tfoot>
                                </table>

                            </div>
                        </div>
                        @endif
                    </div>


                    <div class="mobile-dashboard-buttons">
                        <a class="btn btn-primary" href="{{route('file.file-new')}}">
                            {{ trans('dashboard.add_media') }} <i class="fa fa-plus" aria-hidden="true"></i>
                        </a>
                    </div>
                    <div class="col-md-6">

                        @if($files->count()== 0)
                            <div class="card-home" style="box-shadow: 0px 0px 6px 0px red; background: rgba(0,0,0,0.8);">
                                <div id="text">
                                    {{ trans('dashboard.add_first_file') }} <a class="btn btn-primary" href="{{route('file.file-new')}}"><i class="fa fa-plus" aria-hidden="true"></i></a>
                                </div>
                                <div class="header">
                                    <h4 style="color:#000;" class="title-dashboard">{{ trans('dashboard.media') }} </h4>
                                </div>

                                <div style="z-index:-1; position:relative;" class="content table-responsive table-full-width">

                                    <table class="table table-hover table-striped" >

                                        <thead>
                                        <th>{{ trans('dashboard.open_m') }}</th>
                                        <th>{{ trans('dashboard.name') }}</th>
                                        <th>{{ trans('dashboard.payday') }}</th>
                                        <th>{{ trans('dashboard.status') }}</th>
                                        </thead>
                                        <tbody>
                                            <tr><td colspan="100">{{ trans('dashboard.no_media') }}</td></tr>
                                        <tfoot>
                                        <th>{{ trans('dashboard.open_m') }}</th>
                                        <th>{{ trans('dashboard.name') }}</th>
                                        <th>{{ trans('dashboard.payday') }}</th>
                                        <th>{{ trans('dashboard.status') }}</th>
                                        </tfoot>
                                        </tbody>
                                    </table>

                                </div>
                            </div>
                        @else
                            <div class="card-home">

                            <div class="header">
                                <h4 class="title-dashboard">{{ trans('dashboard.media') }} </h4>
                                <div class="add-new-dashboard">
                                    <a class="btn btn-primary" href="{{route('file.file-new')}}"><i class="fa fa-plus" aria-hidden="true"></i></a>
                                </div>
                            </div>
                                @if ($files->whereNotNull('latest_payday')->where('status', 'Open')->isNotEmpty())
                                    <div class="openstaand-dashboard">
                                        <span class="letop-dashboard"><i class="fas fa-exclamation-triangle"></i> {{ trans('media.letop') }}</span> {{ trans('media.openstaand')}}
                                    </div>
                                @endif
                            <div class="content table-responsive table-full-width">
                                <table class="table table-hover table-striped small-td" >

                                    <thead>
                                    <th>{{ trans('dashboard.open_m') }}</th>
                                    <th>{{ trans('dashboard.name') }}</th>
                                    <th>{{ trans('dashboard.payday') }}</th>
                                    <th>{{ trans('dashboard.status') }}</th>
                                    </thead>

                                    <tbody>
                                    @forelse($files as $file)
                                        <tr>

                                            <?php
                                            $extension = strtolower(explode(".", $file->path)[1]);
                                            $image_types = ['jpg','png','gif','jpeg'];
                                            $image_pdf = ['pdf'];
                                            $image_xlsx = ['xlsx'];
                                            $image_docx = ['docx'];
                                            ?>


                                            @if(in_array($extension, $image_types))
                                                <td><a href="{{route ('openFile', $file->path)}}" target=_Blank>
                                                        <img src="{{route ('openFile', $file->path)}}" style="width:25px;">
                                                </td>
                                            @elseif(in_array($extension, $image_pdf))

                                                <td><a href="{{route ('openFile', $file->path)}}" target=_Blank>
                                                        <i class="fa fa-file-pdf-o" aria-hidden="true" style="font-size:25px;"></i>
                                                </td>

                                            @elseif(in_array($extension, $image_xlsx))

                                                <td><a href="{{route ('openFile', $file->path)}}" target=_Blank>
                                                        <i class="fa fa-file-excel-o" aria-hidden="true" style="font-size:25px;"></i>
                                                </td>

                                            @elseif(in_array($extension, $image_docx))

                                                <td><a href="{{route ('openFile', $file->path)}}" target=_Blank>
                                                        <i class="fa fa-file-word-o" aria-hidden="true" style="font-size:25px;"></i>
                                                </td>

                                            @else

                                                <td></td>
                                            @endif
                                                <td><a href="{{route('site.mediaid',$file)}}">{{ $file->description }}</a></td>

                                                @if ($file->latest_payday == null)
                                                    <td class="empty">{{ trans('media.no_payday') }}</td>
                                                @elseif($file->latest_payday->isBetween('today', '+7 days') and ($file->status == 'Open'))
                                                    <td class="capitalize-red">{{ $file->latest_payday->isoformat ('D MMMM, Y')}}
                                                        <a tabindex="0" role="button" class="btn btn-lg btn-danger" data-toggle="popover" data-trigger="focus" title="{{ trans('media.letop') }}" data-content="{{ trans('media.popover') }}"><i class="fas fa-info-circle right-circle"></i></a></td>
                                                @else
                                                    <td class="capitalize">{{ $file->latest_payday->isoformat ('D MMMM, Y')}}</td>
                                                @endif



                                            @if ($file->status === 'Payed')
                                                <td class="incoming"> <i class="fa fa-circle" aria-hidden="true"></i> {{ trans('dashboard.closed_trans') }} </td>
                                            @elseif ($file->status === 'Open')
                                                <td class="outgoing"> <i class="fa fa-circle" aria-hidden="true"></i> {{ trans('dashboard.open_trans') }} </td>
                                            @else
                                                <td>{{ $file->status }}</td>
                                                @endif
                                                </td>
                                        </tr>
                                    @empty
                                        <tr><td colspan="100">{{ trans('dashboard.no_media') }}</td></tr>
                                    @endforelse

                                    <tfoot>
                                    <th>{{ trans('dashboard.open_m') }}</th>
                                    <th>{{ trans('dashboard.name') }}</th>
                                    <th>{{ trans('dashboard.payday') }}</th>
                                    <th>{{ trans('dashboard.status') }}</th>
                                    </tfoot>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                        @endif
                    </div>

<!-- Household + Bank + Category hidden -->
                        <button class="accordion">{{ trans('dashboard.household_budgeting') }}</button>
                    <div class="panel-home">
                    <div class="mobile-dashboard-buttons">
                <a class="btn btn-primary" href="{{route('household.household-new')}}">
                    {{ trans('dashboard.add_households') }} <i class="fa fa-plus" aria-hidden="true"></i>
                </a>
                </div>
                    <div class="col-md-4">
                    <div class="card-home">
                        <div class="header">
                            <h4 class="title-dashboard">{{ trans('dashboard.household_budgeting') }} </h4>
                            <div class="add-new-dashboard">
                                <a class="btn btn-primary" href="{{route('household.household-new')}}"><i class="fa fa-plus" aria-hidden="true"></i></a>
                            </div>
                        </div>
                        <div class="content table-responsive table-full-width">
                            <table class="table table-hover table-striped">
                                <thead>
                                <th>{{ trans('dashboard.date') }}</th>
                                <th>{{ trans('dashboard.in') }}</th>
                                <th>{{ trans('dashboard.out') }}</th>
                                <th>{{ trans('dashboard.balance') }}</th>
                                </thead>
                                <tbody>
                                @forelse($households as $household)
                                    <tr>
                                        <td class="capitalize"><a href="{{route('household.household-view', $household)}}">{{ $household->date->isoFormat('MMMM - Y')}}</a></td>

                                        <td class="incoming">&euro; + {{$household->totalincomingsum}}</td>

                                        <td class="outgoing">&euro; - {{$household->totaloutgoingsum}}</td>

                                        @if(strpos($household->totaloversum, '-') === 0)
                                            <td class="outgoing">&euro; {{$household->totaloversum}}</td>
                                        @else
                                            <td class="incoming">&euro; + {{$household->totaloversum}}</td>
                                        @endif
                                    </tr>
                                @empty
                                    <tr><td colspan="100">{{ trans('dashboard.no_households') }}</td></tr>
                                @endforelse
                                <tfoot>
                                <th>{{ trans('dashboard.date') }}</th>
                                <th>{{ trans('dashboard.in') }}</th>
                                <th>{{ trans('dashboard.out') }}</th>
                                <th>{{ trans('dashboard.balance') }}</th>
                                </tfoot>
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
                    </div>

                        <button class="accordion">{{ trans('dashboard.banks') }}</button>
                    <div class="panel-home">
                    <div class="mobile-dashboard-buttons">
                        <a class="btn btn-primary" href="{{route('bank.bank-new')}}">
                            {{ trans('dashboard.add_banks') }} <i class="fa fa-plus" aria-hidden="true"></i>
                        </a>
                    </div>
                    <div class="col-md-4">
                        @if($banks->count()== 0)
                         <div class="card-home" style="box-shadow: 0px 0px 6px 0px red; background: rgba(0,0,0,0.8);">
                                <div id="text-small">
                                    {{ trans('dashboard.add_first_bank') }} <a class="btn btn-primary" href="{{route('bank.bank-new')}}"><i class="fa fa-plus" aria-hidden="true"></i></a>
                                </div>
                                <div class="header">
                                    <h4 style="color:#000;" class="title-dashboard">{{ trans('dashboard.banks') }} </h4>
                                </div>
                                <div style="z-index:-1; position:relative;" class="content table-responsive table-full-width">
                                    <table class="table table-hover table-striped">
                                        <thead>
                                        <th>{{ trans('dashboard.name') }}</th>
                                        <th>{{ trans('dashboard.transactions') }}</th>
                                        </thead>
                                        <tbody>
                                            <tr><td colspan="100">{{ trans('dashboard.no_banks') }}</td></tr>
                                        <tfoot>
                                        <th>{{ trans('dashboard.name') }}</th>
                                        <th>{{ trans('dashboard.transactions') }}</th>
                                        </tfoot>
                                        </tbody>
                                    </table>

                                </div>
                            </div>
                        @else
                         <div class="card-home">

                            <div class="header">
                                <h4 class="title-dashboard">{{ trans('dashboard.banks') }} </h4>
                                <div class="add-new-dashboard">
                                    <a class="btn btn-primary" href="{{route('bank.bank-new')}}"><i class="fa fa-plus" aria-hidden="true"></i></a>
                                </div>
                            </div>
                            <div class="content table-responsive table-full-width">
                                <table class="table table-hover table-striped">
                                    <thead>
                                    <th>{{ trans('dashboard.name') }}</th>
                                    <th>{{ trans('dashboard.transactions') }}</th>
                                    </thead>
                                    <tbody>
                                    @forelse($banks as $bank)
                                        <tr>
                                            <td>{{$bank->name}}</td>
                                            <td><a class="badge" href="{{route('bank.transactions',$bank)}}">
                                                    {{$bank->transactions()->count()}}
                                                </a>
                                            </td>
                                        </tr>
                                    @empty
                                        <tr><td colspan="100">{{ trans('dashboard.no_banks') }}</td></tr>
                                    @endforelse
                                    <tfoot>
                                    <th>{{ trans('dashboard.name') }}</th>
                                    <th>{{ trans('dashboard.transactions') }}</th>
                                    </tfoot>
                                    </tbody>
                                </table>

                            </div>
                        </div>

                        @endif
                    </div>
                    </div>

                        <button class="accordion">{{ trans('dashboard.categories') }}</button>
                    <div class="panel-home">
                    <div class="mobile-dashboard-buttons">
                        <a class="btn btn-primary" href="{{route('cat.category-new')}}">
                            {{ trans('dashboard.add_cat') }} <i class="fa fa-plus" aria-hidden="true"></i>
                        </a>
                    </div>
                    <div class="col-md-4">
                        @if($categorys->count()== 0)
                         <div class="card-home" style="box-shadow: 0px 0px 6px 0px red; background: rgba(0,0,0,0.8);">
                                <div id="text-small">
                                    {{ trans('dashboard.add_first_category') }} <a class="btn btn-primary" href="{{route('cat.category-new')}}"><i class="fa fa-plus" aria-hidden="true"></i></a>
                                </div>
                                <div class="header">
                                    <h4 style="color:#000;" class="title-dashboard">{{ trans('dashboard.categories') }} </h4>
                                </div>
                                <div style="z-index:-1; position:relative;" class="content table-responsive table-full-width">
                                    <table  class="table table-hover table-striped">
                                        <thead>
                                        <th>{{ trans('dashboard.cat_subcat') }}</th>
                                        </thead>
                                        <tbody>
                                            <tr><td colspan="100">{{ trans('dashboard.no_cat') }}</td></tr>
                                        <tfoot>
                                        <th>{{ trans('dashboard.cat_subcat') }}</th>
                                        </tfoot>
                                        </tbody>
                                    </table>

                                </div>
                            </div>
                        @else
                            <div class="card-home">

                            <div class="header">
                                <h4 class="title-dashboard">{{ trans('dashboard.categories') }} </h4>
                                <div class="add-new-dashboard">
                                    <a class="btn btn-primary" href="{{route('cat.category-new')}}"><i class="fa fa-plus" aria-hidden="true"></i></a>
                                </div>
                            </div>
                            <div class="content table-responsive table-full-width">
                                <table  class="table table-hover table-striped">
                                    <thead>
                                    <th>{{ trans('dashboard.cat_subcat') }}</th>
                                    </thead>
                                    <tbody>
                                    @forelse($categorys as $category)
                                        <tr>
                                            <td><b>{{ $category->cat}}</b>

                                                @foreach($category->Filecatsub as $subcategory)
                                                    <br/>-- {{ $subcategory->cat}}
                                                @endforeach
                                            </td>
                                        </tr>
                                    @empty
                                        <tr><td colspan="100">{{ trans('dashboard.no_cat') }}</td></tr>
                                    @endforelse
                                    <tfoot>
                                    <th>{{ trans('dashboard.cat_subcat') }}</th>
                                    </tfoot>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                        @endif
                    </div>
                        </div>

            </div>

@endsection
