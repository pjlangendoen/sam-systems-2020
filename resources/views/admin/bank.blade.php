@extends('layouts.Layoutpages')
@section('title')
    {{ trans('bank.bank') }}
@endsection

@section('content')


    <div class="row">
        <div class="sub-mobile">
            <div class="mobile-dashboard-buttons-sub">
                <a class="btn btn-primary" href="{{route('bank.bank-new')}}">
                    {{ trans('bank.add_bank') }} <i class="fa fa-plus" aria-hidden="true"></i>
                </a>
            </div>
            <input id="fullwidth" class="form-group" type="text" placeholder="{{ trans('bank.search_name') }}" ng-model="search.name" ng-change="currentPage = 0">
            <div class="subpage" ng-if="0" ng-repeat-start="bank in banks | filter:{name: search.name} | startFrom:currentPage*pageSize | limitTo:pageSize"></div>

                <button class="accordion"><b ng-bind="bank.name"></b></button>

                <div class="panel-sub">
                    <div class="card-sub">
                    <table class="table">
                            <thead>
                            <th>{{ trans('bank.name') }}</th>
                            <th>{{ trans('bank.transactions') }}</th>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="smalltekst" ng-bind="bank.name"></td>
                                    <td>
                                        <span ng-if="bank.transactions.length == 0" ng-bind="0" class="badge" ></span>
                                        <a ng-if="bank.transactions.length != 0" ng-bind="bank.transactions.length" class="badge" ng-href="[{bank.urlbanktransaction}]"></a>
                                    </td>
                                    </td>
                                </tr>

                            </tbody>
                        </table>
                        <div id="buttons-sub-category">
                            <a class="btn btn-primary" ng-href="[{bank.urlbankedit}]"><i class="fa fa-pencil-alt" aria-hidden="true"></i></a>
                            <a onclick="return ConfirmDelete()" class="btn btn-danger" ng-href="[{bank.urlbankdelete}]"><i class="fa fa-trash" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            <div ng-if="0" ng-repeat-end></div>
            <div class="pagination">

                <li class="paginate_button previous " id="table_previous">
                    <button ng-disabled="currentPage == 0" ng-click="currentPage=currentPage-1">
                        {{ trans('bank.previous') }}
                    </button> </li>

                <li class="paginate_buttonsub">
                    [{currentPage+1}] / [{numberOfPages()}]
                </li>

                <li class="paginate_button next" id="table_next">
                    <button ng-disabled="currentPage >= getBanks().length/pageSize - 1" ng-click="currentPage=currentPage+1">
                        {{ trans('bank.next') }}
                    </button> </li>

            </div>
        </div>
        <div class="sub-desktop">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">{{ trans('bank.bank') }}</h4>
                                <div class="add-new">
                                    <a class="btn btn-primary" href="{{route('bank.bank-new')}}">
                                        {{ trans('bank.add_bank') }} <i class="fa fa-plus" aria-hidden="true"></i>
                                    </a>
                                </div>
                                <p class="category"> {{ trans('bank.subtitle') }}</p>
                            </div>
                            <div class="content table-responsive table-full-width">
                                <table id="table"  class="table table-hover table-striped" exclude="edit|delete|id|wijzigen|verwijderen">
                                    <thead>
                                        <th>{{ trans('bank.name') }}</th>
                                        <th>{{ trans('bank.transactions') }}</th>
                                        <th>{{ trans('bank.edit') }}</th>
                                        <th>{{ trans('bank.delete') }}</th>
                                    </thead>
                                    <tbody>
                                        @forelse($banks as $bank)
                                     <tr>
                                            <td>{{$bank->name}}</td>
                                            <td><a class="badge" href="{{route('bank.transactions',$bank)}}">
                                               {{$bank->transactions()->count()}}
                                               </a>
                                            </td>
                                            <td><a class="btn btn-primary" href="{{route('bank.bank-edit',$bank)}}"><i class="fa fa-pencil-alt" aria-hidden="true"></i></a></td>
                                            <td><a onclick="return ConfirmDelete()" class="btn btn-danger" href="{{route('bank.bank-delete',$bank)}}"><i class="fa fa-trash" aria-hidden="true"></i></a></td>
                                        </tr>
                                        @empty
                                        <tr><td colspan="100">{{ trans('bank.bank_no') }}</td></tr>
                                        @endforelse
                                       <tfoot>
                                       <th>{{ trans('bank.name') }}</th>
                                       <th>{{ trans('bank.transactions') }}</th>
                                       <th>{{ trans('bank.edit') }}</th>
                                       <th>{{ trans('bank.delete') }}</th>
                                    </tfoot>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
        </div>
    </div>

@endsection
@section('scripts')
<script>
    app.controller('main',['$scope', '$http', '$filter', function($scope, $http, $filter){
        var url = "/admin/banks/list";
            $scope.banks = [];
            $scope.pageSize = 10;
            $scope.currentPage = 0;
            $scope.search = [];

        $http.get(url).then(function(res){
            $scope.banks = res.data;
        });

        $scope.getBanks = function () {
            return $filter('filter')($scope.banks, {name: $scope.search.name});
        }

        $scope.numberOfPages=function(){
            return Math.ceil($scope.getBanks().length/$scope.pageSize);
        }

    }]);



</script>
@endsection


