@extends('layouts.Layoutpages')
@section('title')
    Household Budgeting
@endsection

@section('content')

    <div class="row">
        <div id class="sub-mobile">
            <div class="mobile-dashboard-buttons-sub">
                <a class="btn btn-primary" href="{{route('household.household-new')}}">
                    {{ trans('household.add_household_m') }} <i class="fa fa-plus" aria-hidden="true"></i>
                </a>
                <a class="btn btn-primary" href="{{route('household.household-exportall')}}">
                    {{ trans('household.export') }} <i class="fas fa-file-csv" aria-hidden="true"></i>
                </a>
            </div>
            <input id="fullwidth" class="form-group" type="text" placeholder="{{ trans('household.search_date') }}" ng-model="search.date" ng-change="currentPage = 0">
            <div class="subpage"  ng-repeat="household in households | orderBy: '-created_at'  | filter:{monthyear: search.date,} | startFrom:currentPage*pageSize | limitTo:pageSize">

                <button id="green" class="accordion" ng-if="household.totaloversum > '0'"><b class="capitalize" ng-bind="household.monthyear"></b></button>
                <button id="red" class="accordion" ng-if="household.totaloversum < '0'"><b class="capitalize" ng-bind="household.monthyear"></b></button>
                <button ng-if="household.totaloversum == '0'" class="accordion"><b class="capitalize" ng-bind="household.monthyear"></b></button>

                <div class="panel-sub">


                    <div class="card-sub">
                        <table class="table">
                            <thead>
                            <th>{{ trans('household.income') }}</th>
                            <th>{{ trans('household.spending') }}</th>
                            <th>{{ trans('household.balance') }}</th>
                            </thead>
                            <tbody>
                            <tr>
                                <td ng-bind="' € + '+ (household.totalincomingsum | number: 2)" class="incoming"></td>
                                <td ng-bind="' € - '+ (household.totaloutgoingsum | number: 2)" class="outgoing"></td>

                                <td ng-if="household.totaloversum < '0'" class="outgoing" ng-bind="' € '+ (household.totaloversum | number: 2 )"></td>
                                <td ng-if="household.totaloversum > '0'" ng-bind="' € + '+ (household.totaloversum | number: 2 )" class="incoming"></td>
                                <td ng-if="household.totaloversum == '0'" ng-bind="' € '+ (household.totaloversum | number: 2 )"></td>



                                <div id="buttons-sub">
                                    <div class="buttons-subpage"><a class="btn btn-info" ng-href="[{household.urlhouseholdview}]"><i class="fa fa-file" aria-hidden="true"></i></a></div>
                                    <div class="buttons-subpage"><a onclick="return ConfirmDelete()" class="btn btn-danger" ng-href="[{household.urlhouseholddelete}]"><i class="fa fa-trash" aria-hidden="true"></i></a></div>
                                </div>
                            </tr>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
            <div class="pagination">

                <li class="paginate_button previous " id="table_previous">
                    <button ng-disabled="currentPage == 0" ng-click="currentPage=currentPage-1">
                        {{ trans('household.previous') }}
                    </button> </li>

                <li class="paginate_buttonsub">
                    [{currentPage+1}] / [{numberOfPages()}]
                </li>

                <li class="paginate_button next" id="table_next">
                    <button ng-disabled="currentPage >= getHouseholds().length/pageSize - 1" ng-click="currentPage=currentPage+1">
                        {{ trans('household.next') }}
                    </button> </li>

            </div>
        </div>

        <div class="sub-desktop">
            <div class="col-md-12">
                <div class="card">
                    <div class="header">
                        <h4 class="title"> {{ trans('household.household_budgeting') }}</h4>
                        <div class="add-new">
                            <a class="btn btn-primary" href="{{route('household.household-new')}}">
                                {{ trans('household.add_household') }}<i class="fa fa-plus" aria-hidden="true"></i>
                            </a> <a class="btn btn-primary" style="margin-right:20px;" href="{{route('household.household-exportall')}}">
                                {{ trans('household.exportall') }} <i class="fas fa-file-csv" aria-hidden="true"></i>
                            </a>
                        </div>
                        <p class="category"> {{ trans('household.all_monthly') }}</p>
                    </div>
                    <div class="content table-responsive table-full-width">
                        <table id="table"  class="table table-hover table-striped" exclude="edit|open|delete|id|verwijderen|open huishouden|export huishouden|export household|open household">
                            <thead>
                            <th>{{ trans('household.date') }}</th>
                            <th>{{ trans('household.income') }}</th>
                            <th>{{ trans('household.spending') }}</th>
                            <th>{{ trans('household.balance') }}</th>
                            <th>{{ trans('household.transactions') }}</th>
                            <th>{{ trans('household.open') }}</th>
                            <th>{{ trans('household.export') }}</th>
                            {{--<th>Edit</th>--}}
                            <th>{{ trans('household.delete') }}</th>
                            </thead>
                            <tbody>
                            @forelse($households as $household)
                                <tr>
                                    <td class="capitalize">{{ $household->date->isoFormat('MMMM - Y')}}</td>
                                    <td class="incoming">&euro; + {{$household->totalincomingsum}}</td>
                                    <td class="outgoing">&euro; - {{$household->totaloutgoingsum}}</td>

                                    @if(strpos($household->totaloversum, '-') === 0)
                                        <td class="outgoing">&euro; {{$household->totaloversum}}</td>
                                    @else
                                        <td class="incoming">&euro; + {{$household->totaloversum}}</td>
                                    @endif

                                    <td><a class="badge badge-light" href="{{route('household.transactions',$household)}}">
                                            {{$household->transactions()->count()}}
                                        </a>
                                    </td>
                                    <td><a class="btn btn-info" href="{{route('household.household-view',$household)}}"><i class="fa fa-file" aria-hidden="true"></i></a></td>
                                    <td><a class="btn btn-info" href="{{route('household.household-export',$household)}}"><i class="fas fa-file-csv" aria-hidden="true"></i></a></td>
                                    {{--<td><a class="btn btn-primary" href="{{route('household.household-edit',$household)}}"><i class="fa fa-pencil" aria-hidden="true"></i></a></td>--}}
                                    <td><a onclick="return ConfirmDelete()" class="btn btn-danger" href="{{route('household.household-delete',$household)}}"><i class="fa fa-trash" aria-hidden="true"></i></a></td>
                                </tr>
                            @empty
                                <tr><td colspan="100"> {{ trans('household.households_no') }}</td></tr>
                            @endforelse
                            <tfoot>
                            <th>{{ trans('household.date') }}</th>
                            <th>{{ trans('household.income') }}</th>
                            <th>{{ trans('household.spending') }}</th>
                            <th>{{ trans('household.balance') }}</th>
                            <th>{{ trans('household.transactions') }}</th>
                            <th>{{ trans('household.open') }}</th>
                            <th>{{ trans('household.export') }}</th>
                            {{--<th>Edit</th>--}}
                            <th>{{ trans('household.delete') }}</th>
                            </tfoot>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('scripts')
    <script>
        app.controller('main',['$scope', '$http', '$filter', function($scope, $http, $filter){
            var url = "/admin/households/list";
            $scope.households = [];
            $scope.pageSize = 5;
            $scope.currentPage = 0;
            $scope.search = [];

            $http.get(url).then(function(res){
                $scope.households = res.data;
            });

            $scope.getHouseholds = function () {
                return $filter('filter')($scope.households, {monthyear: $scope.search.date});
            }

            $scope.numberOfPages=function(){
                return Math.ceil($scope.getHouseholds().length/$scope.pageSize);
            }

        }]);



    </script>
@endsection


