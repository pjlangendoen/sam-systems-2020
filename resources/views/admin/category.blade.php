@extends('layouts.Layoutpages')
@section('title')
    {{ trans('category.category_sub') }}
@endsection

@section('content')

                <div class="row">
                    <div id class="sub-mobile">
                        <div class="mobile-dashboard-buttons-sub">
                            <a class="btn btn-primary" href="{{route('cat.category-new')}}">
                                {{ trans('category.add_category') }} <i class="fa fa-plus" aria-hidden="true"></i>
                            </a>
                        </div>
                        <input id="fullwidth" class="form-group" type="text" placeholder="{{ trans('category.search_name') }}" ng-model="search.cat" ng-change="currentPage = 0">

                        <div class="subpage" ng-if="0" ng-repeat-start="categorys in category | customSearch: search.cat | startFrom:currentPage*pageSize | limitTo:pageSize"></div>
                        <button class="accordion"><b ng-bind="categorys.cat"></b></button>
                                    <div class="panel-sub">


                                        <div class="card-sub">
                                            <table class="table">
                                                <thead>
                                                <th>{{ trans('category.category_file') }}</th>
                                                <th>{{ trans('category.category_transaction') }}</th>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td>
                                                        <span ng-if="categorys.file.length == 0" ng-bind="0" class="badge" ></span>
                                                            <a ng-if="categorys.file.length != 0" ng-bind="categorys.file.length" class="badge" ng-href="[{categorys.urlfilecategory}]"></a>
                                                    </td>
                                                    <td>

                                                        <span ng-if="categorys.transaction.length == 0" ng-bind="0" class="badge" ></span>
                                                        <a ng-if="categorys.transaction.length != 0" ng-bind="categorys.transaction.length" class="badge" ng-href="[{categorys.urltransactionfile}]"></a>
                                                        </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                            <div id="buttons-sub-category">
                                                <a class="btn btn-primary" ng-href="[{categorys.urlcategoryedit}]"><i class="fa fa-pencil-alt" aria-hidden="true"></i></a>
                                                <a ng-href="[{categorys.urlcategorydelete}]" onclick="return ConfirmDelete()" class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                            </div>
                                        </div>
                                    </div>



                        <div class="subpage" ng-if="0" ng-repeat-start="subCat in categorys.filecatsub | filter:{cat: search.cat}"></div>
                        <button id="sub" class="accordion"><b ng-bind="'-- '+subCat.cat"></b></button>
                        <div class="panel-sub">
                            <div class="card-sub">
                                <table class="table">
                                    <thead>
                                    <th>{{ trans('category.category_file') }}</th>
                                    <th>{{ trans('category.category_transaction') }}</th>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>

                                            <span ng-if="subCat.file.length == 0" ng-bind="0" class="badge" ></span>
                                            <a ng-if="subCat.file.length != 0" ng-bind="subCat.file.length" class="badge" ng-href="[{subCat.urlfilecategory}]"></a>
                                        </td>
                                        <td>


                                            <span ng-if="subCat.transaction.length == 0" ng-bind="0" class="badge" ></span>
                                            <a ng-if="subCat.transaction.length != 0" ng-bind="subCat.transaction.length" class="badge" ng-href="[{subCat.urltransactionfile}]"></a>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                                <div id="buttons-sub-category">
                                    <a class="btn btn-primary" ng-href="[{subCat.urlcategoryedit}]"><i class="fa fa-pencil-alt" aria-hidden="true"></i></a>
                                    <a ng-href="[{subCat.urlcategorydelete}]" onclick="return ConfirmDelete()" class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>



                        <!--<div ng-if="0" ng-repeat-start="subCat in categorys.filecatsub | filter:{cat: search.cat}"></div>
                            <button class="subcategory">
                                <b class="smalltekst" ng-bind="'-- '+subCat.cat"></b>
                                <div id="buttons-sub-cat">
                                    <a class="btn btn-primary-sub" ng-href="[{subCat.urlcategoryedit}]"><i class="fa fa-pencil-alt" aria-hidden="true"></i></a>
                                    <a ng-href="[{subCat.urlcategorydelete}]" onclick="return ConfirmDelete()" class="btn btn-danger-sub"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                </div>
                            </button>-->
                        <div ng-if="0" ng-repeat-end></div>
                        <div ng-if="0" ng-repeat-end></div>

                        <div class="pagination">

                            <li class="paginate_button previous " id="table_previous">
                                <button ng-disabled="currentPage == 0" ng-click="currentPage=currentPage-1">
                                    {{ trans('category.previous') }}
                                </button> </li>

                            <li class="paginate_buttonsub">
                                [{currentPage+1}] / [{numberOfPages()}]
                            </li>

                            <li class="paginate_button next" id="table_next">
                                <button ng-disabled="currentPage >= getCategory().length/pageSize - 1" ng-click="currentPage=currentPage+1">
                                    {{ trans('category.next') }}
                                </button> </li>

                        </div>
                    </div>
                    <div class="sub-desktop">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">{{ trans('category.overview') }}</h4>
                                <div class="add-new">
                                    <a class="btn btn-primary" href="{{route('cat.category-new')}}">
                                        {{ trans('category.add_category') }} <i class="fa fa-plus" aria-hidden="true"></i>
                                    </a>
                                </div>
                                <p class="category"> {{ trans('category.subtitle') }}</p>
                            </div>
                            <div class="content table-responsive table-full-width">
                                <table id="table"  class="table table-hover table-striped" exclude="edit|delete|verwijderen|wijzigen">
                                    <thead>
                                        <th>{{ trans('category.category_sub') }}</th>
                                        <th>{{ trans('category.category_file') }}</th>
                                        <th>{{ trans('category.category_transaction') }}</th>
                                        <th>{{ trans('category.edit') }}</th>
                                        <th>{{ trans('category.delete') }}</th>
                                    </thead>
                                    <tbody>
                                        @forelse($categorys as $category)
                                     <tr>
                                            <td><b style="font-size:16px;">{{ $category->cat}}</b>

                                                    @foreach($category->Filecatsub as $subcategory)
                                               <br/> -- <b>{{ $subcategory->cat}}</b>
                                                        | {{ trans('category.category_file') }}:
                                                    <a class="badge-small" href="{{route('cat.category-media',$subcategory)}}">
                                                        {{count($subcategory->file)}}
                                                    </a> | {{ trans('category.category_transaction') }}:
                                                    <a class="badge-small" href="{{route('cat.category-transactions',$subcategory)}}">
                                                        {{count($subcategory->transaction)}}
                                                    </a>
                                                    <div class="category-right">
                                                        <a class="btn-primary-sub" href="{{route('cat.category-edit',$subcategory)}}"><i id="mob" class="fa fa-pencil-alt" aria-hidden="true"></i></a>
                                                        <a class="btn-danger-sub" href="{{route('cat.category-delete',$subcategory)}}"><i id="mob" class="fa fa-trash" aria-hidden="true"></i></a>
                                                    </div>
                                                @endforeach
                                            </td>
                                         <td style="vertical-align:top;"><a class="badge" href="{{route('cat.category-media',$category)}}">
                                                 {{count($category->file)}}
                                             </a></td>
                                         <td style="vertical-align:top;"><a class="badge" href="{{route('cat.category-transactions',$category)}}">
                                                 {{count($category->transaction)}}
                                             </a></td>

                                            <td style="vertical-align:top;"><a class="btn btn-primary" href="{{route('cat.category-edit',$category)}}"><i class="fa fa-pencil-alt" aria-hidden="true"></i></a></td>
                                            <td style="vertical-align:top;"><a onclick="return ConfirmDelete()" class="btn btn-danger" href="{{route('cat.category-delete',$category)}}"><i class="fa fa-trash" aria-hidden="true"></i></a></td>
                                        </tr>
                                        @empty
                                        <tr><td colspan="100">{{ trans('category.no_category') }}</td></tr>
                                        @endforelse
                                    <tfoot>
                                    <th>{{ trans('category.category_sub') }}</th>
                                    <th>{{ trans('category.category_file') }}</th>
                                    <th>{{ trans('category.category_transaction') }}</th>
                                    <th>{{ trans('category.edit') }}</th>
                                    <th>{{ trans('category.delete') }}</th>
                                    </tfoot>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                    </div>


                </div>

@endsection
@section('scripts')
    <script>
        app.controller('main',['$scope', '$http', '$filter', function($scope, $http, $filter){
            var url = "/admin/category/list";
            $scope.category = [];
            $scope.pageSize = 10;
            $scope.currentPage = 0;
            $scope.search = [];

            $http.get(url).then(function(res){
                $scope.category = res.data;
            });

            $scope.getCategory = function () {
                return $filter('filter')($scope.category, {cat: $scope.search.cat});
            }

            $scope.numberOfPages=function(){
                return Math.ceil($scope.getCategory().length/$scope.pageSize);
            }

        }]);

        app.filter('customSearch', function() {
            return function(input, search) {
                if(!search) return input;
                return input.filter(function(element, index, array) {
                    var contains = false;
                    for (var i = 0; i < element.filecatsub.length; i++) {
                        if (element.filecatsub[i].cat.match(new RegExp(search, "i"))) {
                            contains = true;
                            break;
                        }
                    }

                    return (!contains ? element.cat.match(new RegExp(search, "i")) : contains);
                });
            };
        });

    </script>
@endsection

