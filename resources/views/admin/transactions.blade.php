@extends('layouts.Layoutpages')
@section('title')
    {{ trans('transaction.transactions_all') }}
@endsection

@section('content')

    <div class="row">
        <div class="sub-mobile">
            <div class="mobile-dashboard-buttons-sub">
                <a class="btn btn-primary" href="{{route('transactions.transaction-new')}}">
                    {{ trans('transaction.add_transaction') }} <i class="fa fa-plus" aria-hidden="true"></i>
                </a>
            </div>

            <input class="form-group" type="text" placeholder="{{ trans('transaction.search_name') }}" ng-model="search.name" ng-change="currentPage = 0">
            <input class="form-group" type="text" placeholder="{{ trans('transaction.search_date') }}" ng-model="search.date" ng-change="currentPage = 0">
            <select ng-options="type for type in ['Incoming', 'Outgoing']" class="form-control" ng-model="search.type" ng-change="currentPage = 0">
                <option value="" disabled selected>{{ trans('transaction.search_inout') }}</option>
            </select>
            <select ng-options="monthly for monthly in ['Yes', 'No']" class="form-control" ng-model="search.monthly" ng-change="currentPage = 0">
                <option value="" disabled selected>{{ trans('transaction.search_monthly') }}</option>
            </select>

            <button id="clear-filter" class="btn btn-secondary" type="button" ng-click="search = {}">{{ trans('transaction.clear_filter') }}<i class="fa fa-times"></i></button>
            <div class="subpage" ng-repeat="transaction in transactions | orderBy: '-created_at' | filter:{name: search.name, monthyear: search.date, type: search.type, monthly: search.monthly} | startFrom:currentPage*pageSize | limitTo:pageSize">

                <button id="green" class="accordion" ng-if="transaction.type == 'Incoming'"><b ng-bind="transaction.name"></b> | <span class="capitalize" ng-bind="transaction.household.monthyear " class="sub-small-text"></span> | <span class="sub-small-text"> {{ trans('transaction.incoming_tab') }}</span></button>
                <button id="red" class="accordion" ng-if="transaction.type == 'Outgoing'"><b ng-bind="transaction.name"></b> | <span class="capitalize" ng-bind="transaction.household.monthyear" class="sub-small-text"></span> |<span class="sub-small-text"> {{ trans('transaction.outgoing_tab') }}</span></button>

                <div class="panel-sub">
                    <div class="card-sub">
                        <table class="table">
                            <thead>
                            <th>{{ trans('transaction.name') }}</th>
                            <th>{{ trans('transaction.price_table') }}</th>
                            <th select>{{ trans('transaction.monthly') }}</th>
                            <th select>{{ trans('transaction.type') }}</th>
                            <th select>{{ trans('transaction.bank') }}</th>
                            <th>{{ trans('transaction.category') }}</th>
                            <th>{{ trans('transaction.date') }}</th>
                            <th>{{ trans('transaction.household') }}</th>
                            <th>{{ trans('transaction.files') }}</th>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="smalltekst" ng-bind="transaction.name"></td>
                                    <td ng-bind="'€ '+ transaction.price"></td>


                                        <td ng-if="transaction.monthly == 'Yes' || transaction.monthly == 'yes'">{{ trans('transaction.monthly_yes') }}</td>
                                        <td ng-if="transaction.monthly == 'No' || transaction.monthly == 'no'">{{ trans('transaction.monthly_no') }}</td>


                                        <td ng-if="transaction.type == 'Incoming' || transaction.type == 'incoming'" class="incoming">{{ trans('transaction.incoming') }} </td>

                                        <td ng-if="transaction.type == 'Outgoing' || transaction.type == 'outgoing'" class="outgoing">{{ trans('transaction.outgoing') }}</td>

                                        <td ng-if="transaction.type == null" ng-bind="transaction.type"></td>


                                        <td ng-if="transaction.bank_id == null" class="empty">{{ trans('transaction.bank_no') }}</td>

                                        <td  ng-if="transaction.bank_id != null" ng-bind="transaction.bank.name"></td>

                                        <td ng-if="transaction.filecat_id == null" class="empty">{{ trans('transaction.category_no') }}</td>

                                        <td ng-if="transaction.filecat_id != null" ng-bind="transaction.filecat.cat"></td>



                                    <td class="capitalize" ng-bind="transaction.daymonthyear"></td>
                                    <td>
                                        <a class="capitalize" ng-if="transaction.household_id != null" ng-bind="transaction.household.monthyear" ng-href="[{transaction.urlhousehold}]"></a>
                                        <span ng-if="transaction.household_id == null"  style="color:red;">{{ trans('transaction.household_no') }}</span>
                                    </td>

                                    <td>
                                        <span ng-if="transaction.files.length == 0" ng-bind="0" class="badge" ></span>
                                        <a ng-if="transaction.files.length != 0" ng-bind="transaction.files.length" class="badge" ng-href="[{transaction.urltransactionsfile}]"></a>
                                    </td>
                                    <div id="buttons-sub">
                                        <div class="buttons-subpage"><a class="btn btn-primary" ng-href="[{transaction.urltransactionedit}]"><i class="fa fa-pencil-alt" aria-hidden="true"></i></a></div>
                                        <div class="buttons-subpage"><a onclick="return ConfirmDelete()" class="btn btn-danger" ng-href="[{transaction.urltransactiondelete}]"><i class="fa fa-trash" aria-hidden="true"></i></a></div>
                                    </div>
                                </tr>
                            </tbody>
                        </table>
<div ng-if="transaction.description != null" class="description">
    <b style="color:#9A9A9A;">{{ trans('transaction.description') }}</b><br/>
    <span ng-bind="transaction.description"></span>
</div>

<div ng-if="transaction.description == null" class="description">
     <b style="color:#9A9A9A;">{{ trans('transaction.description') }}</b><br/>
     <span style="color:red;"> {{ trans('transaction.description_no') }}</span>
</div>
                    </div>
                </div>
            </div>
            <div class="pagination">

                <li class="paginate_button previous " id="table_previous">
                    <button ng-disabled="currentPage == 0" ng-click="currentPage=currentPage-1">
                        {{ trans('transaction.previous') }}
                    </button> </li>

                <li class="paginate_buttonsub">
                    [{currentPage+1}] / [{numberOfPages()}]
                    </li>

                <li class="paginate_button next" id="table_next">
                    <button ng-disabled="currentPage >= getTransactions().length/pageSize - 1" ng-click="currentPage=currentPage+1">
                        {{ trans('transaction.next') }}
                    </button> </li>

            </div>
        </div>

        <div class="sub-desktop">
        <div class="col-md-12">
            <div class="card">
                <div class="header">
                    <h4 class="title">{{ trans('transaction.transactions_all') }}</h4>
                    <div class="add-new">
                        <a class="btn btn-primary" href="{{route('transactions.transaction-new')}}">
                            {{ trans('transaction.add_transaction') }} <i class="fa fa-plus" aria-hidden="true"></i>
                        </a>
                    </div>
                    <p class="category"> {{ trans('transaction.subtitle') }}</p>
                </div>
                <div class="content table-responsive table-full-width">
                    <div class="transaction">
                    <table id="table" class="table table-hover table-striped transaction" exclude="edit|delete|id|files|verwijderen|wijzigen">
                        <thead>
                            <th>{{ trans('transaction.name') }}</th>
                            <th>{{ trans('transaction.description') }}</th>
                            <th>{{ trans('transaction.price_table') }}</th>
                            <th select>{{ trans('transaction.monthly') }}</th>
                            <th select>{{ trans('transaction.type') }}</th>
                            <th select>{{ trans('transaction.bank') }}</th>
                            <th>{{ trans('transaction.date') }}</th>
                            <th>{{ trans('transaction.household') }}</th>
                            <th>{{ trans('transaction.category') }}</th>
                            <th>{{ trans('transaction.files') }}</th>
                            <th>{{ trans('transaction.edit') }}</th>
                            <th>{{ trans('transaction.delete') }}</th>
                        </thead>
                        <tbody>
                            @forelse($transactions as $transaction)
                         <tr>
                                <td class="small-padding">{{ $transaction->name }}</td>
                             @if($transaction->description == null)
                                 <td class="empty">{{ trans('transaction.description_no') }}</td>
                             @else
                                <td>{{ $transaction->description }}</td>
                             @endif
                                <td>€ {{ $transaction->price }}</td>

                             @if($transaction->monthly == 'Yes')
                                 <td>  {{ trans('transaction.monthly_yes') }}</td>
                             @else
                                <td>{{ trans('transaction.monthly_no') }}</td>
                             @endif

                                @if ($transaction->type == 'Incoming')
                                    <td class="incoming">{{ trans('transaction.incoming') }}</td>
                                @elseif ($transaction->type == 'Outgoing')
                                    <td class="outgoing">{{ trans('transaction.outgoing') }}</td>
                                @else
                                    <td>{{ $transaction->type }}</td>
                                @endif

                                @if ($transaction->bank_id == null)
                                <td class="empty">{{ trans('transaction.bank_no') }}</td>
                                @else
                                <td >{{ $transaction->bank->name }}</td>
                                @endif


                                <td class="capitalize">{{ $transaction->datetime->isoformat ('D MMMM, Y')}}</td>

                                 @if ($transaction->household)

                                 <td class="capitalize"><a href="{{route('household.household-view',$transaction->household->id)}}">{{ $transaction->household->date->isoFormat('MMMM - Y') }}</a></td>
                                 @else
                                 <td class="empty">{{ trans('transaction.household_no') }}</td>
                                 @endif

                             @if ($transaction->filecat_id == null)
                                 <td class="empty">{{ trans('transaction.category_no') }}</td>
                             @else
                                 <td><b style="font-size:16px;">{{ $transaction->filecat->cat }}</b>
                                     @foreach($transaction->filecat->filecatsub as $filesub)
                                         <br/>-- {{ $filesub->cat}}</b>
                                     @endforeach
                                 </td>
                             @endif


                                <td><a class="badge" href="{{route('site.transactions',$transaction)}}">
                                   {{count($transaction->files)}}
                                   </a>
                                </td>
                                <td class="small-padding"><a class="btn btn-primary" href="{{route('transaction.transaction-edit',$transaction)}}"><i class="fa fa-pencil-alt" aria-hidden="true"></i></a></td>
                                <td class="small-padding"><a onclick="return ConfirmDelete()" class="btn btn-danger" href="{{route('transaction.transaction-delete',$transaction)}}"><i class="fa fa-trash" aria-hidden="true"></i></a></td>
                            </tr>
                            @empty
                            <tr><td colspan="100">{{ trans('transaction.transactions_no') }}</td></tr>
                            @endforelse

                        </tbody>
                        <tfoot>
                        <th>{{ trans('transaction.name') }}</th>
                        <th>{{ trans('transaction.description') }}</th>
                        <th>{{ trans('transaction.price_table') }}</th>
                        <th select>{{ trans('transaction.monthly') }}</th>
                        <th select>{{ trans('transaction.type') }}</th>
                        <th select>{{ trans('transaction.bank') }}</th>
                        <th>{{ trans('transaction.date') }}</th>
                        <th>{{ trans('transaction.household') }}</th>
                        <th>{{ trans('transaction.category') }}</th>
                        <th>{{ trans('transaction.files') }}</th>
                        <th>{{ trans('transaction.edit') }}</th>
                        <th>{{ trans('transaction.delete') }}</th>
                        </tfoot>
                    </table>

                </div>
            </div>
        </div>
        </div>
    </div>
    </div>
@endsection
@section('scripts')
    <script>
        app.controller('main',['$scope', '$http', '$filter', function($scope, $http, $filter){
            var url = "/admin/transactions/list";
            $scope.transactions = [];
            $scope.pageSize = 10;
            $scope.currentPage = 0;
            $scope.search = [];



             @if (!empty($fileCatId))
                url = url + "?id={{ $fileCatId }}";
            @elseif (!empty($BankId))
                url = url + "?bank_id={{ $BankId }}";
            @elseif (count($transactions)==1)
                url = url + "?transaction_id=" + {{ $transactionid }};
            @elseif (!empty($transactions))
                url = url;
            @endif

                        $http.get(url).then(function(res){
                            $scope.transactions = res.data;
                        });

                        $scope.getTransactions = function () {
                            return $filter('filter')($scope.transactions, {name: $scope.search.name, monthyear: $scope.search.date, type: $scope.search.type, monthly: $scope.search.monthly});
                        }

                        $scope.numberOfPages=function(){
                            return Math.ceil($scope.getTransactions().length/$scope.pageSize);
                        }



                    }]);
    </script>

@endsection
