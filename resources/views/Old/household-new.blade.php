
@extends('layouts.Layoutpages')
@section('title')
Household budgeting new
@endsection

@section('content')



        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">New Household Budgeting</div>
                <div class="panel-body">
                        {{Form::open(['class' => 'form-horizontaal' ])}}
                        

                         <div class="form-group{{ $errors->has('date') ? ' has-error' : '' }}">
                            <label for="name" class="control-label">Date</label>
                                <input id="date" type="date" class="form-control " name="date" value="" required autofocus>
                        </div>

                        <div class="form-group">
                                <button type="submit" class="btn btn-primary">
                                    Save household
                                </button>
                        </div>
                        {{Form::close()}}
                </div>
            </div>
        </div>
   


@endsection


