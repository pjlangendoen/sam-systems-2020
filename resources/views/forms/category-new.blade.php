
@extends('layouts.Layoutpages')
@section('title')
    {{ trans('category.new_category') }}
@endsection

@section('content')



    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">{{ trans('category.new_category') }}</div>
            <div class="panel-body">
                {{Form::open(['class' => 'form-horizontaal', 'onsubmit'=>'return ConfirmNew()' ])}}
                <input type="hidden" name="redirect" value="{{ url()->previous() }}">

            @if(\Illuminate\Support\Facades\Request::input('next-page', false))
                    {{ Form::hidden('next-page', \Illuminate\Support\Facades\Request::input('next-page') ) }}
                @endif


                <div class="form-group{{ $errors->has('cat') ? ' has-error' : '' }}">
                    <label for="name" class="control-label">{{ trans('category.name_req') }}</label>
                    <input id="name" type="name" class="form-control " name="cat" value="{{ old('cat') }}" required autofocus>

                </div>

                <div class="form-group{{ $errors->has('maincategory') ? ' has-error' : '' }}">
                    <label for="maincat" class="control-label">{{ trans('category.main_category') }}</label>
                    <div class='input-select'>
                    <select class="form-control selectpicker" data-live-search="true" data-live-search-placeholder="search" id="maincat" name="parent_id">
                        <option disabled selected>{{ trans('category.select_option') }}</option>
                        @forelse ($filecat as $cat)
                            <option value="{{ $cat->id }}">{{ $cat->cat }}</option>

                        @empty
                            <option disabled selected value="">{{ trans('category.no_category') }}</option>
                        @endforelse
                    </select>
                        <div class="input-group-addon"><i class="fa fa-caret-down" aria-hidden="true"></i></div>
                    </div>
                    @foreach ($errors->get('cat') as $error)
                        <div class="form-control-feedback2">{{$error}}</div>
                    @endforeach
                </div>



                <div class="form-group">
                    <button type="submit" class="btn btn-primary">
                        {{ trans('category.save_category') }}
                    </button>
                </div>
                {{Form::close()}}
            </div>
        </div>
    </div>



@endsection
