
@extends('layouts.Layoutpages')
@section('title')
    {{ trans('media.file_edit') }}
@endsection

@section('content')



    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                {{ trans('media.file_edit') }}
            </div>
            <div class="panel-body">
                {{Form::open(['class' => 'form-horizontaal', 'onsubmit'=>'return ConfirmUpdate()', 'files'=>true ])}}
                @if(isset($transaction))
                    {{ Form::hidden('transaction_id', $transaction->id) }}
                    {{ Form::hidden('status', 'Payed') }}
                    {{ Form::hidden('next-page', route('household.household-view', [$household->id])) }}
                @endif

                <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                    <label for="description" class="control-label">{{ trans('media.name_description') }}</label>
                    <input id="description" type="text"  class="form-control" name="description" value="{{$file->description}}" autofocus></input>
                </div>
                @foreach ($errors->get('description') as $error)
                    <div class="form-control-feedback2">{{$error}}</div>
                @endforeach
                <div class="form-group{{ $errors->has('category') ? ' has-error' : '' }}">
                    <label for="maincat" class="control-label">{{ trans('media.categorys_req') }}</label>
                    <div class="input-select">
                    <select class="form-control selectpicker" data-live-search="true" data-live-search-placeholder="search" id="category" name="filecat_id">
                        <option disabled selected>Select your option..</option>
                        @forelse ($categorys as $category)
                            <option <?=($file->filecat_id == $category->id ? " selected":"")?> value="{{ $category->id }}">{{ $category->cat }}</option>
                            @foreach($category->Filecatsub()->orderBy('cat', 'asc')->get() as $filecatsub)
                                <option <?=($file->filecat_id == $filecatsub->id ? " selected":"")?> value="{{ $filecatsub->id }}">&nbsp;&nbsp;&nbsp;&nbsp; --  {{ $filecatsub->cat }}</option>
                            @endforeach
                        @empty
                            <option value="">{{ trans('media.no_cat') }}</option>
                        @endforelse
                    </select>
                        <div class="input-group-addon"><i class="fa fa-caret-down" aria-hidden="true"></i></div>
                    </div>
                    <span class="secondary">{{ trans('media.make') }} <a href="{{route('cat.category-new')}}" target="_Blank">{{ trans('media.new_cat') }}</a> {{ trans('media.en') }} <a href="javascript:window.location.href=window.location.href">{{ trans('media.refresh') }}</a></span>

                </div>

                @if(isset($transactions))
                    <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                        <label for="status" class="control-label">{{ trans('media.status') }}</label>
                        <div class="input-select">
                        <select class="form-control selectpicker" id="status" name="status">
                            <option value="Open" <?=($file->status=="Open" ? " selected":"")?>>{{ trans('media.open_pay') }}</option>
                            <option value="Payed" <?=($file->status=="Payed" ? " selected":"")?>>{{ trans('media.payed') }}</option>
                        </select>
                            <div class="input-group-addon"><i class="fa fa-caret-down" aria-hidden="true"></i></div>
                        </div>
                    </div>
                @endif

                @if(isset($transactions))
                    <div class="form-group{{ $errors->has('transaction') ? ' has-error' : '' }}">
                        <label for="maincat" class="control-label">{{ trans('media.transaction') }}</label>
                        <div class="input-select">
                        <select class="form-control selectpicker" data-live-search="true" data-live-search-placeholder="search" id="transaction" name="transaction_id">
                            <option disabled selected>{{ trans('media.select_option') }}</option>
                            @forelse ($transactions as $transaction_obj)
                                <option <?=($file->transaction_id == $transaction_obj->id ? " selected":"")?> value="{{ $transaction_obj->id }}">{{ $transaction_obj->name }} | {{$transaction_obj->datetime->format('d-M-Y')}}</option>

                            @empty
                                <option value="">{{ trans('media.transaction_no') }}</option>
                            @endforelse
                        </select>
                        <div class="input-group-addon"><i class="fa fa-caret-down" aria-hidden="true"></i></div>
                    </div>
                    </div>
                @endif

                @if(isset($transactions) and $file->latest_payday == !null)
                    <div class="form-group">
                        <label for="payday" class="control-label">{{ trans('media.payday') }}</label>
                        <div class="input-group date">
                        <input id="latest_payday" type="text" data-value="{{$file->latest_payday->format('Y-m-d')}}" class="form-control datepicker" name="latest_payday" value="{{$file->latest_payday->format('Y-m-d')}}">
                            <div class="input-group-addon"><i class="fa fa-calendar-alt" aria-hidden="true"></i></div>
                        </div>
                    </div>
                @else
                    <div class="form-group">
                        <label for="payday" class="control-label">{{ trans('media.payday') }}</label>
                        <div class="input-group date">
                        <input id="latest_payday" type="text"  placeholder="{{ trans('media.select_date') }}" class="form-control datepicker" name="latest_payday" value="">
                            <div class="input-group-addon"><i class="fa fa-calendar-alt" aria-hidden="true"></i></div>
                        </div>
                    </div>
                @endif

                <div class="form-group {{ $errors->has('date') ? ' has-error' : '' }}">
                    <label for="date" class="control-label">{{ trans('media.date') }} *</label>
                    <div class="input-group date">
                    <input id="datetime" type="text" data-value="{{$file->datetime->format('Y-m-d')}}" class="form-control datepicker" name="datetime" value="{{$file->datetime->format('Y-m-d')}}" required>
                        <div class="input-group-addon"><i class="fa fa-calendar-alt" aria-hidden="true"></i></div>
                    </div>
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-primary" >
                        {{ trans('media.save') }}
                    </button>
                </div>
                {{Form::close()}}
            </div>
        </div>
    </div>



@endsection
