@extends('layouts.Layoutpages')
@section('title')
    {{ trans('user.profile') }}
@endsection

@section('content')
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">{{ trans('user.profile') }}</div>
                <div class="panel-body">
                    {{Form::open(['class' => 'form-horizontal' ])}}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">{{ trans('user.name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{{ Auth::user()->name }}}" required autofocus>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('lastname') ? ' has-error' : '' }}">
                            <label for="lastname" class="col-md-4 control-label">{{ trans('user.lastname') }}</label>

                            <div class="col-md-6">
                                <input id="lastname" type="text" class="form-control" name="lastname" value="{{{ Auth::user()->lastname }}}">
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">{{ trans('user.email') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{{ Auth::user()->email }}}" required>
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ trans('user.update') }}
                                </button>
                            </div>
                        </div>
                     {{Form::close()}}
                </div>
            </div>
        </div>
@endsection