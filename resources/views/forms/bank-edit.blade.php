
@extends('layouts.Layoutpages')
@section('title')
    {{ trans('bank.edit_bank') }}
@endsection

@section('content')



        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">{{ trans('bank.edit_bank') }} | {{ $bank->name }}</div>
                <div class="panel-body">
                        {{Form::open(['class' => 'form-horizontaal', 'onsubmit'=>'return ConfirmUpdate()' ])}}
                    <input type="hidden" name="redirect" value="{{ url()->previous() }}">


                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="control-label">{{ trans('bank.name') }}</label>
                                <input id="name" type="text" class="form-control " name="name" value="{{ $bank->name }}" required autofocus>
                        </div>
                    @foreach ($errors->get('name') as $error)
                        <div class="form-control-feedback2">{{$error}}</div>
                    @endforeach
                        
                    
                        <div class="form-group">
                                <button type="submit" class="btn btn-primary">
                                    {{ trans('bank.update_bank') }}
                                </button>
                        </div>
                        {{Form::close()}}
                </div>
            </div>
        </div>
   


@endsection


