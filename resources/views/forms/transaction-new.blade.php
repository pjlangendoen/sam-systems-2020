@extends('layouts.Layoutpages')
@section('title')
    {{ trans('transaction.transaction_new') }}
@endsection

@section('content')
        <div class="col-md-12">
            <div class="panel panel-default">
                @if(!isset($household))
                <div class="panel-heading">{{ trans('transaction.transaction_new') }}</div>
                @else
                    <div class="panel-heading">{{ trans('transaction.transaction_new') }} -> Household: {{$household->date->isoFormat('MMMM - Y')}}</div>
                @endif
                <div class="panel-body">
                        {{Form::open(['class' => 'form-horizontaal', 'onsubmit'=>'return ConfirmNew()' ])}}
                    <input type="hidden" name="redirect" value="{{ url()->previous() }}">

                        <div class="form-group{{ $errors->has('name') ? ' has-warning' : '' }}">
                            <label for="name" class="control-label">{{ trans('transaction.name_req') }}</label>
                                <input id="name" type="text" class="form-control" name="name" value="" autofocus>
                        </div>
                    @foreach ($errors->get('name') as $error)
                        <div class="form-control-feedback-warning">{{$error}}</div>
                    @endforeach
                        <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                            <label for="description" class="control-label">{{ trans('transaction.description') }}</label>
                            <textarea id="description" class="form-control" name="description" value="" autofocus></textarea>
                        </div>
                        <div class="form-group{{ $errors->has('price') ? ' has-warning' : '' }}">
                            <label for="name" class="control-label">{{ trans('transaction.price') }}</label>
                            <div class='input-group date'>
                            <div class="input-group-addon">{{ trans('transaction.price_icon') }}</div>
                                <input id="price" type="price" placeholder="{{ trans('transaction.price') }}" class="form-control" name="price" value="" autofocus>
                            <div class="input-group-addon"><i class="fa fa-asterisk"></i></div>
                            </div>
                        </div>
                    @foreach ($errors->get('price') as $error)
                        <div class="form-control-feedback-warning">{{$error}}</div>
                    @endforeach

                    <div class="form-group{{ $errors->has('filecat_id') ? ' has-warning' : '' }}">
                        <label for="maincat" class="control-label">{{ trans('transaction.categorys') }}</label>
                        <div class='input-select'>
                        <select class="form-control selectpicker" data-live-search="true" data-live-search-placeholder="{{ trans('transaction.search') }}" id="category" name="filecat_id" >
                            <option disabled selected>{{ trans('transaction.select_option') }}</option>
                            @forelse ($categorys as $category)
                                <option value="{{ $category->id }}">{{ $category->cat }}</option>
                                @foreach($category->Filecatsub()->orderBy('cat', 'asc')->get() as $filecatsub)
                                    <option value="{{ $filecatsub->id }}"> &nbsp;&nbsp;&nbsp;&nbsp; -- {{ $filecatsub->cat }}</option>
                                @endforeach
                            @empty
                                <option value="">{{ trans('transaction.categorys_no') }}</option>
                            @endforelse
                        </select>
                        <div class="input-group-addon"><i class="fa fa-caret-down" aria-hidden="true"></i></div>
                    </div>
                        <span class="secondary">{{ trans('transaction.make') }} <a href="{{route('cat.category-new')}}" target="_Blank">{{ trans('transaction.new_category') }}</a> {{ trans('transaction.and') }} <a href="javascript:window.location.href=window.location.href">{{ trans('transaction.refresh') }}.</a></span>
                    </div>
                    @foreach ($errors->get('filecat_id') as $error)
                        <div class="form-control-feedback-2">{{$error}}</div>
                    @endforeach

                    <div class="form-group{{ $errors->has('bank_id') ? ' has-warning' : '' }}">
                        <label for="bank" class="control-label">{{ trans('transaction.bank_name') }} *</label>
                        <div class='input-select'>
                        <select class="form-control selectpicker" data-live-search="true" data-live-search-placeholder="{{ trans('transaction.search') }}" id="bank" name="bank_id">
                            <option disabled selected>{{ trans('transaction.select_option') }}</option>
                            @forelse($banks as $bank)
                                <option value="{{ $bank->id }}">{{ $bank->name }}</option>
                            @empty
                                <option disabled selected value="">{{ trans('transaction.bank_no') }}</option>
                            @endforelse
                        </select>
                        <div class="input-group-addon"><i class="fa fa-caret-down" aria-hidden="true"></i></div>
                    </div>
                        <span class="secondary">{{ trans('transaction.make') }} <a href="{{route('bank.bank-new')}}" target="_Blank">{{ trans('transaction.new_bank') }}</a> {{ trans('transaction.and') }} <a href="javascript:window.location.href=window.location.href">{{ trans('transaction.refresh') }}.</a></span>

                    </div>
                    @foreach ($errors->get('bank_id') as $error)
                        <div class="form-control-feedback-warning">{{$error}}</div>
                    @endforeach

                        <div class="form-group{{ $errors->has('monthly') ? ' has-error' : '' }}">
                            <label for="monthly" class="control-label">{{ trans('transaction.monthly') }}</label>
                            <div class='input-select'>
                                <select class="form-control selectpicker" id="monthly" name="monthly">
                                    <option value="Yes">{{ trans('transaction.monthly_yes') }}</option>
                                    <option value="No">{{ trans('transaction.monthly_no') }}</option>
                                </select>
                                <div class="input-group-addon"><i class="fa fa-caret-down" aria-hidden="true"></i></div>
                            </div>
                        </div>

                         <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
                            <label for="type" class="control-label">{{ trans('transaction.type') }}</label>
                             <div class='input-select'>
                               <select class="form-control selectpicker" id="type" name="type">
                                    <option value="Incoming">{{ trans('transaction.incoming') }}</option>
                                    <option value="Outgoing">{{ trans('transaction.outgoing') }}</option>
                                  </select>
                                 <div class="input-group-addon"><i class="fa fa-caret-down" aria-hidden="true"></i></div>
                             </div>
                        </div>


                        @if(!isset($household))
                        <div class="form-group{{ $errors->has('household_id') ? ' has-warning' : '' }}">
                            <label for="household" class="control-label">{{ trans('transaction.household_req') }}</label>
                            <div class='input-select'>
                            <select class="form-control selectpicker" data-live-search="true" data-live-search-placeholder="{{ trans('transaction.search') }}" id="household" name="household_id">
                                <option disabled selected>{{ trans('transaction.select_option') }}</option>
                                @forelse($households as $household)
                                    <option class="capitalize" value="{{ $household->id }}">{{ $household->date->isoFormat('MMMM - Y') }}</option>
                                @empty
                                    <option disabled selected>{{ trans('transaction.household_no_found') }}</option>
                                @endforelse
                            </select>
                                <div class="input-group-addon"><i class="fa fa-caret-down" aria-hidden="true"></i></div>
                            </div>

                                @if(!isset($household))
                                <span class="secondary">{{ trans('transaction.make') }} <a href="{{route('household.household-new')}}" target="_Blank">{{ trans('transaction.household') }}</a> {{ trans('transaction.and') }} <a href="javascript:window.location.href=window.location.href">{{ trans('transaction.refresh') }}</a></span>
                                @else
                                @endif


                        </div>
                        @endif
                    @foreach ($errors->get('household_id') as $error)
                        <div class="form-control-feedback-warning">{{$error}}</div>
                    @endforeach

                        <div class="form-group{{ $errors->has('datetime') ? ' has-warning' : '' }}">
                            <label for="datetime" class="control-label">{{ trans('transaction.date_req') }}</label>
                            <div class='input-group date'>
                                <input id="datetime" placeholder="{{ trans('media.select_date') }}" type="text" class="form-control datepicker" name="datetime" value="" autofocus>
                                <div class="input-group-addon"><i class="fa fa-calendar-alt" aria-hidden="true"></i></div>
                            </div>
                        </div>
                    @foreach ($errors->get('datetime') as $error)
                        <div class="form-control-feedback-warning">{{$error}}</div>
                    @endforeach

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">
                                {{ trans('transaction.save_transaction') }}
                            </button>
                        </div>
                        {{Form::close()}}
                </div>
            </div>
        </div>
@endsection