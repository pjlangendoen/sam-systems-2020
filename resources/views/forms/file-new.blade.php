
@extends('layouts.Layoutpages')
@section('title')
    {{ trans('media.add_media') }}
@endsection

@section('content')

    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                @if(isset($transactions))
                    {{ trans('media.add_media') }}
                @else
                    {{ trans('media.add_media_for') }}: <strong>{{ $transaction->name }}</strong>
                @endif
            </div>
            <div class="panel-body">
                {{Form::open(['class' => 'form-horizontaal', 'onsubmit'=>'return ConfirmNew()', 'files'=>true ])}}
                @if(isset($transaction))
                    {{ Form::hidden('transaction_id', $transaction->id) }}
                    {{ Form::hidden('status', 'Payed') }}
                    {{ Form::hidden('next-page', route('household.household-view', [$household->id])) }}
                @endif

                <div class="input-group mb-3{{ $errors->has('file') ? ' has-warning' : '' }}">
                    <div class="input-group-prepend">
                        <span class="input-group-text">Upload</span>
                    </div>
                        <div class="custom-file">
                            <input type="file" name="file" class="form-control-file" id="exampleInputFile" aria-describedby="fileHelp">
                        <label class="custom-file-label" for="exampleInputFile">{{ trans('media.choose_file') }}</label>
                  </div>
                    <span class="input-group-text-browse">{{ trans('media.browse') }}</span>
                </div>
                @foreach ($errors->get('file') as $error)
                    <div class="form-control-feedback-warning">{{$error}}</div>
                @endforeach

                <div class="form-group{{ $errors->has('description') ? ' has-warning' : '' }}">
                    <label for="description" class="control-label">{{ trans('media.name_description') }}</label>
                    <input id="description" class="form-control " name="description" value="" autofocus></input>
                </div>
                @foreach ($errors->get('description') as $error)
                    <div class="form-control-feedback-warning">{{$error}}</div>
                @endforeach

                <div class="form-group {{ $errors->has('filecat_id') ? ' has-warning' : '' }}">
                    <label for="maincat" class="control-label">{{ trans('media.categorys_req') }}</label>
                    <div class='input-select '>
                    <select  class="form-control selectpicker" data-live-search="true" data-live-search-placeholder="{{ trans('media.search') }}" id="category"  name="filecat_id" >
                        <option disabled selected>{{ trans('media.select_option') }}</option>
                        @forelse ($categorys as $category)
                            <option value="{{ $category->id }}">{{ $category->cat }}</option>
                            @foreach($category->Filecatsub()->orderBy('cat', 'asc')->get() as $filecatsub)
                                <option value="{{ $filecatsub->id }}">&nbsp;&nbsp;&nbsp;&nbsp; --  {{ $filecatsub->cat }}</option>
                            @endforeach
                        @empty
                            <option disabled selected value="">{{ trans('media.no_cat') }}</option>
                        @endforelse
                    </select>
                    <div class="input-group-addon"><i class="fa fa-caret-down" aria-hidden="true"></i></div>
                    </div>
                    @foreach ($errors->get('filecat_id') as $error)
                        <div class="form-control-feedback-warning">{{$error}}</div>
                    @endforeach
                    <span class="secondary">{{ trans('media.make') }} <a href="{{route('cat.category-new')}}" target="_Blank">{{ trans('media.new_cat') }}</a> {{ trans('media.en') }} <a href="javascript:window.location.href=window.location.href">{{ trans('media.refresh') }}</a></span>

                </div>

                    <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                        <label for="status" class="control-label">{{ trans('media.status') }}</label>
                        <div class='input-select'>
                        <select class="form-control selectpicker" id="status" name="status">
                            <option value="Open">{{ trans('media.open_pay') }}</option>
                            <option value="Payed">{{ trans('media.payed') }}</option>
                        </select>
                        <div class="input-group-addon"><i class="fa fa-caret-down" aria-hidden="true"></i></div>
                    </div>
                    </div>

                @if(isset($transactions))
                    <div class="form-group{{ $errors->has('transaction') ? ' has-error' : '' }}">
                        <label for="maincat" class="control-label">{{ trans('media.transaction') }}</label>
                        <div class='input-select'>
                        <select class="form-control selectpicker" data-live-search="true" data-live-search-placeholder="{{ trans('media.search') }}" id="transaction" name="transaction_id" data-live-search="true">
                            <option disabled selected>{{ trans('media.select_option') }}</option>
                            @forelse ($transactions as $transaction_obj)
                                <option value="{{ $transaction_obj->id }}">{{ $transaction_obj->name }} | {{$transaction_obj->datetime->format('M-Y')}}</option>
                            @empty
                                <option value="">{{ trans('media.transaction_no') }}</option>
                            @endforelse
                        </select>
                        <div class="input-group-addon"><i class="fa fa-caret-down" aria-hidden="true"></i></div>
                        </div>
                    </div>
                @endif

                    <div class="form-group">
                        <label for="payday" class="control-label">{{ trans('media.payday') }}</label>
                        <div class='input-group date'>
                            <input id="latest_payday" type="text" placeholder="{{ trans('media.select_date') }}" class="form-control datepicker" name="latest_payday" value="" >
                            <div class="input-group-addon"><i class="fa fa-calendar-alt" aria-hidden="true"></i></div>
                        </div>

                    </div>

                <div class="form-group{{ $errors->has('date') ? ' has-error' : '' }}">
                    <label for="date" class="control-label">{{ trans('media.date_req') }}</label>
                    <div  class='input-group date'>
                        <input type="text" id="datetime" placeholder="{{date('d F, Y')}}" class="form-control" name="datetime" required readonly>
                        <div class="input-group-addon"><i class="fa fa-calendar-alt" aria-hidden="true"></i></div>

                    </div>
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-primary">
                        {{ trans('media.add_media') }}
                    </button>

                    @if(isset($transaction))
                        <button class="btn btn-secondary" onclick="location.href='{{ route('transaction.transaction-edit', $transaction) }}'">
                            {{ trans('media.skip') }}
                        </button>
                    @endif
                </div>

                {{Form::close()}}
            </div>
        </div>
    </div>

    <script>
        document.querySelector("#datetime").valueAsDate = new Date();
    </script>


@endsection
