
@extends('layouts.Layoutpages')
@section('title')
    {{ trans('category.edit_category') }}
@endsection

@section('content')



        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading"> {{ trans('category.edit_category') }} | {{$filecat->cat}}</div>
                <div class="panel-body">
                    {{Form::open(['class' => 'form-horizontaal'])}}
                    <input type="hidden" name="redirect" value="{{ url()->previous() }}">


                        <div class="form-group{{ $errors->has('cat') ? ' has-error' : '' }}">
                            <label for="name" class="control-label"> {{ trans('category.name_req') }}</label>
                            <input id="name" type="name" class="form-control " name="cat" value="{{ $filecat->cat }}" required autofocus>

                        </div>
                    @foreach ($errors->get('cat') as $error)
                        <div class="form-control-feedback2">{{$error}}</div>
                    @endforeach

                        @if($filecat->parent_id == !null)
                        <div class="form-group{{ $errors->has('maincategory') ? ' has-error' : '' }}">
                            <label for="maincat" class="control-label"> {{ trans('category.main_category') }}</label>
                            <div class='input-select'>
                                {{ Form::select('parent_id', $categories, $filecat->parent_id, ['class' => 'form-control selectpicker', 'data-live-search' =>'true', 'data-live-search-placeholder' => 'search', 'id' => 'maincat']) }}
                                <div class="input-group-addon"><i class="fa fa-caret-down" aria-hidden="true"></i></div>
                            </div>
                                @foreach ($errors->get('cat') as $error)
                            <div class="form-control-feedback2">{{$error}}</div>
                                @endforeach
                        </div>

                        @else
                        {{ trans('category.no_other_category') }}
                        @endif
                        <br/><br/>
                        <div class="form-group">
                                <button type="submit" class="btn btn-primary">
                                    {{ trans('category.update_category') }}
                                </button>
                        </div>
                        {{Form::close()}}
                </div>
            </div>
        </div>
   


@endsection
