
@extends('layouts.Layoutpages')
@section('title')
    {{ trans('transaction.edit_transaction') }}
@endsection

@section('content')




        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">{{ trans('transaction.edit_transaction') }} | {{$transaction->name}}</div>
                <div class="panel-body">
                        {{Form::open(['class' => 'form-horizontaal', 'onsubmit'=>'return ConfirmUpdate()'])}}
                    <input type="hidden" name="redirect" value="{{ url()->previous() }}">

                        <div class="form-group{{ $errors->has('name') ? ' has-warning' : '' }}">
                            <label for="name" class="control-label">{{ trans('transaction.name_req') }}</label>
                                <input id="name" type="text" class="form-control" name="name" value="{{$transaction->name}}" autofocus>
                        </div>
                    @foreach ($errors->get('name') as $error)
                        <div class="form-control-feedback-warning">{{$error}}</div>
                    @endforeach
                        <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                            <label for="description" class="control-label">{{ trans('transaction.description') }}</label>
                            <input id="description" type="text" class="form-control" name="description" value="{{$transaction->description}}" autofocus>
                        </div>
                        <div class="form-group{{ $errors->has('price') ? ' has-warning' : '' }}">
                            <label for="name" class="control-label">{{ trans('transaction.price') }}</label>
                            <div class='input-group date'>
                            <div class="input-group-addon">{{ trans('transaction.price_icon') }}</div>
                                <input id="price" type="text" class="form-control" placeholder="{{ trans('transaction.price') }}" name="price" value="{{$transaction->price}}" autofocus>
                                <div class="input-group-addon"><i class="fa fa-asterisk"></i></div>
                            </div>
                         </div>
                    @foreach ($errors->get('price') as $error)
                        <div class="form-control-feedback-warning">{{$error}}</div>
                    @endforeach
                    <div class="form-group{{ $errors->has('category') ? ' has-error' : '' }}">
                        <label for="maincat" class="control-label">{{ trans('transaction.categorys') }}</label>
                        <div class='input-select'>
                        <select class="form-control selectpicker" data-live-search="true" data-live-search-placeholder="search" id="category" name="filecat_id">
                            <option disabled selected>{{ trans('transaction.select_option') }}</option>
                            @forelse ($categorys as $category)
                                <option <?=($transaction->filecat_id == $category->id ? " selected":"")?> value="{{ $category->id }}">{{ $category->cat }}</option>
                                @foreach($category->Filecatsub()->orderBy('cat', 'asc')->get() as $filecatsub)
                                    <option <?=($transaction->filecat_id == $filecatsub->id ? " selected":"")?> value="{{ $filecatsub->id }}"> &nbsp;&nbsp;&nbsp;&nbsp; -- {{ $filecatsub->cat }}</option>
                                @endforeach
                            @empty
                                <option value="">{{ trans('transaction.categorys_no') }}</option>
                            @endforelse
                        </select>
                            <div class="input-group-addon"><i class="fa fa-caret-down" aria-hidden="true"></i></div>
                        </div>
                        <span class="secondary">{{ trans('transaction.make') }} <a href="{{route('cat.category-new')}}" target="_Blank">{{ trans('transaction.new_category') }}</a> {{ trans('transaction.and') }} <a href="javascript:window.location.href=window.location.href">{{ trans('transaction.refresh') }}.</a></span>

                        </div>

                    <div class="form-group{{ $errors->has('bank_id') ? ' has-error' : '' }}">
                        <label for="bank" class="control-label">{{ trans('transaction.bank_name') }} *</label>
                        <div class='input-select'>
                        <select class="form-control selectpicker" data-live-search="true" data-live-search-placeholder="search" id="bank" name="bank_id">
                            <option disabled selected>{{ trans('transaction.select_option') }}</option>
                            @forelse($banks as $bank)
                                <option <?=($transaction->bank != null && $bank->id==$transaction->bank->id ? " selected":"")?> value="{{ $bank->id }}">{{ $bank->name }}</option>
                            @empty
                                <option value="">{{ trans('transaction.banks_no') }}</option>
                            @endforelse
                        </select>
                            <div class="input-group-addon"><i class="fa fa-caret-down" aria-hidden="true"></i></div></div>
                        <span class="secondary">{{ trans('transaction.make') }} <a href="{{route('bank.bank-new')}}" target="_Blank">{{ trans('transaction.new_bank') }}</a> {{ trans('transaction.and') }} <a href="javascript:window.location.href=window.location.href">{{ trans('transaction.refresh') }}.</a></span>

                    </div>



                        <div class="form-group{{ $errors->has('monthly') ? ' has-error' : '' }}">
                            <label for="monthly" class="control-label">{{ trans('transaction.monthly') }}</label>
                            <div class='input-select'>
                                <select class="form-control selectpicker" id="monthly" name="monthly">
                                     <option value="Yes" <?=($transaction->monthly=="Yes" ? " selected":"")?>>{{ trans('transaction.monthly_yes') }}</option>
                                    <option value="No" <?=($transaction->monthly=="No" ? " selected":"")?>>{{ trans('transaction.monthly_no') }}</option>
                                </select>
                                <div class="input-group-addon"><i class="fa fa-caret-down" aria-hidden="true"></i></div>
                            </div></div>

                         <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}">
                            <label for="type" class="control-label">{{ trans('transaction.type') }}</label>
                             <div class='input-select'>
                               <select class="form-control selectpicker" id="type" name="type">
                                    <option value="Incoming" <?=($transaction->type=="Incoming" ? " selected":"")?>>{{ trans('transaction.incoming') }}</option>
                                    <option value ="Outgoing" <?=($transaction->type=="Outgoing" ? " selected":"")?>>{{ trans('transaction.outgoing') }}</option>
                                  </select>
                                 <div class="input-group-addon"><i class="fa fa-caret-down" aria-hidden="true"></i></div>
                             </div></div>



                        <div class="form-group{{ $errors->has('household_id') ? ' has-error' : '' }}">
                            <label for="household" class="control-label">{{ trans('transaction.household_req') }}</label>
                            <div class='input-select'>
                                <select class="form-control selectpicker" data-live-search="true" data-live-search-placeholder="search" id="household" name="household_id">
                                    <option disabled selected>{{ trans('transaction.select_option') }}</option>

                                @forelse($households as $household)
                                    <option class="capitalize" <?=($transaction->household != null && $household->id==$transaction->household->id ? " selected":"")?> value="{{ $household->id }}">{{ $household->date->isoFormat('MMMM - Y')}} </option>

                                @empty
                                        <option disabled selected>{{ trans('transaction.household_no_found') }}</option>
                                @endforelse
                                </select>
                                <div class="input-group-addon"><i class="fa fa-caret-down" aria-hidden="true"></i></div>
                            </div>
                            @if(!isset($household))
                                <span class="secondary">make <a href="{{route('household.household-new')}}" target="_Blank">household</a> and <a href="javascript:window.location.href=window.location.href">refresh this page.</a></span>
                            @else
                            @endif
                        </div>

                        <div class="form-group{{ $errors->has('datetime') ? ' has-error' : '' }}" >
                            <label for="datetime" class="control-label">{{ trans('transaction.date_req') }}</label>
                            <div class='input-group date'>
                                <input type="text" id="datetime" data-value="{{ $transaction->datetime->format('Y-m-d') }}"  class="form-control datepicker"  name="datetime" value="{{ $transaction->datetime->format('Y-m-d') }}" autofocus>
                                <div class="input-group-addon"><i class="fa fa-calendar-alt" aria-hidden="true"></i></div>
                            </div>
                        </div>





                        <div class="form-group">
                                <button type="submit" class="btn btn-primary">
                                    {{ trans('transaction.update_transaction') }}
                                </button>

                                <a href="{{route('transaction.transaction-edit.upload', $transaction->id )}}" class="btn btn-secondary">
                                    {{ trans('transaction.upload_file') }}
                                </a>

                        </div>

                        {{Form::close()}}
                </div>
            </div>
        </div>


@endsection



