@extends('layouts.main')

@section('maincontent')

    <div class="row">
        <div class="panel-heading-login">SAM Systems |  <a href="/lang/en"><span style="font-size:16px; float:none !important;" class="flag-icon flag-icon-us"></span></a> <a href="/lang/nl"><span style="font-size:16px; float:none !important;" class="flag-icon flag-icon-nl"></span></a></div>

        <div class="col-md-4 col-md-offset-4">

            <div class="panel panel-default2">

<div class="panel-body2">

                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">{{ trans('login.email') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">{{ trans('login.password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                    <label>
                                        <input type="checkbox" name="remember"> {{ trans('login.remember') }}
                                    </label>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Login
                                </button>

                                <a class="btn btn-link" href="{{ url('/password/reset') }}">
                                    {{ trans('login.forget') }}
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
    <footer style="text-align:center; margin-top:10px;"> &copy; SAMSYSTEMS  <?php echo date('Y'); ?> </footer>
@endsection
