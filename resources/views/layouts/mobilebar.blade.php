<div class="menu-mobile">
    <div class="navbar navbar-default">
        <a class="navbar-brand" href="{{ route('site.dashboard') }}">
            <i class="pe-7s-graph"></i>
            <p>Home</p>
        </a>
        <a class="navbar-brand" href="{{ route('site.media') }}">
            <i class="pe-7s-folder"></i>
            <p>Media</p>
        </a>
        <a class="navbar-brand" href="{{ route('household.household') }}">
            <i class="pe-7s-cash"></i>
            <p>{{ trans('mobilebar.household') }}</p>
        </a>
        <a class="navbar-brand" href="{{ route('transaction.transactions')}}">
            <i class="fa fa-exchange-alt"></i>
            <p>{{ trans('mobilebar.transactions') }}</p>
        </a>
        <a class="navbar-brand" href="{{ route('cat.category')}}">
            <i class="fa fa-cubes"></i>
            <p>{{ trans('mobilebar.categories') }}</p>
        </a>
        <a class="navbar-brand" href="{{ route('bank.banks')}}">
            <i class="pe-7s-piggy"></i>
            <p>{{ trans('mobilebar.banks') }}</p>
        </a>
        <a style="border-right:none;" class="navbar-brand" href="{{route('auth.logout')}}">
            <i class="fa fa-sign-out-alt"></i>
            <p>{{ trans('mobilebar.log_out') }}</p>
        </a>
        {{--<a class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">--}}
            {{--<i class="fa fa-bars"></i>--}}
            {{--<p>More info</p>--}}
        {{--</a>--}}

    </div>
</div>
