
    <!--

        Tip 1: you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple"
        Tip 2: you can also add an image using data-image tag

    -->

    {{--<div class="menu-mobile">--}}
        {{--<div class="sidebar" data-color="blue" data-image="/assets/img/sidebar-5.jpg">--}}

            {{--<div class="sidebar-wrapper">--}}
                {{--<div class="logo">--}}


                    {{--<a href="{{ route('site.dashboard') }}" class="simple-text">--}}
                        {{--SAM Systems--}}
                    {{--</a>--}}
                {{--</div>--}}

                {{--<ul class="nav">--}}
                    {{--<li class="{{ Route::currentRouteNamed('site.dashboard') ? 'active' : '' }}">--}}
                        {{--<a href="{{ route('site.dashboard') }}">--}}
                            {{--<i class="pe-7s-graph"></i>--}}
                            {{--<p>Dashboard</p>--}}
                        {{--</a>--}}
                    {{--</li>--}}

                    {{--<li class="{{ Route::currentRouteNamed('site.media') ? 'active' : '' }}">--}}
                        {{--<a href="{{ route('site.media') }}" class="dropdown-toggle" >--}}
                            {{--<i class="pe-7s-folder"></i>--}}
                            {{--<p>MultiMedia</p>--}}
                        {{--</a>--}}
                        {{--<ul class="dropdown-menu">--}}
                        {{--<li><a href="{{ route('site.media') }}">Media</a></li>--}}
                        {{--<li><a href="{{ route('cat.category') }}">Category's</a></li>--}}
                        {{--</ul>--}}
                    {{--</li>--}}

                    {{--<li class="{{ Route::currentRouteNamed('household.household') ? 'active' : '' }}">--}}
                        {{--<a href="{{ route('household.household') }}" class="dropdown-toggle">--}}
                            {{--<i class="pe-7s-cash"></i>--}}
                            {{--<p>Household budgeting</p>--}}
                        {{--</a>--}}
                        {{--<ul class="dropdown-menu">--}}
                        {{--<li><a href="{{ route('household.household') }}">Households</a></li>--}}
                        {{--<li><a href="{{ route('transaction.transactions') }}">Transactions</a></li>--}}
                        {{--<li><a href="{{ route('bank.banks') }}">Banks</a></li>--}}
                        {{--</ul>--}}
                    {{--</li>--}}
                    {{--<li class="{{ Route::currentRouteNamed('transaction.transaction') ? 'active' : '' }}">--}}
                        {{--<a href="{{ route('transaction.transactions')}}">--}}
                            {{--<i class="fa fa-exchange-alt"></i>--}}
                            {{--<p>Transactions</p>--}}
                        {{--</a>--}}
                    {{--</li>--}}
                    {{--<li class="{{ Route::currentRouteNamed('cat.category') ? 'active' : '' }}">--}}
                        {{--<a href="{{ route('cat.category')}}">--}}
                            {{--<i class="pe-7s-user"></i>--}}
                            {{--<p>Category's</p>--}}
                        {{--</a>--}}
                    {{--</li>--}}
                    {{--<li class="{{ Route::currentRouteNamed('bank.banks') ? 'active' : '' }}">--}}
                        {{--<a href="{{ route('bank.banks')}}">--}}
                            {{--<i class="pe-7s-piggy"></i>--}}
                            {{--<p>Banks</p>--}}
                        {{--</a>--}}
                    {{--</li>--}}
                    {{--<li class="{{ Route::currentRouteNamed('site.user') ? 'active' : '' }}">--}}
                        {{--<a href="{{ route('site.user', Auth::user()->id)}}">--}}
                            {{--<i class="pe-7s-user"></i>--}}
                            {{--<p>User Profile</p>--}}
                        {{--</a>--}}
                    {{--</li>--}}
                {{--</ul>--}}

            {{--</div>--}}




        {{--</div>--}}
    {{--</div>--}}

    <div class="menu-desktop">
        <div class="sidebar" data-color="blue" data-image="/assets/img/sidebar-5.jpg">

    	<div class="sidebar-wrapper">
            <div class="logo">


                <a href="{{ route('site.dashboard') }}" class="simple-text">
                    SAM Systems
                </a>
            </div>

            <ul class="nav">
               <li class="{{ Route::currentRouteNamed('site.dashboard') ? 'active' : '' }}">
                    <a href="{{ route('site.dashboard') }}">
                        <i class="pe-7s-graph"></i>
                        <p>Dashboard</p>
                    </a>
                </li>

                 <li class="{{ Route::currentRouteNamed('site.media') ? 'active' : '' }}">
                    <a href="{{ route('site.media') }}" >
                        <i class="pe-7s-folder"></i>
                        <p>MultiMedia</p>
                    </a>
                     {{--<ul class="dropdown-menu">--}}
                                {{--<li><a href="{{ route('site.media') }}">Media</a></li>--}}
                                {{--<li><a href="{{ route('cat.category') }}">Category's</a></li>--}}
                              {{--</ul>--}}
                </li>
                
                <li class="{{ Route::currentRouteNamed('household.household') ? 'active' : '' }}">
                    <a href="{{ route('household.household') }}">
                        <i class="pe-7s-cash"></i>
                        <p>{{ trans('sidebar.household_budgeting') }}</p>
                    </a>
                    {{--<ul class="dropdown-menu">--}}
                                {{--<li><a href="{{ route('household.household') }}">Households</a></li>--}}
                                {{--<li><a href="{{ route('transaction.transactions') }}">Transactions</a></li>--}}
                                {{--<li><a href="{{ route('bank.banks') }}">Banks</a></li>--}}
                              {{--</ul>--}}
                </li>
                <li class="{{ Route::currentRouteNamed('transaction.transaction') ? 'active' : '' }}">
                    <a href="{{ route('transaction.transactions')}}">
                        <i class="fa fa-exchange-alt"></i>
                        <p>{{ trans('sidebar.transactions') }}</p>
                    </a>
                </li>
                <li class="{{ Route::currentRouteNamed('cat.category') ? 'active' : '' }}">
                    <a href="{{ route('cat.category')}}">
                        <i class="fa fa-cubes"></i>
                        <p>{{ trans('sidebar.categories') }}</p>
                    </a>
                </li>
                <li class="{{ Route::currentRouteNamed('bank.banks') ? 'active' : '' }}">
                    <a href="{{ route('bank.banks')}}">
                        <i class="pe-7s-piggy"></i>
                        <p>{{ trans('sidebar.banks') }}</p>
                    </a>
                </li>
                <li class="{{ Route::currentRouteNamed('site.user') ? 'active' : '' }}">
                    <a href="{{ route('site.user', Auth::user()->id)}}">
                        <i class="pe-7s-user"></i>
                        <p>{{ trans('sidebar.user_profile') }}</p>
                    </a>
                </li>
<div style="bottom:0; position:absolute; width:100%;">
                <p style="font-size:16px; text-align:center; margin:0;">{{ trans('sidebar.taal') }}</p>
               <div style="text-align:Center;"><a href="/lang/en"><span class="flag-icon flag-icon-us"></span></a> <a href="/lang/nl"><span class="flag-icon flag-icon-nl"></span></a></div>
            </div>
            </ul>

    	</div>




    </div>
    </div>
