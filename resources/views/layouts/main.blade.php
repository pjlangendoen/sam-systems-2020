<!doctype html>
<html lang="nl" ng-app="sam">
<head>
	<meta charset="utf-8" />
	<link rel="icon" type="image/png" href="/assets/img/favicon.ico">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="apple-mobile-web-app-title" content="Sam Systems">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">

	<title>@yield('title')</title>



    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <!-- datepicker CSS     -->
    <link href="/assets/css/default.css" rel="stylesheet" />
    <link href="/assets/css/default.date.css" rel="stylesheet" />

    <!-- Custom CSS-->
    <link href="/assets/css/custom.css" rel="stylesheet" />



    <!-- Custom CSS-->
    <link href="/assets/css/responsive.css" rel="stylesheet" />

    <!-- Bootstrap core CSS-->
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="/assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Light Bootstrap Table core CSS    -->
    <link href="/assets/css/light-bootstrap-dashboard.css?v=1.4.0" rel="stylesheet"/>


    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="/assets/css/demo.css" rel="stylesheet" />


    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="/assets/css/pe-icon-7-stroke.css" rel="stylesheet" />
    <link href="/assets/css/flag-icon.css" rel="stylesheet" />



    <!-- Datatables -->
    <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet" />

    <!-- Selectpicker-->
    <link rel="stylesheet" href="/assets/css/bootstrap-select.css" />
    <link rel="stylesheet" href="/assets/css/bootstrap-new.css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.1/css/all.min.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">


</head>
<body ng-controller="main">

@yield('maincontent')
</body>
</html>

<!--   Core JS Files-->
    <script src="/assets/js/jquery.3.2.1.min.js" type="text/javascript"></script>
    <script src="/assets/js/bootstrap.min.js" type="text/javascript"></script>


<script src="/assets/js/picker.js"></script>
<script src="/assets/js/picker.date.js"></script>
<script src="/assets/js/legacy.js"></script>

    <!--  Charts Plugin -->
    <script src="/assets/js/chartist.min.js"></script>

    <!--  Notifications Plugin    -->
    <script src="/assets/js/bootstrap-notify.js"></script>


    <!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
    <script src="/assets/js/light-bootstrap-dashboard.js?v=1.4.0"></script>

    <!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
    <script src="/assets/js/demo.js"></script>

    {{--<!--  Filter tables -->--}}

     <script src="/assets/js/datatables.js"></script>

     <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.6.5/angular.min.js"></script>

    <!--  custom Js -->
    <script src="/assets/js/custom.js"></script>

    <!--  angular Js -->
    <script src="/assets/js/angular.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>

    <!--  confirmation Js -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-confirmation/1.0.5/bootstrap-confirmation.min.js"></script>

    <!-- Selectpicker -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.1/js/bootstrap-select.js"></script>




@yield('scripts')


@if (Session::has('flash_message'))
    <script type="text/javascript">
        $(document).ready(function(){
            demo.initChartist();
            $.notify({
                message: "{!!   Session::get('flash_message') !!}"
            },{
                type: 'info',
                timer: 2000
            });


        });

        {{ Session::forget('flash_message') }}
    </script>
@endif


<script>
    @foreach( errors() as $error)
    $.notify({
        message: "{!!   $error !!}"
    }, {
        icon: 'glyphicon glyphicon-warning-sign',
        type: 'danger',
        timer: 5000,

    });

    @endforeach

</script>

{{ Session::forget('error') }}

<script>
    function ConfirmNew()
    {
        var x = confirm("{{ trans('flashmessage.confirmnew') }}");
        if (x)
            return true;
        else
            return false;
    }


    function ConfirmUpdate()
    {
        var x = confirm("{{ trans('flashmessage.confirmupdate') }}");
        if (x)
            return true;
        else
            return false;
    }

    function ConfirmDelete()
    {
        var x = confirm("{{ trans('flashmessage.confirmdelete') }}");
        if (x)
            return true;
        else
            return false;
    }
    $(document).ready(function(){
        // iOS web app full screen hacks.
        if(window.navigator.standalone == true) {
            // make all link remain in web app mode.
            $('a').click(function() {
                window.location = $(this).attr('href');
                return false;
            });
        }
    });
</script>

<script>
    $('#exampleInputFile').on('change',function(){
        //get the file name
        var fileName = $(this).val().replace('C:\\fakepath\\', " ");
        //replace the "Choose a file" label
        $(this).next('.custom-file-label').html(fileName);
    })
</script>
<script>
window.translations = {
search: '{{ trans ('media.search') }}',
}
</script>
<script>
    (function ($, DataTable) {

        // Datatable global configuration
        $.extend(true, DataTable.defaults, {
            language: {
                "sProcessing": "{{trans('datatables.busy')}}",
                "sLengthMenu": "{{trans('datatables.laat')}} _MENU_ {{trans('datatables.show_results')}}",
                "sZeroRecords": "{{trans('datatables.no_data')}}",
                "sInfo": "{{trans('datatables.results')}} _START_ {{trans('datatables.to')}} _END_ {{trans('datatables.of')}} _TOTAL_ weergegeven",
                "sInfoEmpty": "{{trans('datatables.empty')}}",
                "sInfoFiltered": " (gefilterd uit _MAX_ regels)",
                "sInfoPostFix": "",
                "sSearch": "{{trans('datatables.search')}}:",
                "sEmptyTable": "{{trans('datatables.empty')}}",
                "sInfoThousands": ",",
                "sLoadingRecords": "Een moment geduld aub - bezig met laden...",
                "oPaginate": {
                    "sFirst": "{{trans('datatables.first')}}",
                    "sLast": "{{trans('datatables.last')}}",
                    "sNext": "{{trans('datatables.next')}}",
                    "sPrevious": "{{trans('datatables.previous')}}"
                },
            }
        });

    })(jQuery, jQuery.fn.dataTable);

</script>

<!--<script>
    $('#myModal').on('shown.bs.modal', function () {
        $('#myInput').trigger('focus')
    })
</script>-->
    <script>
        $(document).ready(function(){
            $("#modal-home").modal('show');
        });

    </script>

<script>
    $('.datepicker').pickadate({
// Strings and translations
        monthsFull: ["{{trans('datepicker.jan')}}", "{{trans('datepicker.feb')}}", "{{trans('datepicker.mar')}}", 'April', "{{trans('datepicker.mei')}}", "{{trans('datepicker.juni')}}", "{{trans('datepicker.juli')}}", "{{trans('datepicker.aug')}}", "{{trans('datepicker.sep')}}", "{{trans('datepicker.okt')}}",
            'November', 'December'],
        monthsShort: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
        weekdaysFull: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
        weekdaysShort: ["{{trans('datepicker.sun')}}", "{{trans('datepicker.mon')}}", "{{trans('datepicker.tue')}}", "{{trans('datepicker.wed')}}", "{{trans('datepicker.thu')}}", "{{trans('datepicker.fri')}}", "{{trans('datepicker.sat')}}"],
        showMonthsShort: undefined,
        showWeekdaysFull: undefined,

// Buttons
        today: "{{trans('datepicker.today')}}",
        clear: "{{trans('datepicker.clear')}}",
        close: "{{trans('datepicker.close')}}",

// Accessibility labels
        labelMonthNext: 'Next month',
        labelMonthPrev: 'Previous month',
        labelMonthSelect: 'Select a month',
        labelYearSelect: 'Select a year',

// Formats
      //  format: 'yyyy-mm-dd',


// Formats
        format: 'd mmmm, yyyy',
        formatSubmit: 'yyyy-mm-dd',
        hiddenSuffix: '',


// Editable input
        editable: undefined,

// Dropdown selectors
        selectYears: undefined,
        selectMonths: undefined,

// First day of the week
        firstDay: undefined,

// Date limits
        min: undefined,
        max: undefined,

// Disable dates
        disable: undefined,

// Root picker container
        container: undefined,

// Hidden input container
        containerHidden: undefined,

// Close on a user action
        closeOnSelect: true,
        closeOnClear: true,

// Events
        onStart: undefined,
        onRender: undefined,
        onOpen: undefined,
        onClose: undefined,
        onSet: undefined,
        onStop: undefined,

// Classes
        klass: {

// The element states
            input: 'picker__input',
            active: 'picker__input--active',

// The root picker and states *
            picker: 'picker',
            opened: 'picker--opened',
            focused: 'picker--focused',

// The picker holder
            holder: 'picker__holder',

// The picker frame, wrapper, and box
            frame: 'picker__frame',
            wrap: 'picker__wrap',
            box: 'picker__box',

// The picker header
            header: 'picker__header',

// Month navigation
            navPrev: 'picker__nav--prev',
            navNext: 'picker__nav--next',
            navDisabled: 'picker__nav--disabled',

// Month & year labels
            month: 'picker__month',
            year: 'picker__year',

// Month & year dropdowns
            selectMonth: 'picker__select--month',
            selectYear: 'picker__select--year',

// Table of dates
            table: 'picker__table',

// Weekday labels
            weekdays: 'picker__weekday',

// Day states
            day: 'picker__day',
            disabled: 'picker__day--disabled',
            selected: 'picker__day--selected',
            highlighted: 'picker__day--highlighted',
            now: 'picker__day--today',
            infocus: 'picker__day--infocus',
            outfocus: 'picker__day--outfocus',

// The picker footer
            footer: 'picker__footer',

// Today, clear, & close buttons
            buttonClear: 'picker__button--clear',
            buttonClose: 'picker__button--close',
            buttonToday: 'picker__button--today'
        }
    })
</script>

<script>
    $(function () {
        $('[data-toggle="popover"]').popover()
    })
    $('.popover-dismiss').popover({
        trigger: 'focus'
    })

    $(document).on('click', '#capitalize-warning', function (e) {
        //
        // If popover is visible: do nothing
        //
        if ($(this).prop('popShown') == undefined) {
            $(this).prop('popShown', true).popover('show');
        }
    });
    $(document).on('click', '#click', function (e) {
        //
        // If popover is visible: do nothing
        //
        if ($(this).prop('popShown') == undefined) {
            $(this).prop('popShown', true).popover('show');
        }
    });

    $(function () {
        $('#capitalize-warning').on('hide.bs.popover', function (e) {
            //
            // on hiding popover stop action
            //
            e.preventDefault();
        });
    });
    $(function () {
        $('#click').on('hide.bs.popover', function (e) {
            //
            // on hiding popover stop action
            //
            e.preventDefault();
        });
    });
</script>
