@extends('layouts.main')
@section('maincontent')

    @include ('layouts.sidebar')

    <div class="main-panel">

      @if(isset($paydayFiles) && $paydayFiles->isNotEmpty())


            <div class="modal fade" style="z-index:1100;" id="modal-home" tabindex="-1" role="dialog">
                <div class="modal-dialog-centered modal-notify modal-danger" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title"><b>{{ trans('flashmessage.message-payday') }}</b></h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true" class="white-text">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
@foreach($paydayFiles as $payday)

                            <p>{!! trans_choice('flashmessage.payday', $days = now()->diffInDays($payday->latest_payday), ['name' => '<a style="font-weight:bold;" href="' . route('site.mediaid', $payday)  . '">' . $payday->description . '</a></b>', 'count' => $days]) !!}</p>
@endforeach
                        </div>
                        <div class="modal-footer justify-content-center">
                            <a href="{{route('site.media')}}" type="button" class="btn btn-danger">{{ trans('flashmessage.media') }} </a>
                        </div>
                    </div>
                </div>
            </div>

     @endif

        <div class="mobile">

            <a href="{{ route('site.user', Auth::user()->id)}}"><div class="panel-heading-login"><a href="/lang/en"><span class="flag-icon flag-icon-us"></span></a>  <a href="/lang/nl"><span class="flag-icon flag-icon-nl"></span></a><a href="{{ route('site.dashboard')}}"> SAM Systems</a> | <span><a href="{{ route('site.user', Auth::user()->id)}}">{{ Auth::user()->name }}</span></a> <div class="icons-top-right"><!-- <a href="{{route('forms.feedback')}}"><i class="fa fa-comments" aria-hidden="true"></i></a>  |-->
                <a href="{{route('site.info')}}"><i class="fa fa-info" aria-hidden="true"></i></a> | <a href="{{ route('site.user', Auth::user()->id)}}"><i class="fa fa-user"></i></a></div></div>
            </div>

            <div class="menu-desktop">
            <nav class="navbar navbar-default navbar-fixed">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="{{ route('site.dashboard') }}">Dashboard</a>
                    </div>
                    <div class="collapse navbar-collapse">
                        <ul class="nav navbar-nav navbar-right">
                            <li style="margin-top:5%;">
                                <a href="{{route('auth.logout')}}">
                                    <p>{{ trans('mobilebar.log_out') }}</p>
                                </a>
                            </li>
                           <li><a href="{{ route('site.user', Auth::user()->id)}}">
                               <div class="chip chip-md waves-effect waves-effect">
                                   <i style="font-size:40px;" class="pe-7s-user"></i> <b>{{Auth::user()->name}}</b>
                               </div>
                               </a>
                          </li>

                            <li class="separator hidden-lg"></li>
                        </ul>
                    </div>
                </div>
            </nav>
        </div>
        <div class="content">
            <div class="container-fluid">
                @yield('content')
            </div>
        </div>
    </div>


    @include ('layouts.mobilebar')

@endsection




