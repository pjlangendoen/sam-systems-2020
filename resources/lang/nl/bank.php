<?php

return [


    'bank' => 'Bank accounts',
    'subtitle' => 'Bekijk hier al uw bankaccounts',
    'add_bank' => 'Voeg bankaccount toe',
    'new_bank' => 'Nieuw bankaccount',
    'bank_no' => 'Geen bankaccounts',
    'name' => 'Bank naam',
    'edit' => 'Wijzigen',
    'delete' => 'Verwijderen',
    'transactions' => 'Transacties',

    'search_name' => 'Zoek bank',
    'next' => 'Volgende',
    'previous' => 'Vorige',

    'edit_bank' => 'Wijzig bankaccount',
    'update_bank' => 'Update bankaccount',
    'save_bank' => 'Bankaccount opslaan',




];
