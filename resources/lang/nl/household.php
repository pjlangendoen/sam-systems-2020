<?php

return [


    'household_budgeting' => 'Huishoudboekje',
    'add_household' => 'Voeg huishoudboekje toe',
    'add_household_m' => 'Huishoudboekje',
    'date' => 'Datum',
    'income' => 'Ontvangen',
    'spending' => 'Uitgaven',
    'balance' => 'Balans',
    'transactions' => 'Transacties',
    'open' => 'Open huishouden',
    'export' => 'Export huishouden',
    'exportall' => 'Export',
    'delete' => 'Verwijderen',
    'households_no' => 'Geen huishoudboekje gevonden',
    'all_monthly' => 'Alle maandelijkse inkomsten en uitgaven',
    'search_date' => 'Zoek datum',
    'previous' => 'Vorige',
    'next' => 'Volgende',

    'name' => 'Naam',
    'price' => 'Prijs',
    'files' => 'Bestanden',
    'edit' => 'Wijzigen',
    'transactions_no' => 'Geen transacties gevonden',
    'fixed_outgoing' => 'Vaste uitgaven',
    'fixed_income' => 'Vaste inkomsten',
    'incoming_extra' => 'Extra inkomsten',
    'outgoing_extra' => 'Extra uitgaven',
    'household_view' => 'Huishoud boekje',
    'add_transaction' => 'Transactie toevoegen',
    'transactions_view' => 'Transacties bekijken',
    'totals' => 'Totaal',


];
