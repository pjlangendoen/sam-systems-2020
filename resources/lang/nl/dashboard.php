<?php

return [


    'household_budgeting' => 'Huishoudboekje',
    'transactions' => 'Transacties',
    'no_transactions' => 'Geen transacties gevonden',
    'add_transactions' => 'Transactie toevoegen',
    'add_first_transactions' => 'Voeg uw eerste transactie toe!',
    'add_first_bank' => 'Voeg uw eerste bankaccount toe!',
    'add_first_category' => 'Voeg uw eerste categorie toe!',
    'add_first_file' => 'Voeg uw eerste bestand toe!',
    'categories' => 'Categorieën',
    'banks' => 'Bank accounts',
    'media' => 'Bestanden',
    'add_media' => 'Upload bestand',
    'no_media' => 'Geen bestanden gevonden',
    'outgoing' => 'Uitgaand',
    'incoming' => 'Inkomend',
    'open_trans' => 'Open',
    'closed_trans' => 'Betaald',

    'name' => 'Naam',
    'date' => 'Datum',
    'type' => 'Type',
    'in' => 'In',
    'out' => 'Uit',
    'open' => 'Open bestand',
    'open_m' => 'Open',
    'price' => 'Prijs',
    'payday' => 'Uiterste betaaldag',
    'no_payday' => 'Geen uiterste betaaldag',
    'no_payday_m' => 'Geen',
    'status' => 'Status',
    'balance' => 'Balans',
    'cat_subcat' => 'Categorie + Subcategorie',
    'no_cat' => 'Geen categorieën gevonden',
    'add_cat' => 'Categorie toevoegen',
    'no_banks' => 'Geen bankaccounts gevonden',
    'add_banks' => 'Bankaccount toevoegen',
    'no_households' => 'Geen huishoudboekjes gevonden',
    'add_households' => 'Huishoudboekje',


];
