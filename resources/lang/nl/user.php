<?php

return [


    'profile' => 'Gebruikers profiel',
    'name' => 'Naam',
    'lastname' => 'Achternaam',
    'email' => 'Emailadres',
    'update' => 'Update profiel',

];
