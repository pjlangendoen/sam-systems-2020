<?php

return [


    'jan' => 'Januari',
    'feb' => 'Februari',
    'mar' => 'Maart',
    'mei' => 'Mei',
    'juni' => 'Juni',
    'juli' => 'Juli',
    'aug' => 'Agustus',
    'sep' => 'September',
    'okt' => 'Oktober',

    'sun' => 'Zon',
    'mon' => 'Maa',
    'tue' => 'Din',
    'wed' => 'Woe',
    'thu' => 'Don',
    'fri' => 'Vri',
    'sat' => 'Zat',



    'today' => 'Vandaag',
    'clear' => 'Leeg',
    'close' => 'Sluiten',




];
