<?php

return [


    'overview' => 'Categorieën',
    'subtitle' => 'Bekijk hier al uw categorieën',
    'category_sub' => 'Categorie + Subcategorie',
    'add_category' => 'Voeg categorie toe',
    'new_category' => 'Nieuwe categorie / subcategorie',
    'no_category' => 'Geen categorieën',
    'edit' => 'Wijzigen',
    'delete' => 'Verwijderen',
    'category_file' => 'Bestanden',
    'category_transaction' => 'Transacties',

    'search_name' => 'Zoek naam',
    'next' => 'Volgende',
    'previous' => 'Vorige',

    'edit_category' => 'Wijzig categorie',
    'name_req' => 'Naam *',
    'main_category' => 'Hoofd categorie',
    'no_other_category' => 'Deze hoofd categorie kan geen andere hoofdcategorie hebben!',
    'update_category' => 'Categorie updaten',
    'save_category' => 'Categorie opslaan',

    'select_option' => 'Selecteer keuze..',



];
