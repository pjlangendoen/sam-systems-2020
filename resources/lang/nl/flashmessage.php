<?php

return [

    // Home
    'welcome_message' => 'Welkom <b>:user</b> in Samsystems ',
    'payday' => '{1}U heeft nog <b style="color:red;">:count</b>  dag om <b>:name</b> te betalen. | U heeft nog <b style="color:red;">:count</b>  dagen om <b>:name</b> te betalen.',
    'message-payday' => 'Openstaande betalingen. Let op!',
    'media' => 'Ga naar alle bestanden',

    //media
    'file_uploaded_succes' => 'Bestand geupload.',
    'file_deleted_succes' => 'Bestand verwijderd.',
    'file_changed_succes' => 'Bestand gewijzigd.',

    //transaction
    'transaction_with_file' => 'Transactie aangemaakt met bestand.',
    'transaction_made_succes' => 'Transactie aangemaakt.',
    'transaction_deleted_succes' => 'Transactie verwijderd.',
    'transaction_changed_succes' => 'Transactie gewijzigd.',
    'price_numeric' => 'Prijs moet een getal zijn!',
    'price_required' => 'Prijs is verplicht!',
    'household_required' => 'Huishoudboekje is verplicht!',
    'bank_required' => 'Bank account is verplicht!',
    'name_required' => 'Naam is verplicht!',
    'datetime_required' => 'Datum is verplicht!',

    //household
    'household_deleted_succes' => 'Household verwijderd.',
    'household_in_use' => 'Er is al een huishoudboekje voor deze maand.',
    'household_new_made' => 'Er is een huishoudboekje gemaakt met ',
    'household_transactions' => ' transacties.',

    //bank
    'bank_made_succes' => 'Bankaccount aangemaakt.',
    'bank_deleted_succes' => 'Bankaccount verwijderd.',
    'bank_changed_succes' => 'Bankaccount gewijzigd.',
    'bank_name_unique' => 'Deze banknaam is al gebruikt!',


    //category
    'category_made_succes' => 'Categorie aangemaakt.',
    'category_deleted_succes' => 'Categorie verwijderd.',
    'category_changed_succes' => 'Categorie gewijzigd.',
    'category_name_unique' => 'Deze categorie is al gebruikt!',

    //home
    'user_changed_succes' => 'Gebruiker gewijzigd.',
    'first_bank' => 'Voeg uw eerste bank toe. ',
    'first_transaction' => 'Voeg uw eerste transactie toe.',
    'first_file' => 'Voeg uw eerste bestand toe.',
    'first_category' => 'Voeg uw eerste category toe.',
    'first_household' => 'Voeg uw eerste huishoudboekje toe.',
    'refresh' => 'ververs pagina',

    //Confirm
    'confirmnew' => 'Weet u zeker dat u dit wilt toevoegen?',
    'confirmdelete' => 'Weet u zeker dat u dit wilt verwijderen?',
    'confirmupdate' => 'Weet u zeker dat u dit wilt wijzigen?',


];
