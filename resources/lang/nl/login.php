<?php

return [

    'remember' => 'Onthouden',
    'forget' => 'Wachtwoord vergeten?',
    'email' => 'E-mail Adres',
    'password' => 'Wachtwoord',

];
