<?php

return [


    'transaction' => 'Transactie',
    'no_transaction' => 'Geen transactie gekoppeld',
    'no_transaction_m' => 'Geen transactie',
    'category' => 'Categorie',
    'new_cat' => 'nieuwe categorie',
    'categorys_req' => 'Categorie *',
    'no_cat' => 'Geen categorie',
    'media' => 'Media',
    'subtitle' => 'Bekijk hier al uw mediabestanden',
    'add_media' => 'Upload bestand',
    'add_media_for' => 'Upload bestand voor',
    'add_media_input' => 'Voeg bestand toe *',
    'no_media' => 'Geen bestanden',
    'name' => 'Naam',
    'name_description' => 'Naam | Beschrijving *',
    'description' => 'Beschrijving',
    'date' => 'Upload datum',
    'date_req' => 'Upload datum',
    'type' => 'Type',
    'open' => 'Open bestand',
    'payday' => 'Uiterste betaaldatum',
    'no_payday' => 'Geen uiterste betaaldatum',
    'no_payday_m' => 'Geen',
    'status' => 'Status',
    'search_open' => 'Zoek open of betaald',
    'search_name' => 'Zoek naam',
    'search_date' => 'Zoek uploaddatum',
    'search_payday' => 'Zoek uiterste betaaldag',
    'clear_filter' => 'Wis filter',
    'open_pay' => 'Open',
    'payed' => 'Betaald',
    'search' => 'Zoek',
    'select_option' => 'Selecteer keuze..',
    'select_date' => 'Selecteer datum..',
    'make' => 'Maak',
    'refresh' => 'refresh de pagina',
    'en' => 'en',
    'save' => 'Opslaan',
    'file_edit' => 'Bestandsgegevens aanpassen',
    'choose_file' => 'Kies bestand..',
    'skip' => 'Annuleer',
    'browse' => 'bladeren',
    'sort' => 'Sorteer',
    'letop'  => 'Let op!',
    'popover' => 'Betaling verloopt bijna!',
    'openstaand' => 'Er staan nog betalingen open!',

    'previous' => 'Vorige',
    'next' => 'Volgende',

    'open_trans' => 'Open',
    'closed_trans' => 'Betaald',

    'file_required' => 'Upload een bestand met het volgende formaat: jpeg,png,jpg,gif,svg,xlsx,docx,pdf',
    'category_required' => 'Categorie is verplicht!',
    'description_required' => 'Beschrijving is verplicht!',



    'download' => 'Download',
    'edit' => 'Wijzigen',
    'delete' => 'Verwijderen',




];
