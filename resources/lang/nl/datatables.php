<?php

return [


     'previous' => 'Vorige',
    'next' => 'Volgende',
    'search' => 'Zoek',
    'show_results' => 'Resultaten zien',
    'laat' => 'Laat',
    'results' => 'Resultaten',
    'last' => 'Laatste pagina',
    'first' => 'Eerste pagina',
    'to' => 'tot',
    'of' => 'van',

    'empty' => 'Deze tabel is leeg',
    'no_date' => 'Geen data om te weergeven',
    'busy' => 'Bezig..',


];
