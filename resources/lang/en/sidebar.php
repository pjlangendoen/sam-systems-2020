<?php

return [


    'household_budgeting' => 'Household budgeting',
    'transactions' => 'Transactions',
    'taal' => 'Choose language',
    'categories' => 'Categories',
    'banks' => 'Banks',
    'user_profile' => 'User Profile',

];
