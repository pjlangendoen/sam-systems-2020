<?php

return [

    'household_budgeting' => 'household budgeting',
    'add_household' => 'Add household',
    'add_household_m' => 'Add household',
    'date' => 'Date',
    'income' => 'Incoming',
    'spending' => 'Outgoing',
    'balance' => 'Balance',
    'transactions' => 'Transactions',
    'open' => 'Open Household',
    'export' => 'Export household',
    'exportall' => 'Export',
    'delete' => 'Delete',
    'households_no' => 'No households',
    'all_monthly' => 'All monthly incoming and outgoing',
    'search_date' => 'Search date',
    'previous' => 'Previous',
    'next' => 'Next',

    'name' => 'Name',
    'price' => 'Price',
    'files' => 'Files',
    'edit' => 'Edit',
    'transactions_no' => 'No transactions',
    'fixed_outgoing' => 'Fixed outgoing',
    'fixed_income' => 'Fixed incoming',
    'incoming_extra' => 'Extra incoming',
    'outgoing_extra' => 'Extra outgoing',
    'household_view' => 'Household view',
    'add_transaction' => 'Add transaction',
    'transactions_view' => 'View transactions',
    'totals' => 'Totals',



];
