<?php

return [


    'household_budgeting' => 'Household budgeting',
    'transactions' => 'Transactions',
    'no_transactions' => 'No transactions found',
    'add_transactions' => 'Add Transactions',
    'add_first_transactions' => 'Add your first transaction!',
    'add_first_bank' => 'Add your first bankaccount!',
    'add_first_category' => 'Add your first category!',
    'add_first_file' => 'Add your first file!',
    'categories' => 'Categorys',
    'banks' => 'Bank accounts',
    'media' => 'Invoices',
    'add_media' => 'Upload file',
    'no_media' => 'No files found',
    'outgoing' => 'Outgoing',
    'incoming' => 'Incoming',
    'open_trans' => 'Open',
    'closed_trans' => 'Payed',


    'name' => 'Name',
    'date' => 'Date',
    'type' => 'Type',
    'in' => 'In',
    'out' => 'Out',
    'open' => 'Open file',
    'open_m' => 'Open',
    'price' => 'Price',
    'payday' => 'Latest payday',
    'no_payday' => 'No latest payday',
    'no_payday_m' => 'None',
    'status' => 'Status',
    'balance' => 'Balance',
    'cat_subcat' => 'Category + Subcategory',
    'no_cat' => 'No categorys found',
    'add_cat' => 'Add category',
    'no_banks' => 'No bank accounts found',
    'add_banks' => 'Add bankaccount',
    'no_households' => 'No households found',
    'add_households' => 'Add household',


];
