<?php

return [


    'jan' => 'January',
    'feb' => 'February',
    'mar' => 'March',
    'mei' => 'May',
    'juni' => 'June',
    'juli' => 'July',
    'aug' => 'August',
    'sep' => 'September',
    'okt' => 'October',

    'sun' => 'Sun',
    'mon' => 'Mon',
    'tue' => 'Tue',
    'wed' => 'Wed',
    'thu' => 'Thu',
    'fri' => 'Fri',
    'sat' => 'Sat',



    'today' => 'Today',
    'clear' => 'Clear',
    'close' => 'Close',




];
