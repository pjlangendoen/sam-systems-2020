<?php

return [

    // Home
    'welcome_message' => 'Welkom <b>:user</b> in Samsystems ',
    'payday' => '{1} U have <b style="color:red;">:count</b>  day left to pay <b>:name</b>. | U have <b style="color:red;">:count</b>  days left to pay <b>:name</b>.',
    'message-payday' => 'Outstanding payments. Note!',
    'media' => 'Go to all media',

    //media
    'file_uploaded_succes' => 'File Uploaded.',
    'file_deleted_succes' => 'File Deleted.',
    'file_changed_succes' => 'File Changed.',

    //transaction
    'transaction_with_file' => 'Transaction made with file.',
    'transaction_made_succes' => 'Transaction made.',
    'transaction_deleted_succes' => 'Transaction deleted.',
    'transaction_changed_succes' => 'Transaction changed.',
    'price_numeric' => 'Price must be a number!',
    'price_required' => 'Price is required!',
    'household_required' => 'Household is required!',
    'bank_required' => 'Bank account is required',
    'name_required' => 'Name is required!',
    'datetime_required' => 'Datum is required!',

    //household
    'household_deleted_succes' => 'Household deleted.',
    'household_in_use' => 'There is already a household for this month.',
    'household_new_made' => 'There is a household made with ',
    'household_transactions' => ' transactions.',

    //bank
    'bank_made_succes' => 'Bankaccount made.',
    'bank_deleted_succes' => 'Bankaccount deleted.',
    'bank_changed_succes' => 'Bankaccount changed.',
    'bank_name_unique' => 'This bankname is already in use!',

    //category
    'category_made_succes' => 'Category made.',
    'category_deleted_succes' => 'Category deleted.',
    'category_changed_succes' => 'Category changed.',
    'bank_name_unique' => 'This category is already in use!',

    //home
    'user_changed_succes' => 'User changed.',
    'first_bank' => 'Add your first bank.',
    'first_transaction' => 'Add your first transaction.',
    'first_file' => 'Add your first file.',
    'first_category' => 'Add your first category.',
    'first_household' => 'Add your first household.',
    'refresh' => 'refresh page',

    //Confirm
    'confirmnew' => 'Are you sure you want to save this?',
    'confirmdelete' => 'Are you sure you want to delete this?',
    'confirmupdate' => 'Are you sure you want to change this?',



];
