<?php

return [


    'bank' => 'Bank accounts',
    'subtitle' => 'View all your bank accounts',
    'add_bank' => 'Add bankaccount',
    'new_bank' => 'New bankaccount',
    'bank_no' => 'No bankaccounts',
    'name' => 'Bank name',
    'edit' => 'Edit',
    'delete' => 'Delete',
    'transactions' => 'Transactions',

    'search_name' => 'Search bank',
    'next' => 'Next',
    'previous' => 'Previous',

    'edit_bank' => 'Edit bankaccount',
    'update_bank' => 'Update bankaccount',
    'save_bank' => 'Save bankaccount',



];
