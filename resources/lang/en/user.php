<?php

return [

    'profile' => 'User profile',
    'name' => 'Name',
    'lastname' => 'Lastname',
    'email' => 'Email Address',
    'update' => 'Update profile',

];
