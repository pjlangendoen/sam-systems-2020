<?php

return [


    'transaction' => 'Transaction',
    'no_transaction' => 'No transaction',
    'no_transaction_m' => 'No transaction',
    'category' => 'Category',
    'new_cat' => 'new category',
    'categorys_req' => 'Category *',
    'no_cat' => 'No category',
    'media' => 'Media',
    'subtitle' => 'View all your media files',
    'add_media_input' => 'File input *',
    'add_media_for' => 'Add file for',
    'add_media' => 'Upload file',
    'no_media' => 'No files',
    'name' => 'Name',
    'name_description' => 'Name | Description *',
    'description' => 'Description',
    'date_req' => 'Upload date',
    'date' => 'Upload date',
    'type' => 'Type',
    'open' => 'Open file',
    'payday' => 'Latest payday',
    'no_payday' => 'No latest payday',
    'no_payday_m' => 'None',
    'status' => 'Status',
    'search_open' => 'Search open or payed',
    'search_name' => 'Search name',
    'search_date' => 'Search date',
    'search_payday' => 'Search latest payday',
    'clear_filter' => 'Clear filter',
    'open_pay' => 'Open',
    'payed' => 'Payed',
    'search' => 'Search',
    'select_option' => 'Select option..',
    'select_date' => 'Select date..',
    'make' => 'Make',
    'refresh' => 'refresh the page',
    'en' => 'en',
    'save' => 'Save',
    'file_edit' => 'File data edit',
    'choose_file' => 'Choose file..',
    'skip' => 'Skip',
    'browse' => 'browse',
    'sort' => 'Sort',
    'letop'  => 'Pay attention!',
    'popover' => 'Payment has to be done.',
    'openstaand' => 'There are open payments!',

    'open_trans' => 'Open',
    'closed_trans' => 'Payed',

    'previous' => 'Previous',
    'next' => 'Next',

    'file_required' => 'Upload a file with the following format: jpeg,png,jpg,gif,svg,xlsx,docx,pdf',
    'category_required' => 'Category is required!',
    'description_required' => 'Description is required!',

    'download' => 'Download',
    'edit' => 'Edit',
    'delete' => 'Delete',




];
