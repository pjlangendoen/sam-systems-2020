<?php

return [


    'overview' => 'Categorys',
    'subtitle' => 'View all your categorys',
    'category_sub' => 'Category + Subcategory',
    'add_category' => 'Add category',
    'new_category' => 'New category / subcategory',
    'no_category' => 'No categorys',
    'edit' => 'Edit',
    'delete' => 'Delete',
    'category_file' => 'Files',
    'category_transaction' => 'Transactions',

    'search_name' => 'Search name',
    'next' => 'Next',
    'previous' => 'Previous',

    'edit_category' => 'Edit category',
    'name_req' => 'Name *',
    'main_category' => 'Main category',
    'no_other_category' => 'This maincategory can\'t have another maincategory!',
    'update_category' => 'Update category',
    'save_category' => 'Save category',

    'select_option' => 'Select option..',



];
