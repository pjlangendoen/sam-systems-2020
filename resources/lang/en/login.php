<?php

return [

    'remember' => 'Remember me',
    'forget' => 'Forgot your password?',
    'email' => 'E-mail Address',
    'password' => 'Password',

];
