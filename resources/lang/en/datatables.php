<?php

return [


    'previous' => 'Previous',
    'next' => 'Next',
    'search' => 'Search',
    'show_results' => 'Results',
    'laat' => 'Show',
    'results' => 'Result',
    'last' => 'Last page',
    'first' => 'First page',
    'to' => 'To',
    'of' => 'of',

    'empty' => 'This table is empty',
    'no_date' => 'No data to show',
    'busy' => 'Busy..',


];
