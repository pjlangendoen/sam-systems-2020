$(document).ready(function() {
    // Setup - add a text input to each footer cell
    $('#table thead th').each( function () {
        var title = $(this).text();
        var table = $(this).parent().parent().parent();
        var exlude = (table.is("[exclude]") ? table.attr('exclude').split("|") : []);
        // var exlude = $(this).parent().parent().parent().html('Test');
        if(!exlude.includes(title.toLowerCase())) {
        	if($(this).is("[select]")) {
        		$(this).html( '<select class="form-control"><option value="">' + title + '</option></select><div class="title-table">' +title +'</div>' );
        	} else {
        		$(this).html( '<input type="text" placeholder="'+window.translations.search+' '+title+'" /><div class="title-table">' +title +'</div>' );
        	}
        }else{
        	$(this).html( '<div class="title-table1">' +title +'</div>' );
        }
    });


 
    // DataTable
    var table = $('#table').DataTable();

    // var table = $('#household').html('Test');
 
    // Apply the search
    table.columns().every( function (index) {
        var that = this;
 
        $( 'input', this.header() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        });

        $( 'input', this.header() ).on('click', function(e) {
        	e.stopPropagation();
        });

        if($(this.header()).find("select").length) {
        	var select = $(this.header()).find("select:first");
        	select.on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
 
                        that
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    });

        	that.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                });

        	select.on('click', function(e) {
        		e.stopPropagation();
        	});
        }
        
 	/*$('<select><option value=""></option></select>')
                    .appendTo( $(column.footer()).empty() )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
 
                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    });
                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                });*/
    });

});

/* Accordion homepage */


// var acc = document.getElementsByClassName("accordion");
// var i;
//
// for (i = 0; i < acc.length; i++) {
//     acc[i].addEventListener("click", function() {
//         this.classList.toggle("active");
//         var panel = this.nextElementSibling;
//         if (panel.style.maxHeight){
//             panel.style.maxHeight = null;
//         } else {
//             panel.style.maxHeight = panel.scrollHeight + "px";
//         }
//     });


$(document).on('click', '.accordion', function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight){
        panel.style.maxHeight = null;
    } else {
        panel.style.maxHeight = panel.scrollHeight + "px";
        $('html, body').animate({ scrollTop: $(this).offset().top-50 }, 500);
    }
});


//
// $.fn.dataTable.ext.search.push(
//     function( settings, data, dataIndex ) {
//         var min = parseInt( $('#min').val(), 10 );
//         var max = parseInt( $('#max').val(), 10 );
//         var age = parseFloat( data[3] ) || 0; // use data for the age column
//
//         if ( ( isNaN( min ) && isNaN( max ) ) ||
//             ( isNaN( min ) && age <= max ) ||
//             ( min <= age   && isNaN( max ) ) ||
//             ( min <= age   && age <= max ) )
//         {
//             return true;
//         }
//         return false;
//     }
// );
