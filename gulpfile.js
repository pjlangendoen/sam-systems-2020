var gulp = require('gulp');
var runSequence = require('run-sequence');
var plugins = require('gulp-load-plugins')();

var paths = {
    input: {
        site: {
            sass: {
                dir: 'resources/assets/scss/site/',
                filename: 'site.scss'
            },
            script: [
                'resources/assets/js/site/*.js',
                'resources/assets/js/site/**/*.js'
            ]
        },
        admin: {
            sass: {
                dir: 'resources/assets/scss/admin/',
                filename: 'admin.scss'
            },
            script: [
                'resources/assets/js/admin/*.js',
                'resources/assets/js/admin/**/*.js'
            ]
        },
        vendor: {
            style: [
                'resources/assets/vendor/*.css',
                'resources/assets/vendor/**/*.css'
            ],
            script: [
                'resources/assets/vendor/*.js',
                'resources/assets/vendor/**/*.js'
            ]
        }
    },
    output: {
        site: {
            sass: {
                dir: 'public/build/assets/css/',
                filename: 'site.css'
            },
            script: {
                dir: 'public/build/assets/js/',
                filename: 'site.js'
            }
        },
        admin: {
            sass: {
                dir: 'public/build/assets/css/',
                filename: 'admin.css'
            },
            script: {
                dir: 'public/build/assets/js/',
                filename: 'admin.js'
            }
        },
        manifest: {
            output_dir: 'public/build/',
            cache_dir: 'manifest/'
        }
    }
};

function getTask(task) {
    return require('./gulp-tasks/' + task)(gulp, plugins, paths);
}

/***
 * Tasks
 */
gulp.task('site-sass', getTask('site-sass'));
gulp.task('site-script', getTask('site-script'));
gulp.task('admin-sass', getTask('admin-sass'));
gulp.task('admin-script', getTask('admin-script'));
gulp.task('build-clean', getTask('build-clean'));
gulp.task('build', ['build-clean'], getTask('build'));
gulp.task('cleanup', getTask('cleanup'));

gulp.task('default', function () {
    return runSequence(
        ['site-sass', 'admin-sass'],
        ['site-script', 'admin-script'],
        'build'
    );
});

gulp.task('watch', function() {
    runSequence(
        ['site-sass', 'admin-sass'],
        ['site-script', 'admin-script'],
        'build'
    );

    gulp.watch(paths.input.site.sass.dir + '**', function(){
        runSequence('site-sass', 'build');
    });

    gulp.watch(paths.input.site.style, function(){
        runSequence('site-sass', 'build');
    });

    gulp.watch(paths.input.site.script, function() {
        runSequence('site-script', 'build');
    });

    gulp.watch(paths.input.admin.style, function() {
        runSequence('admin-sass', 'build');
    });

    gulp.watch(paths.input.admin.script, function() {
        runSequence('admin-script', 'build');
    });
});